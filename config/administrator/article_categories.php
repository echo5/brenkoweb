<?php

/**
 * Article Categories model config
 */

return array(

    'title' => 'Article Categories',

    'single' => 'Category',

    'model' => 'App\PostCategory',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * Limit to type
     */
    'query_filter'=> function($query)
    {
        $query->where('post_type', 'article');
    },

    /**
     * The display columns
     */
    'columns' => array(
        'name' => array(
            'title' => 'Name',
        ),
        'slug' => array(
            'title' => 'Slug',
        ),
        'parent_name' => array(
            'title' => 'Parent Category',
            'relationship' => 'parent',
            'select' => '(:table).name',
        ),
        'created_at' => array(
            'title' => 'Date',
        ),
        'image' => array(
            'title' => 'Image',
            'output' => function($value)
            {
                if ($value)
                    return '<img src="' . config('assets.postCategoryImageThumbsDirectory') . 'small/'.$value.'" height="100" />';
                return;
            },
            'sortable' => false,
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name'
        ),
        'parent' => array(
            'title' => 'Parent',
            'type' => 'relationship'
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'post_type' => array(
            'title' => 'Type',
            'type' => 'text',
            'value' => 'article',
            'visible' => function($model)
            {
                return false; 
            },
        ),
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'meta_description' => array(
            'title' => 'Meta Description',
            'type' => 'textarea',
        ),
        'parent' => array(
            'title' => 'Parent Category',
            'type' => 'relationship',
            'name_field' => 'name',
            'options_filter' => function($query)
            {
                $query->where('post_type', 'article');
            },
        ),
        'image' => array(
            'title' => 'Image',
            'type' => 'image',
            'location' => public_path() . config('assets.postCategoryImagesDirectory'),
            'naming' => 'random',
            'length' => 20,
            'size_limit' => 2,
            'sizes' => array(
                array(420, 210, 'crop', public_path() . config('assets.postCategoryImageThumbsDirectory') . 'small/', 100),
                array(420, 380, 'crop', public_path() . config('assets.postCategoryImageThumbsDirectory') . 'medium/', 100),
                array(880, 380, 'crop', public_path() . config('assets.postCategoryImageThumbsDirectory') . 'large/', 100),
            )
        ),
        'created_at' => array(
            'type' => 'datetime',
            'title' => 'Date Created',
            'date_format' => 'yy-mm-dd',
            'time_format' => 'HH:mm',
        )

    ),

);