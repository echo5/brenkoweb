<?php

/**
 * Articles model config
 */

use App\Events\ArticleApproved;
use App\Events\ArticleRejected;
use App\Events\ArticleSuspended;

$posts = include('posts.php');

$articles = array(

    'title' => 'Articles',

    'single' => 'Article',

    'model' => 'App\Post',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * Limit to type
     */
    'query_filter'=> function($query)
    {
        $query->where('type', 'article');
    },

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'type' => array(
            'title' => 'Type',
            'type' => 'text',
            'value' => 'article',
            'visible' => function($model)
            {
                return false; 
            },
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
            'options_filter' => function($query)
            {
                $query->where('post_type', 'article');
                $query->whereNotNull('parent_id');
            },
        ),
        'template' => array(
            'title' => 'Template',
            'type' => 'relationship',
            'name_field' => 'name',
            'options_filter' => function($query)
            {
                $query->where('view', 'LIKE', 'posts.%');
                $query->where('view', '!=', 'posts.index');
            },
        ),
        'status_notes' => array(
            'type' => 'textarea',
            'title' => 'Status Notes / Reason',
            'limit' => 255, //optional, defaults to no limit
            'height' => 130, //optional, defaults to 100
        )
    ),

    /**
     * This is where you can define the model's custom actions
     */
    'actions' => array(
        'approve' => array(
            'title' => 'Approve',
            'messages' => array(
                'active' => 'Approving and sending approval email...',
                'success' => 'Success',
                'error' => 'There was an error while approving',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new ArticleApproved($model->user, $model));
                return $response;
            }
        ),
        'reject' => array(
            'title' => 'Reject',
            'messages' => array(
                'active' => 'Rejecting and sending rejection email...',
                'success' => 'Success',
                'error' => 'There was an error while rejecting',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new ArticleRejected($model->user, $model));
                return $response;
            }
        ),
        'suspend' => array(
            'title' => 'Suspend',
            'messages' => array(
                'active' => 'Suspending and sending suspension email...',
                'success' => 'Success',
                'error' => 'There was an error while rejecting',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new ArticleSuspended($model->user, $model));
                return $response;
            }
        ),
    ),

);

$merged = array_replace_recursive($posts, $articles);

return $merged;