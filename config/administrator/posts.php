<?php

/**
 * Posts model common items
 */
use App\Post;
$statuses = Post::getStatuses();

return array(

    /**
     * The display columns
     */
    'columns' => array(
        'title' => array(
            'title' => 'Title',
        ),
        'slug' => array(
            'title' => 'Slug',
        ),
        'category_name' => array(
            'title' => 'Category',
            'relationship' => 'category',
            'select' => '(:table).name',
        ),
        'user_name' => array(
            'title' => 'User',
            'relationship' => 'user',
            'select' => '(:table).name',
        ),
        'image' => array(
            'title' => 'Image',
            'output' => function($value)
            {
                if ($value)
                    return '<img src="' . config('assets.postImageThumbsDirectory') . 'small/'.$value.'" height="100" />';
                return;
            },
            'sortable' => false,
        ),
        'status' => array(
            'title' => 'Status',
            'output' => function($value)
            {
                if ($value)
                    return '<span class="model-status"><i class="fa status-'.$value.'"></i><span>';
                return;
            },
        )
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'title' => array(
            'title' => 'Title',
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship'
        ),
        'status' => array(
            'title' => 'Status',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'type' => array(
            'title' => 'Type',
            'type' => 'text',
            'value' => 'article',
            'visible' => function($model)
            {
                return false; 
            },
        ),
        'status' => array(
            'title' => 'Status',
            'type' => 'enum',
            'options' => $statuses,
            'value' => 'approved'
        ),
        'title' => array(
            'title' => 'Title',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'meta_description' => array(
            'title' => 'Meta Description',
            'type' => 'text',
        ),
        'template' => array(
            'title' => 'Template',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'image' => array(
            'title' => 'Image',
            'type' => 'image',
            'location' => public_path() . config('assets.postImagesDirectory'),
            'naming' => 'random',
            'length' => 20,
            'size_limit' => 2,
            'sizes' => array(
                array(420, 210, 'crop', public_path() . config('assets.postImageThumbsDirectory') . 'small/', 100),
                array(420, 380, 'crop', public_path() . config('assets.postImageThumbsDirectory') . 'medium/', 100),
                array(880, 380, 'crop', public_path() . config('assets.postImageThumbsDirectory') . 'large/', 100),
            )
        ),
        'content' => array(
            'title' => 'Content',
            'type' => 'wysiwyg',
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'created_at' => array(
            'type' => 'datetime',
            'title' => 'Date Created',
            'date_format' => 'yy-mm-dd',
            'time_format' => 'HH:mm',
        )

    ),

);