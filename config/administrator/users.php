<?php

/**
 * Users model config
 */

use App\Events\UserSuspended;
use App\User;
$statuses = User::getStatuses();

return array(

    'title' => 'Users',

    'single' => 'User',

    'model' => 'App\User',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,


    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'username' => array(
            'title' => 'Username',
            'select' => "username",
        ),
        'email' => array(
            'title' => 'Email',
            'select' => "email",
        ),
        'status' => array(
            'title' => 'Status',
            'output' => function($value)
            {
                if ($value)
                    return '<span class="model-status"><i class="fa status-'.$value.'"></i><span>';
                return;
            },
        )
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name',
        ),
        'email' => array(
            'title' => 'Email',
        ),
        'roles' => array(
            'title' => 'User Role',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'username' => array(
            'title' => 'Username',
            'type' => 'text',
        ),
        'email' => array(
            'title' => 'Email',
            'type' => 'text',
        ),
        'password' => array(
            'title' => 'Password',
            'type' => 'password',
        ),
        'roles' => array(
            'title' => 'User Role',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'status' => array(
            'title' => 'Status',
            'type' => 'enum',
            'options' => $statuses,
        ),
    ),

    /**
     * This is where you can define the model's custom actions
     */
    'actions' => array(
        'suspend' => array(
            'title' => 'Suspend',
            'messages' => array(
                'active' => 'Suspending and sending suspension email...',
                'success' => 'Success',
                'error' => 'There was an error while rejecting',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new UserSuspended($model));
                return $response;
            }
        ),
    ),

);