<?php

/**
 * The main site settings page
 */

use App\Activity;

return array(

	/**
	 * Settings page title
	 *
	 * @type string
	 */
	'title' => 'Settings',

	/**
	 * The edit fields array
	 *
	 * @type array
	 */
	'edit_fields' => array(
		'site_name' => array(
			'title' => 'Site Name',
			'type' => 'text',
			'limit' => 50,
		),
        'admin_email' => array(
            'title' => 'Admin Email',
            'type' => 'text',
        ),
        // 'paypal_email' => array(
        //     'title' => 'PayPal Email (account used for receiving payment)',
        //     'type' => 'text',
        // ),
        'price_split' => array(
            'title' => 'PayPal price split 0-100 (e.g., 10 would be 10% of the total for the site owner and 90% for the script author)',
            'type' => 'number',
        ),
        'home_articles' => array(
            'title' => 'Number of articles on homepage',
            'type' => 'number',
        ),
        'home_tutorials' => array(
            'title' => 'Number of tutorials on homepage',
            'type' => 'number',
        ),
        'home_scripts' => array(
            'title' => 'Number of scripts on homepage',
            'type' => 'number',
        ),
        // 'home_sponsored_script' => array(
        //     'title' => 'Show a sponsored script on homepage',
        //     'type' => 'bool',
        // ),
        'home_sponsored_listing' => array(
            'title' => 'Show a sponsored listing on homepage',
            'type' => 'bool',
        ),
        'banner_low_impressions' => array(
            'title' => 'Low Banner Impressions',
            'type' => 'number',
            'description' => 'Number of impressions remaining before sending reminder email.'
        ),
        'banner_low_days' => array(
            'title' => 'Low Banner Days',
            'type' => 'number',
            'description' => 'Number of days remaining before sending reminder email.'
        ),
        'upload_image_max_width' => array(
            'title' => 'Maximum Upload Width in Pixels (px) Allowed for Images in CKeditor',
            'type' => 'number',
        ),
        'upload_image_max_size' => array(
            'title' => 'Maximum Upload Size in Killobytes (KB) Allowed for Images in CKeditor',
            'type' => 'number',
        ),
        // 'activity_days_old' => array(
        //     'title' => 'Old Activity Days',
        //     'type' => 'number',
        //     'description' => 'Number of days after which activity is qualified as old.'
        // ),

		// 'logo' => array(
		// 	'title' => 'Image (200 x 150)',
		// 	'type' => 'image',
		// 	'naming' => 'random',
		// 	'location' => public_path(),
		// 	'size_limit' => 2,
		// 	'sizes' => array(
		//  		array(200, 150, 'crop', public_path() . '/resize/', 100),
		//  	)
		// ),
	),

	/**
	 * The validation rules for the form, based on the Laravel validation class
	 *
	 * @type array
	 */
	'rules' => array(
		'site_name' => 'required|max:50',
        'admin_email' => 'required|email',
        'price_split' => 'required|integer|min:0|max:100',
        'home_articles' => 'required|integer',
        'home_tutorials' => 'required|integer',
        'home_scripts' => 'required|integer',
        'banner_low_impressions' => 'required|integer',
        'banner_low_days' => 'required|integer',
		// 'logo' => 'required',
	),

	/**
	 * This is run prior to saving the JSON form data
	 *
	 * @type function
	 * @param array		$data
	 *
	 * @return string (on error) / void (otherwise)
	 */
	'before_save' => function(&$data)
	{
		// $data['site_name'] = $data['site_name'] . ' - Brenko Web';  
	},

	/**
	 * The permission option is an authentication check that lets you define a closure that should return true if the current user
	 * is allowed to view this settings page. Any "falsey" response will result in a 404.
	 *
	 * @type closure
	 */
	'permission'=> function()
	{
		return true;
		//return Auth::user()->hasRole('developer');
	},

	/**
	 * This is where you can define the settings page's custom actions
	 */
	'actions' => array(
		'clean_old_activity' => array(
			'title' => 'Delete Old Activity',
			'messages' => array(
				'active' => 'Cleaning old activity...',
				'success' => 'Old logs deleted',
				'error' => 'There was an error while cleaning the activity logs',
			),
			//the settings data is passed to the closure and saved if a truthy response is returned
			'action' => function(&$data)
			{    
                $activity = new Activity();
				$activity->cleanLog();
				return true;
			}
		),
	),
);