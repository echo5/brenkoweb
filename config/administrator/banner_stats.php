<?php

/**
 * Banner Stats model config
 */

return array(

    'title' => 'Banner Stats',

    'single' => 'Banner Stat',

    'model' => 'App\BannerStat',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The sort options for a model
     *
     * @type array
     */
    'sort' => array(
        'field' => 'date',
        'direction' => 'desc',
    ),

    /**
     * The display columns
     */
    'columns' => array(
        'banner_caption' => array(
            'title' => 'Banner',
            'relationship' => 'banner',
            'select' => '(:table).id',
            'output' => '<a href="/admin/banners/(:value)">Banner #(:value)</a>',
        ),
        'clicks' => array(
            'title' => 'Clicks',
        ),
        'impressions' => array(
            'title' => 'Impressions',
        ),
        'month_and_year' => array(
            'title' => 'Month & Year',
            'select' => "DATE_FORMAT((:table).date, '%M %Y')",
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'date' => array(
            'type' => 'date',
            'title' => 'Date',
            'date_format' => 'yy-mm-00',
        )
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'impressions' => array(
            'title' => 'Impressions',
            'type' => 'text',
        ),
        'clicks' => array(
            'title' => 'Clicks',
            'type' => 'text',
        ),
        'date' => array(
            'type' => 'date',
            'title' => 'Date',
            'date_format' => 'yy-mm',
        )

    ),

    /**
     * Action permissions
     *
     * @type array
     */
    'action_permissions'=> array(
        'create' => false,
        'delete' => false,
        'update' => false,
        'view' => false,
    ),

);