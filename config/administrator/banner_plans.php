<?php

/**
 * Banner Plans model config
 */

return array(

    'title' => 'Banner Plans',

    'single' => 'Plan',

    'model' => 'App\BannerPlan',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * The display columns
     */
    'columns' => array(
        'name' => array(
            'title' => 'Name',
        ),
        'cost' => array(
            'title' => 'Cost',
            'output' => '$(:value)',
        ),
        'impressions' => array(
            'title' => 'Impressions',
        ),
        'days' => array(
            'title' => 'Days',
        ),
        'public' => array(
            'title' => 'Public',
            'select' => "IF((:table).public, 'Yes', 'No')",
        )
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'public' => array(
            'title' => 'Public',
            'type' => 'bool',
        ),
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'cost' => array(
            'title' => 'Cost',
            'type' => 'text',
        ),
        'impressions' => array(
            'title' => 'Impressions',
            'type' => 'text',
        ),
        'days' => array(
            'title' => 'Days',
            'type' => 'text',
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'wysiwyg',
        ),
        'created_at' => array(
            'type' => 'datetime',
            'title' => 'Date Created',
            'date_format' => 'yy-mm-dd',
            'time_format' => 'HH:mm',
        )
    ),

);