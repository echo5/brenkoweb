<?php

/**
 * Orders model config
 */

return array(

    'title' => 'Orders',

    'single' => 'Order',

    'model' => 'App\Order',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The display columns
     */
    'columns' => array(
        'id' => array(
            'title' => 'Order ID',
            'output' => '<a href="/admin/page/admin.order?id=(:value)">(:value)</a>',
        ),
        'payment_id' => array(
            'title' => 'Payment ID',
        ),
        'payer_id' => array(
            'title' => 'Payer ID',
        ),
        'payer_email' => array(
            'title' => 'PayPal Email',
        ),
        'user_name' => array(
            'title' => 'User',
            'relationship' => 'user',
            'select' => '(:table).name',
        ),
        'created_at' => array(
            'title' => 'Date',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'id' => array(
            'title' => 'Order ID',
        ),
        'payment_id' => array(
            'title' => 'Payment ID',
        ),
        'payer_id' => array(
            'title' => 'Payer ID',
        ),
        'payer_email' => array(
            'title' => 'PayPal Email',
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship'
        ),
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(

        'user' => array(
            'title' => 'User',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'created_at' => array(
            'type' => 'date',
            'title' => 'Date Created',
            'date_format' => 'yy-mm-dd', //optional, will default to this value
        )
    ),

    /**
     * Action permissions
     */
    'action_permissions'=> array(
        'create' => false,
        'update' => false,
        // 'view' => false
    ),

);