<?php

/**
 * Page Categories model config
 */

return array(

    'title' => 'Page Categories',

    'single' => 'Category',

    'model' => 'App\PostCategory',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * Limit to type
     */
    'query_filter'=> function($query)
    {
        $query->where('post_type', 'page');
    },

    /**
     * The display columns
     */
    'columns' => array(
        'name' => array(
            'title' => 'Name',
        ),
        'slug' => array(
            'title' => 'Slug',
        ),
        'parent_name' => array(
            'title' => 'Parent Category',
            'relationship' => 'parent',
            'select' => '(:table).name',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'parent' => array(
            'title' => 'Parent Category',
            'type' => 'relationship',
            'name_field' => 'name',
            'options_filter' => function($query)
            {
                $query->where('post_type', 'page');
            },
        ),

    ),

);