<?php

/**
 * Messages model config
 */


return array(

    'title' => 'Messages',

    'single' => 'Message',

    'model' => 'App\Message',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 600,


    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name' => array(
            'title' => 'Name',
            'select' => "name",
        ),
        'email' => array(
            'title' => 'Email',
            'select' => "email",
        ),
        'message' => array(
            'title' => 'Message',
            'select' => "message",
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name',
        ),
        'email' => array(
            'title' => 'Email',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'email' => array(
            'title' => 'Email',
            'type' => 'text',
        ),
        'message' => array(
            'title' => 'Message',
            'type' => 'wysiwyg',
        ),
    ),

);