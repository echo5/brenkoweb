<?php

/**
 * Directory Listings model config
 */

use App\Events\ListingApproved;
use App\Events\ListingRejected;
use App\Events\ListingSuspended;
use App\Listing;

$statuses = Listing::getStatuses();

return array(

    'title' => 'Directory Listings',

    'single' => 'Listing',

    'model' => 'App\Listing',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * The display columns
     */
    'columns' => array(
        'title' => array(
            'title' => 'Title',
        ),
        'image' => array(
            'title' => 'Image',
            'output' => function($value)
            {
                if ($value)
                    return '<img src="' . config('assets.listingImageThumbsDirectory') .$value.'" height="100" />';
                return;
            },
            'sortable' => false,
        ),
        'user_name' => array(
            'title' => 'User',
            'relationship' => 'user',
            'select' => '(:table).name',
        ),
        'category_name' => array(
            'title' => 'Category',
            'relationship' => 'category',
            'select' => '(:table).name',
        ),
        'status' => array(
            'title' => 'Status',
            'output' => function($value)
            {
                if ($value)
                    return '<span class="model-status"><i class="fa status-'.$value.'"></i><span>';
                return;
            },
        )
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'title' => array(
            'title' => 'Title',
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship'
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship'
        ),
        'plan' => array(
            'title' => 'Plan',
            'type' => 'relationship'
        ),
        'status' => array(
            'title' => 'Status',
        ),
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'status' => array(
            'title' => 'Status',
            'type' => 'enum',
            'options' => $statuses,
        ),
        'title' => array(
            'title' => 'Title',
            'type' => 'text',
        ),
        'link' => array(
            'title' => 'Link',
            'type' => 'text',
        ),
        'image' => array(
            'title' => 'Image',
            'type' => 'image',
            'location' => public_path() . config('assets.listingImagesDirectory'),
            'naming' => 'random',
            'length' => 20,
            'size_limit' => 2,
            'sizes' => array(
                array(340, 240, 'crop', public_path() . config('assets.listingImageThumbsDirectory') , 100),
            )
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'wysiwyg',
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'plan' => array(
            'title' => 'Plan',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'sponsored' => array(
            'type' => 'bool',
            'title' => 'Sponsored',
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),

    ),

    /**
     * This is where you can define the model's custom actions
     */
    'actions' => array(
        'approve' => array(
            'title' => 'Approve',
            'messages' => array(
                'active' => 'Approving and sending approval email...',
                'success' => 'Success',
                'error' => 'There was an error while approving',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new ListingApproved($model->user, $model));
                return $response;
            }
        ),
        'reject' => array(
            'title' => 'Reject',
            'messages' => array(
                'active' => 'Rejecting and sending rejection email...',
                'success' => 'Success',
                'error' => 'There was an error while rejecting',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new ListingRejected($model->user, $model));
                return $response;
            }
        ),
        'suspend' => array(
            'title' => 'Suspend',
            'messages' => array(
                'active' => 'Suspending and sending suspension email...',
                'success' => 'Success',
                'error' => 'There was an error while rejecting',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new ListingSuspended($model->user, $model));
                return $response;
            }
        ),
    ),

);