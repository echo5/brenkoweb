<?php

/**
 * Email Templates model config
 */

return array(

    'title' => 'Email Templates',

    'single' => 'Template',

    'model' => 'App\EmailTemplate',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * The display columns
     */
    'columns' => array(
        'subject' => array(
            'title' => 'Subject',
        ),
        'view' => array(
            'title' => 'View',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'subject' => array(
            'title' => 'Subject',
            'type' => 'text',
        ),
        'view' => array(
            'title' => 'View',
            'type' => 'text',
        ),
        'content' => array(
            'title' => 'Content',
            'type' => 'wysiwyg',
        ),

    ),

);