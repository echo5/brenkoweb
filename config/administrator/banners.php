<?php

/**
 * Banners model config
 */

use App\Banner;
use App\Events\BannerApproved;
use App\Events\BannerRejected;
use App\Events\BannerSuspended;

$banner = new Banner;
$locations = Banner::getLocations();
$statuses = Banner::getStatuses();

return array(

    'title' => 'Banners',

    'single' => 'Banner',

    'model' => 'App\Banner',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The display columns
     */
    'columns' => array(
        'caption' => array(
            'title' => 'Caption',
        ),
        'location' => array(
            'title' => 'Location',
        ),
        'plan_name' => array(
            'title' => 'Plan',
            'relationship' => 'plan',
            'select' => '(:table).name',
        ),
        'user_name' => array(
            'title' => 'User',
            'relationship' => 'user',
            'select' => '(:table).name',
        ),
        'image' => array(
            'title' => 'Image',
            'output' => function($value)
            {
                if ($value)
                    return '<div class="has-image-field"><img src="' . config('assets.bannerImagesDirectory') .$value.'" height="100" /></div>';
                return;
            },
            'sortable' => false,
        ),
        'status' => array(
            'title' => 'Status',
            'output' => function($value)
            {
                if ($value)
                    return '<span class="model-status"><i class="fa status-'.$value.'"></i><span>';
                return;
            },
        ),
        'created_at' => array(
            'title' => 'Date',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'caption' => array(
            'title' => 'Caption',
        ),
        'location' => array(
            'title' => 'Location',
            'type' => 'enum',
            'options' => $locations,
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship'
        ),
        'plan' => array(
            'title' => 'Plan',
            'type' => 'relationship'
        ),
        'status' => array(
            'title' => 'Status',
        ),
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'status' => array(
            'title' => 'Status',
            'type' => 'enum',
            'options' => $statuses,
        ),
        'caption' => array(
            'title' => 'Caption',
            'type' => 'text',
        ),
        'link' => array(
            'title' => 'Link',
            'type' => 'text',
        ),
        'location' => array(
            'title' => 'Location',
            'type' => 'enum',
            'options' => $locations,
        ),
        'plan' => array(
            'title' => 'Plan',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'max_impressions' => array(
            'title' => 'Maximum Impressions',
            'type' => 'number'
        ),
        'end_date' => array(
            'title' => 'End Date',
            'type' => 'date',
        ),
        'image' => array(
            'title' => 'Image',
            'type' => 'image',
            'location' => public_path() . config('assets.bannerImagesDirectory'),
            'naming' => 'random',
            'length' => 20,
            'size_limit' => 2,
        ),
        'script' => array(
            'title' => 'Script',
            'type' => 'wysiwyg',
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'created_at' => array(
            'type' => 'date',
            'title' => 'Date Created',
            'date_format' => 'yy-mm-dd', //optional, will default to this value
        )

    ),

    /**
     * This is where you can define the model's custom actions
     */
    'actions' => array(
        'approve' => array(
            'title' => 'Approve',
            'messages' => array(
                'active' => 'Approving and sending approval email...',
                'success' => 'Success',
                'error' => 'There was an error while approving',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new BannerApproved($model->user, $model));
                return $response;
            }
        ),
        'reject' => array(
            'title' => 'Reject',
            'messages' => array(
                'active' => 'Rejecting and sending rejection email...',
                'success' => 'Success',
                'error' => 'There was an error while rejecting',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new BannerRejected($model->user, $model));
                return $response;
            }
        ),
        'suspend' => array(
            'title' => 'Suspend',
            'messages' => array(
                'active' => 'Suspending and sending suspension email...',
                'success' => 'Success',
                'error' => 'There was an error while rejecting',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new BannerSuspended($model->user, $model));
                return $response;
            }
        ),
    ),

);