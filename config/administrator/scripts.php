<?php

/**
 * Scripts model config
 */
use App\Events\ScriptApproved;
use App\Events\ScriptRejected;
use App\Events\ScriptSuspended;
use App\Script;
$statuses = Script::getStatuses();

return array(

    'title' => 'Scripts',

    'single' => 'Script',

    'model' => 'App\Script',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The display columns
     */
    'columns' => array(
        'name' => array(
            'title' => 'Name',
        ),
        'version' => array(
            'title' => 'Version',
        ),
        'user_name' => array(
            'title' => 'User',
            'relationship' => 'user',
            'select' => '(:table).name',
        ),
        'category_name' => array(
            'title' => 'Category',
            'relationship' => 'category',
            'select' => '(:table).name',
        ),
        'image' => array(
            'title' => 'Image',
            'output' => '<img src="' . config('assets.scriptImageThumbsDirectory') . '/(:value)" height="100" />',
            'sortable' => false,
        ),
        'status' => array(
            'title' => 'Status',
            'output' => function($value)
            {
                if ($value)
                    return '<span class="model-status"><i class="fa status-'.$value.'"></i><span>';
                return;
            },
        )
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name',
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship'
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship'
        ),
        'status' => array(
            'title' => 'Status',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'status' => array(
            'title' => 'Status',
            'type' => 'enum',
            'options' => $statuses,
        ),
        'waive_commission' => array(
            'title' => 'Waive Commission',
            'type' => 'bool',
        ),
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'version' => array(
            'title' => 'Version',
            'type' => 'text',
        ),
        'price' => array(
            'type' => 'number',
            'title' => 'Price',
            'symbol' => '$',
            'decimals' => 2,
            'thousands_separator' => ',',
            'decimal_separator' => '.',
        ),
        'preview_link' => array(
            'title' => 'Preview Link',
            'type' => 'text',
        ),
        'image' => array(
            'title' => 'Image',
            'type' => 'image',
            'location' => public_path() . config('assets.scriptImagesDirectory'),
            'naming' => 'random',
            'length' => 20,
            'size_limit' => 2,
            'sizes' => array(
                array(340, 240, 'crop', public_path() . config('assets.scriptImageThumbsDirectory'), 100),
            )
        ),
        'file' => array(
            'title' => 'File Link',
            'type' => 'file',
            'location' => public_path() . config('assets.scriptFilesDirectory'),
            'naming' => 'random',
            'length' => 20,
            'size_limit' => 200,
            'mimes' => 'zip',
        ),
        'description' => array(
            'title' => 'Description',
            'type' => 'wysiwyg',
        ),
        'features' => array(
            'title' => 'Features',
            'type' => 'wysiwyg',
        ),
        'requirements' => array(
            'title' => 'Requirements',
            'type' => 'wysiwyg',
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'created_at' => array(
            'type' => 'date',
            'title' => 'Date Created',
            'date_format' => 'yy-mm-dd',
        )

    ),

    /**
     * This is where you can define the model's custom actions
     */
    'actions' => array(
        'approve' => array(
            'title' => 'Approve',
            'messages' => array(
                'active' => 'Approving and sending approval email...',
                'success' => 'Success',
                'error' => 'There was an error while approving',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new ScriptApproved($model->user, $model));
                return $response;
            }
        ),
        'reject' => array(
            'title' => 'Reject',
            'messages' => array(
                'active' => 'Rejecting and sending rejection email...',
                'success' => 'Success',
                'error' => 'There was an error while rejecting',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new ScriptRejected($model->user, $model));
                return $response;
            }
        ),
        'suspend' => array(
            'title' => 'Suspend',
            'messages' => array(
                'active' => 'Suspending and sending suspension email...',
                'success' => 'Success',
                'error' => 'There was an error while rejecting',
            ),
            'action' => function($model)
            {
                $response = Event::fire(new ScriptSuspended($model->user, $model));
                return $response;
            }
        ),
    ),

);