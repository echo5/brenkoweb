<?php

/**
 * Scripts model config
 */

return array(

    'title' => 'Script Reviews',

    'single' => 'Script Review',

    'model' => 'App\ScriptReview',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,

    /**
     * The display columns
     */
    'columns' => array(
        'script_name' => array(
            'title' => 'Script',
            'relationship' => 'script',
            'select' => '(:table).name',
        ),
        'rating' => array(
            'title' => 'Rating',
        ),
        'review' => array(
            'title' => 'Review',
        ),
        'user_name' => array(
            'title' => 'User',
            'relationship' => 'user',
            'select' => '(:table).name',
        ),
        'approved' => array(
            'title' => 'Approved',
            'select' => "IF((:table).approved, '<span class=\"approved\"><i class=\"fa fa-check\"></i></div>', '<span class=\"approved\"><i class=\"fa fa-times\"></i></div>')",
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'rating' => array(
            'title' => 'Rating',
        ),
        'script' => array(
            'title' => 'Script',
            'type' => 'relationship'
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship'
        ),
        'approved' => array(
            'title' => 'Approved',
            'type' => 'bool',
        ),
    ),

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'approved' => array(
            'title' => 'Approved',
            'type' => 'bool',
        ),
        'rating' => array(
            'title' => 'Rating',
            'type' => 'text',
        ),
        'review' => array(
            'title' => 'Review',
            'type' => 'wysiwyg',
        ),
        'script' => array(
            'title' => 'Script',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'user' => array(
            'title' => 'User',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'created_at' => array(
            'type' => 'date',
            'title' => 'Date Created',
            'date_format' => 'yy-mm-dd',
        )

    ),

);