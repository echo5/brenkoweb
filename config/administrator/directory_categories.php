<?php

/**
 * Directory Categories model config
 */

return array(

    'title' => 'Directory Categories',

    'single' => 'Category',

    'model' => 'App\ListingCategory',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * The display columns
     */
    'columns' => array(
        'name' => array(
            'title' => 'Name',
        ),
        'slug' => array(
            'title' => 'Slug',
        ),
        'parent_name' => array(
            'title' => 'Parent Category',
            'relationship' => 'parent',
            'select' => '(:table).name',
        ),
    ),

    /**
     * The filter fields
     *
     * @type array
     */
    'filters' => array(
        'name' => array(
            'title' => 'Name'
        ),
        'parent' => array(
            'title' => 'Parent',
            'type' => 'relationship'
        ),
    ),
    
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name' => array(
            'title' => 'Name',
            'type' => 'text',
        ),
        'slug' => array(
            'title' => 'Slug',
            'type' => 'text',
        ),
        'meta_description' => array(
            'title' => 'Meta Description',
            'type' => 'textarea',
        ),
        'parent' => array(
            'title' => 'Parent Category',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'created_at' => array(
            'type' => 'datetime',
            'title' => 'Date Created',
            'date_format' => 'yy-mm-dd',
            'time_format' => 'HH:mm',
        )

    ),

);