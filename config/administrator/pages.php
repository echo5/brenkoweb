<?php

/**
 * Pages model config
 */

$posts = include('posts.php');

$pages = array(

    'title' => 'Pages',

    'single' => 'Page',

    'model' => 'App\Post',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * Limit to type
     */
    'query_filter'=> function($query)
    {
        $query->where('type', 'page');
    },

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'type' => array(
            'title' => 'Type',
            'type' => 'text',
            'value' => 'page',
            'visible' => function($model)
            {
                return false; 
            },
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
            'visible' => false,
            'options_filter' => function($query)
            {
                $query->where('post_type', 'page');
            },
        ),
        'template' => array(
            'title' => 'Template',
            'type' => 'relationship',
            'name_field' => 'name',
            'options_filter' => function($query)
            {
                $query->where('view', 'LIKE', 'pages.%');
            },
        ),
    ),
);

$merged = array_replace_recursive($posts, $pages);

return $merged;