<?php

/**
 * Tutorials model config
 */

$posts = include('posts.php');

$tutorials = array(

    'title' => 'Tutorials',

    'single' => 'Tutorial',

    'model' => 'App\Post',

    /**
     * The width of the model's edit form
     *
     * @type int
     */
    'form_width' => 800,
    
    /**
     * Limit to type
     */
    'query_filter'=> function($query)
    {
        $query->where('type', 'tutorial');
    },

    /**
     * The editable fields
     */
    'edit_fields' => array(
        'type' => array(
            'title' => 'Type',
            'type' => 'text',
            'value' => 'tutorial',
            'visible' => function($model)
            {
                return false; 
            },
        ),
        'category' => array(
            'title' => 'Category',
            'type' => 'relationship',
            'name_field' => 'name',
            'options_filter' => function($query)
            {
                $query->where('post_type', 'tutorial');
                $query->whereNotNull('parent_id');
            },
        ),
        'template' => array(
            'title' => 'Template',
            'type' => 'relationship',
            'name_field' => 'name',
            'options_filter' => function($query)
            {
                $query->where('view', 'LIKE', 'posts.%');
                $query->where('view', '!=', 'posts.index');
            },
        ),
    ),
);

$merged = array_replace_recursive($posts, $tutorials);

return $merged;