<?php

return [
    // @TODO move to .env file
    'Sandbox' => true,

    'DeveloperAccountEmail' => '',

    'ApplicationID' => env('PAYPAL_ADAPTIVE_APP_ID'),

    'DeviceID' => '',

    'APIUsername' => env('PAYPAL_ADAPTIVE_API_USERNAME'),

    'APIPassword' => env('PAYPAL_ADAPTIVE_API_PASSWORD'),

    'APISignature' => env('PAYPAL_ADAPTIVE_API_SIGNATURE'),

    'LogResults' => env('APP_DEBUG', false),

    'LogPath' => storage_path() . '/logs/paypal.log',

];