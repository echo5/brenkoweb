<?php

return [

    /*
     * Main admin email for notifications
     */
    'adminEmail' => 'brenko.web@gmail.com',

    /*
     * Site name
     */
    'siteName' => 'BrenkoWeb',


];
