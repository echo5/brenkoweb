<?php

return [

    /*
     * Path to Post upload directory
     */
    'postImagesDirectory' => '/uploads/posts/originals/',

    /*
     * Path to Post thumbs upload directory
     */
    'postImageThumbsDirectory' => '/uploads/posts/thumbs/',

    /*
     * Path to PostCategory upload directory
     */
    'postCategoryImagesDirectory' => '/uploads/postcategories/originals/',

    /*
     * Path to PostCategory thumbs upload directory
     */
    'postCategoryImageThumbsDirectory' => '/uploads/postcategories/thumbs/',

    /*
     * Path to user avatar uploads
     */
    'userAvatarsDirectory' => '/uploads/users/avatars/',

    /*
     * Path to Listing upload directory
     */
    'listingImagesDirectory' => '/uploads/listings/originals/',
 
    /*
     * Path to Listing upload directory
     */
    'listingImageThumbsDirectory' => '/uploads/listings/thumbs/',
 
    /*
     * Path to Script upload directory
     */
    'scriptFilesDirectory' => '/uploads/scripts/files/',

    /*
     * Path to Script upload directory
     */
    'scriptImagesDirectory' => '/uploads/scripts/images/originals/',

    /*
     * Path to Script upload directory
     */
    'scriptImageThumbsDirectory' => '/uploads/scripts/images/thumbs/',

    /*
     * Path to Banner upload directory
     */
    'bannerImagesDirectory' => '/uploads/banners/',

    /*
     * Path to Tutorial suggestion zips directory
     */
    'tutorialZipsDirectory' => '/uploads/tutorials/',


];
