<!DOCTYPE html>
<html lang="en-us" >
<head>
<title>Maintenance page, &copy;Brenkoweb</title>
<style type="text/css">
body {text-align:center;font-size:2.4em;font-family:Verdana, Geneva, sans-serif;color:#111;background-color:#b2b2b2;text-shadow:0px 1px 1px #ccc, -1px 0px 1px #777;}
</style>
</head>
<body>
<p>BrenkoWeb.com is under re-construction.</p>
<p>Re-opening is scheduled for <date>March 05th, 2016</date>.</p>
<p>&nbsp;</p>
<p style="font-size:0.75em; color:#999">We apologize for inconvenience. Come back and check out our new and improved look!<br /><em>brenkoweb.com team</em></p>
</body>
</html>