<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<?php
$name = "";
$colors = Array();
if(isset($_GET['form_submit'])) {
	$name = $_GET['name'];
	if (isset($_GET['red'])) array_push($colors, $_GET['red']); 
	if (isset($_GET['green'])) array_push($colors, $_GET['green']);  
	if (isset($_GET['blue'])) array_push($colors, $_GET['blue']);  
	if (isset($_GET['lime'])) array_push($colors, $_GET['lime']);  
	if (isset($_GET['fuchsia'])) array_push($colors, $_GET['fuchsia']);  
	asort($colors);
}
else {
	echo "Problems with the script, sorry. Refresh the page.";
	die();
}
?>


</head>

<body>

<p>Welcome <?php echo $name; ?>!</p>
<p>You like these colors:<br />
<?php 
for ($i = 0; $i <= count($colors)-1; $i++) {
	if (count($colors) == 1) {
		echo $colors[$i];
		break;
	}
	if ($i == 0) {
		echo $colors[$i];
	}
	else {
		echo ", ".$colors[$i]; 
	}
}
?>

</body>
</html>