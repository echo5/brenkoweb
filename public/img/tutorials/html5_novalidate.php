<!DOCTYPE>
<html>
<head>
<style type="text/css">
  #answer {
      background-color:#444eee;
      margin:6% 3%; padding:3%; width:80%;
	  font-family:Verdana; color:#eee445;
  }
</style>
<?php
$answer = "";
if(isset($_GET['save'])) {
	if (isset($_GET['description']) & $_GET['description'] != "") $answer = "Your description: '<strong>" . $_GET['description'] . "'</strong> is temporary saved.";
	else $answer = "Your description is saved empty.";
}
else if(isset($_GET['publish'])) {
	if(isset($_GET['description'])) $answer = "Your description: '<strong>" . $_GET['description'] . "'</strong> HAS NOT been verified but IS published.";
}
else {
	echo "Problems with the script, sorry. Refresh the page."; die();
}
?>
</head>

<body>

<h3>Save or publish?</h3>

<form action="../../../img/tutorials/html5_formnovalidate.php" method="get">
  <p>
    <label for="description">Description:</label>
    <input type="text" id="description" name="description" required />
  </p>
  <p>
    <input type="submit" value="Save Draft" formnovalidate="true" name="save" />
    or
    <input type="submit" value="Publish" name="publish" />
  </p>
</form>

<p id="answer"><?php echo $answer; ?></p>

</body>

</body>
</html>