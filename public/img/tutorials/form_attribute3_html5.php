<!DOCTYPE>
<html>
<head>
<style type="text/css">
  #answer {
      background-color:#eee445;
      margin:6% 3%; padding:3%; width:80%;
	  font-family:Verdana;
  }
</style>
<?php
$answer = "";
if(isset($_GET['update'])) {
	if (isset($_GET['name'])) $answer = $_GET['name'];
	}
else {
	echo "Problems with the script, sorry. Refresh the page."; die();
}
?>
</head>

<body>

<form action="../../../img/tutorials/form_attribute3_html5.php" method="get">
	<p>
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" value="<?php echo $answer; ?>" />
    </p>
  	<p>
        <input type="submit" value="Update" name="update" />
    or
        <input type="submit" value="Delete" formaction="../../../img/tutorials/html5_formaction.php" name="delete" />
  	</p>
</form>

<p id="answer">Your name is:<br /><?php echo $answer; ?></p>

</body>

</body>
</html>