/** Activity Read All */
$("document").ready(function(){

    $(".activity.unread").click(function(e){
        e.preventDefault();
        var link = $(this).find('a').attr('href');
        var activityId = $(this).data('activity-id');
		$.post( "/admin/readactivities", { 'activities[]': [ activityId ] } )
			.done(function() {
				location.href = link;
			});
	});

    $(".mark-all-as-read").click(function(e){
        e.preventDefault();
        var activityIds = [];
        var listItems = $(this).siblings('.activity-list').eq(0).find('li.activity.unread');
        var flag = $(this).parent().siblings('.dropdown-toggle').eq(0).find('span.label');
        listItems.each(function() {
	        activityIds.push($(this).data('activity-id'));
        });
        $.post( "/admin/readactivities", { 'activities[]':  activityIds } )
        	.done(function() {
        		listItems.removeClass('unread');
        		listItems.addClass('read');
        		flag.remove();
        	});
	});

});

/** Filter by URL param */
$("document").ready(function(){
	var status = getUrlParameter('status');
	var input = $('#filter_field_status');
	setTimeout(function() {
		input.val(status);
		input.keydown();
	}, 1000);
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};