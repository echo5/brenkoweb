/**
 * Scroll arrows
 */
$('#scroll-down').click(function(e) {
	e.preventDefault();
	$('html, body').animate({
	    scrollTop: $("#main").offset().top - 80
	}, 1000);
});

$('#scroll-to-top').click(function(e) {
	e.preventDefault();
	$('html, body').animate({
	    scrollTop: 0
	}, 1000);
});

/**
 * Sidebar tutorials
 */
$('#sidebar-tutorials').find('.has-children').click(function(e) {
	e.preventDefault();
	$(this).next('.children').toggleClass('nav-open');
});

/**
 * Sidebar directory
 */
$('#sidebar-directory').find('.has-children').click(function(e) {
	e.preventDefault();
	$(this).next('.children').toggleClass('nav-open');
});

/**
 * Comments
 */
var commentForm = $('#comment-form');
$('.comment-reply').click(function(e) {
	e.preventDefault();
	var commentId;
	commentId = $(this).data('comment-id');
	commentForm.find('input[name="parent_id"]').val(commentId);
	commentForm.appendTo('#comment-' + commentId);
	commentForm.find('.comment-cancel').show();
});

$('.comment-cancel').click(function(e) {
	e.preventDefault();
	commentForm.find('input[name="parent_id"]').val('');
	$(this).hide();
	commentForm.appendTo('#comment-form-container');
});

/**
 * Load more
 */
var loadMoreBtn = $('#btn-load-more');
loadMoreBtn.click(function(e) {
	e.preventDefault();
	loadFeed($(this).data('page'));
});

/**
 * Load more posts into container
 */
function loadFeed(pageNumber) {

	$('#loading-icon-container').slideDown('fast');
	var curURL = window.location.href;

	$.ajax({
		url: curURL + "loadmore/" + pageNumber,
		type: 'GET',
		success: function (html) {
			$('#loading-icon-container').slideUp('1000');
			var col1 = $(html).find(".posts-grid-col-1").html();
			var col2 = $(html).find(".posts-grid-col-2").html();
			if (!col1) {
				$('#loading-message').html('There are no more posts.');
				$('#loading-message').slideDown('fast');
				setTimeout(hideLoadMore, 4000);
			}
			else {
				$('<div class="ajax-preload">' + col1 + '</div>').appendTo('#posts-grid-ajax-1');
				$('<div class="ajax-preload">' + col2 + '</div>').appendTo('#posts-grid-ajax-2');
				setTimeout(showLoaded, 500);
				loadMoreBtn.data('page', pageNumber + 1);
			}
		},
		fail: function () {
		    alert('Posts could not be loaded.');
		}
	});
}

/**
 * Class for hiding loading animation
 */
function showLoaded() {
	$('#home-grid').find('.ajax-preload').addClass('ajax-loaded');
}

/**
 * Hide loader button if no more posts
 */
function hideLoadMore() {
	$('#load-more').slideUp('1000');
}

/**
 * Track ad views after page load if not a bot
 */
 $( document ).ready(function() {
 	var userAgent = navigator.userAgent;
	var botPattern = "(googlebot\/|Googlebot-Mobile|Googlebot-Image|Google favicon|Mediapartners-Google|bingbot|slurp|java|wget|curl|Commons-HttpClient|Python-urllib|libwww|httpunit|nutch|phpcrawl|msnbot|jyxobot|FAST-WebCrawler|FAST Enterprise Crawler|biglotron|teoma|convera|seekbot|gigablast|exabot|ngbot|ia_archiver|GingerCrawler|webmon |httrack|webcrawler|grub.org|UsineNouvelleCrawler|antibot|netresearchserver|speedy|fluffy|bibnum.bnf|findlink|msrbot|panscient|yacybot|AISearchBot|IOI|ips-agent|tagoobot|MJ12bot|dotbot|woriobot|yanga|buzzbot|mlbot|yandexbot|purebot|Linguee Bot|Voyager|CyberPatrol|voilabot|baiduspider|citeseerxbot|spbot|twengabot|postrank|turnitinbot|scribdbot|page2rss|sitebot|linkdex|Adidxbot|blekkobot|ezooms|dotbot|Mail.RU_Bot|discobot|heritrix|findthatfile|europarchive.org|NerdByNature.Bot|sistrix crawler|ahrefsbot|Aboundex|domaincrawler|wbsearchbot|summify|ccbot|edisterbot|seznambot|ec2linkfinder|gslfbot|aihitbot|intelium_bot|facebookexternalhit|yeti|RetrevoPageAnalyzer|lb-spider|sogou|lssbot|careerbot|wotbox|wocbot|ichiro|DuckDuckBot|lssrocketcrawler|drupact|webcompanycrawler|acoonbot|openindexspider|gnam gnam spider|web-archive-net.com.bot|backlinkcrawler|coccoc|integromedb|content crawler spider|toplistbot|seokicks-robot|it2media-domain-crawler|ip-web-crawler.com|siteexplorer.info|elisabot|proximic|changedetection|blexbot|arabot|WeSEE:Search|niki-bot|CrystalSemanticsBot|rogerbot|360Spider|psbot|InterfaxScanBot|Lipperhey SEO Service|CC Metadata Scaper|g00g1e.net|GrapeshotCrawler|urlappendbot|brainobot|fr-crawler|binlar|SimpleCrawler|Livelapbot|Twitterbot|cXensebot|smtbot|bnf.fr_bot|A6-Indexer|ADmantX|Facebot|Twitterbot|OrangeBot|memorybot|AdvBot|MegaIndex|SemanticScholarBot|ltx71|nerdybot|xovibot|BUbiNG|Qwantify|archive.org_bot|Applebot|TweetmemeBot|crawler4j|findxbot|SemrushBot|yoozBot|lipperhey|y!j-asr|Domain Re-Animator Bot|AddThis)";
	var botRegex = new RegExp(botPattern, 'i');
    if (!botRegex.test(userAgent)) {
    	var bannerIds = [];
    	$('.banner-block').each(function() {
    		bannerIds.push($(this).data('banner-id'));
    	});
    	$.ajaxSetup({
    	    headers: {
    	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	    }
    	});
    	$.ajax({
    		data: {
    			'banner_ids' : bannerIds
    		},
    		url: "/ad/track",
    		type: 'POST',
    		success: function (html) {
    			//console.log(html);
    		},
    		fail: function () {
    		    console.log(html);
    		}
    	});
    }
 });