<?php

use App\ListingCategory;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ListingCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

    	DB::table('listing_categories')->delete();
    	$faker = Faker::create();

        $icons = [
            'icon_pencil',
            'icon_ul',
            'icon_desktop',
            'icon_cursor_alt',
            'icon_printer',
            'icon_film',
            'icon_calulator',
            'icon_pens',
            'icon_globe',
            'icon_currency',
        ];

        foreach(range(1, 10) as $index) {
            $parent_id = ($index > 5 ? rand(1, $index) : NULL);
            ListingCategory::create([
                'id' => $index,
                'name' => $faker->text(30),
        		'slug' => $faker->slug,
                'icon' => $icons[$index],
                'parent_id' => $parent_id,
            ]);
        }

    }

}