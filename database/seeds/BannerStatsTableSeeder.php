<?php

use App\BannerStat;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class BannerStatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

    	DB::table('banner_stats')->delete();
    	$faker = Faker::create();

        foreach(range(1, 20) as $index) {
            foreach(range(1, 9) as $month) {
                BannerStat::create([
                    'date' => '2015-0'.$month.'-'.rand(0,28),
                    'banner_id' => $index,
                    'clicks' => rand(0,20),
                    'impressions' => rand(0,150),
                ]);
            }
        }

    }

}