<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PostCategoriesTableSeeder::class);
        $this->call(PostTemplatesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(ListingCategoriesTableSeeder::class);
        $this->call(ListingPlansTableSeeder::class);
        $this->call(ListingsTableSeeder::class);
        $this->call(BannerPlansTableSeeder::class);
        $this->call(BannersTableSeeder::class);
        $this->call(BannerStatsTableSeeder::class);
        $this->call(ScriptCategoriesTableSeeder::class);
        $this->call(ScriptsTableSeeder::class);
        $this->call(ScriptReviewsTableSeeder::class);
        $this->call(EmailTemplatesTableSeeder::class);

        Model::reguard();
    }

    /**
     * Loads data from $source
     *
     * @param $source   string  Source data file
     * @param $closure  Closure Closure to execute on each line
     */
    public function loadCsvData($source, Closure $closure)
    {
        $handle = fopen(__DIR__ . '/source/' . $source, 'r');
        $headers = fgetcsv($handle);

        while (($row = fgetcsv($handle)) !== false) {
            call_user_func($closure, array_combine($headers, $row));
        }

        fclose($handle);

    }
}
