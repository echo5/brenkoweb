<?php

use App\ScriptCategory;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ScriptCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

    	DB::table('script_categories')->delete();
    	$faker = Faker::create();

        foreach(range(1, 10) as $index) {
            $parent_id = ($index > 5 ? rand(1, $index) : 'NULL');
            ScriptCategory::create([
                'id' => $index,
                'name' => $faker->text(30),
        		'slug' => $faker->slug,
                'parent_id' => $parent_id,
            ]);
        }

    }

}