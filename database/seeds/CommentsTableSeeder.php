<?php

use App\Comment;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

    	DB::table('comments')->delete();
    	$faker = Faker::create();

        foreach(range(1, 10) as $index) {
            $parent_id = ($index > 3 ? rand(1, $index) : 'NULL');
            Comment::create([
        		'content' => $faker->text(100),
                'parent_id' => $parent_id,
        		'post_id' => rand(1,3),
                'user_id' => 1,
            ]);
        }

    }

}