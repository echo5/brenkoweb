<?php

use App\ListingPlan;
use Illuminate\Database\Seeder;

class ListingPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        DB::table('listing_plans')->delete();

        $plans = [
            [
                'id' => 1,
                'name' => 'Free',
                'description' => '<p>Limited time only! Free listing on our web directory.</p><p>Please support our directory by placing a reciprocal link to it from your website, as well.</p>',
                'cost' => '0',
                'public' => true,
            ],
            [
                'id' => 2,
                'name' => 'Reciprocal',
                'description' => '<p>Please support our directory by placing a reciprocal link to it from your website.</p>',
                'cost' => '0',
                'public' => true,
            ],
            [
                'id' => 3,
                'name' => 'Reciprocal',
                'description' => '<p>This option will create a featured listing and will be have priority over regural listings.</p>',
                'cost' => '1.00',
                'public' => true,
            ],
        ];

        foreach ($plans as $plan)
            ListingPlan::create($plan);

    }

}