<?php

use App\ScriptReview;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ScriptReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

    	DB::table('script_reviews')->delete();
    	$faker = Faker::create();

        foreach(range(1, 20) as $index) {
            ScriptReview::create([
                'id' => $index,
                'rating' => rand(1,5),
                'title' => $faker->text(10),
                'review' => $faker->text(30),
                'user_id' => 1,
                'script_id' => rand(1,10),
                'approved' => $faker->boolean(80),
            ]);
        }

    }

}