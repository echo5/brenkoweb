<?php

use App\BannerPlan;
use Illuminate\Database\Seeder;

class BannerPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        DB::table('banner_plans')->delete();

        $plans = [
            [
                'id' => 1,
                'name' => 'Monthly',
                'description' => '<p>Limited time only! Free listing on our web directory.</p><p>Please support our directory by placing a reciprocal link to it from your website, as well.</p>',
                'cost' => '40',
                'impressions' => '0',
                'days' => '30',
                'public' => 1,
            ],
            [
                'id' => 2,
                'name' => '1000 Impressions',
                'description' => 'Display your banner 1000 times on our site for a fixed cost.',
                'cost' => '20',
                'impressions' => '1000',
                'days' => '0',
                'public' => 1,
            ],
        ];

        foreach ($plans as $plan)
            BannerPlan::create($plan);

    }

}