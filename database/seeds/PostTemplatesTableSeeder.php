<?php

use App\PostTemplate;
use Illuminate\Database\Seeder;

class PostTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_templates')->delete();
        Eloquent::unguard();

        $templates = [
            [
                'id' => 1,
                'name' => 'Default',
                'view' => 'posts.default',
            ],
            [
                'id' => 2,
                'name' => 'Full Width',
                'view' => 'pages.full_width',
            ],
            [
                'id' => 3,
                'name' => 'Category',
                'view' => 'posts.index',
            ],
            [
                'id' => 4,
                'name' => 'Contact',
                'view' => 'pages.contact',
            ],
            [
                'id' => 5,
                'name' => 'Full Width With Image Header',
                'view' => 'pages.full-width-image-header',
            ],
            [
                'id' => 6,
                'name' => 'Full Width No Title',
                'view' => 'pages.full-width-no-title',
            ],
        ];

        foreach ($templates as $template)
            PostTemplate::create($template);

    }

}