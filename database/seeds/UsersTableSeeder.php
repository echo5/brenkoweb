<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $users = [
            [
                'id' => 1,
                'username' => 'joshua',
                'name' => 'Joshua',
                'email' => 'joshua@echo5webdesign.com',
                'password' => 'echo5p455',
                'role' => '1',
                'status' => 'approved',
            ],
            [
                'id' => 2,
                'username' => 'angelo',
                'name' => 'Angelo',
                'email' => 'brenko.web@gmail.com',
                'password' => 'brenkop455',
                'role' => '1',
                'status' => 'approved',
            ],
        ];

        foreach ($users as $user) {
            $role = $user['role'];
            unset($user['role']);
            $user = User::create($user);
            $user->attachRole($role);
        }

    }

}