<?php

use Bican\Roles\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id' => 1,
                'name' => 'Admin',
                'slug' => 'admin',
                'description' => 'Access to the admin panel',
                'level' => '100',
            ],
            [
                'id' => 2,
                'name' => 'Member',
                'slug' => 'member',
                'description' => 'Access to membership area in front-end site',
                'level' => '1',
            ],
        ];

        foreach ($roles as $role)
            Role::create($role);

    }

}