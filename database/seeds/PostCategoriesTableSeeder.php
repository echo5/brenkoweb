<?php

use App\PostCategory;
use Illuminate\Database\Seeder;

class PostCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'id' => 1,
                'name' => 'World Wide Web',
                'slug' => 'world-wide-web',
                'post_type' => 'article',
            ],
            [
                'id' => 2,
                'name' => 'Introduction To Web',
                'slug' => 'introduction-to-web',
                'post_type' => 'article',
                'parent_id' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Development',
                'slug' => 'development',
                'post_type' => 'article',
            ],
            [
                'id' => 4,
                'name' => 'Html',
                'slug' => 'html',
                'post_type' => 'tutorial',
            ],
        ];

        foreach ($categories as $category)
            PostCategory::create($category);

    }

}