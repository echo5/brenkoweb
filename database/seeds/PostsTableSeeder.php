<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = [
            [
                'id' => 1,
                'title' => 'Browser Wars',
                'slug' => 'browser-wars',
                'type' => 'article',
                'meta_description' => 'Meta description here...',
                'content' => 'Content here...',
                'category_id' => 2,
                'user_id' => 1,
                'status' => 'approved',
            ],
            [
                'id' => 2,
                'title' => 'What to know when buying a domain name',
                'slug' => 'what-to-know-when-buying-a-domain-name',
                'type' => 'article',
                'meta_description' => 'Meta description here...',
                'content' => 'Content here...',
                'category_id' => 3,
                'user_id' => 1,
                'status' => 'approved',
            ],
            [
                'id' => 3,
                'title' => 'Introduction To HTML',
                'slug' => 'introduction-to-html',
                'type' => 'tutorial',
                'meta_description' => 'Meta description here...',
                'content' => 'Content here...',
                'category_id' => 4,
                'user_id' => 1,
                'status' => 'approved',
            ],

        ];

        foreach ($posts as $post)
            Post::create($post);

    }

}