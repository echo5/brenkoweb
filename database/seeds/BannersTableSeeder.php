<?php

use App\Banner;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

    	DB::table('banners')->delete();
    	$faker = Faker::create();

        $locations = [
            [
                'slug' => 'left-sidebar',
                'size' => '360x280',
            ],
            [
                'slug' => 'bottom',
                'size' => '728x90',
            ],
            [
                'slug' => 'content',
                'size' => '360x280',
            ],
        ];

        $statuses = [
            '0' => 'pending',
            '1' => 'approved'
        ];

        foreach(range(1, 20) as $index) {
            $ad = $locations[rand(0,2)];

            $image_url = 'http://fpoimg.com/'.$ad['size'].'?text=Advertisement';
            $img_name = str_slug(str_random(32)) . '.jpg';
            $img = Image::make($image_url);
            // $img->backup();
            $img->save( public_path() . config('assets.bannerImagesDirectory') . $img_name );

            Banner::create([
                'id' => $index,
                'link' => $faker->url,
                'caption' => $faker->text(20),
        		'location' => $ad['slug'],
                'image' => $img_name,
                'max_impressions' => '1000',
                'user_id' => 1,
                'plan_id' => rand(1,2),
                'status' => $statuses[$faker->boolean(70)],
            ]);
        }

    }

}