<?php

use App\Script;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Intervention\Image\ImageManagerStatic as Image;

class ScriptsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

    	DB::table('scripts')->delete();
    	$faker = Faker::create();
        $image_url = 'http://lorempixel.com/500/300/technics/';
        $statuses = [
            '0' => 'pending',
            '1' => 'approved'
        ];

        foreach(range(1, 20) as $index) {

            $img_name = str_slug(str_random(32)) . '.jpg';
            $img = Image::make($image_url);
            // $img->backup();
            $img->save( public_path() . config('assets.scriptImagesDirectory') . $img_name );
            $img->fit(340,240);
            $img->save( public_path() . config('assets.scriptImageThumbsDirectory') . $img_name );

            Script::create([
                'id' => $index,
                'name' => $faker->text(20),
                'version' => rand(1,3). '.' .rand(0,9).'.0',
                'image' => $img_name,
                'file' => $faker->url,
                'preview_link' => $faker->url,
                'description' => $faker->paragraph(1),
                'requirements' => $faker->paragraph(2),
                'features' => $faker->paragraph(4),
                'category_id' => rand(1,10),
                'user_id' => 1,
                'status' => $statuses[$faker->boolean(70)],
            ]);
        }

    }

}