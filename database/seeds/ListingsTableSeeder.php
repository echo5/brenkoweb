<?php

use App\Listing;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ListingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

    	DB::table('listings')->delete();
    	$faker = Faker::create();
        $image_url = 'http://pipsum.com/435x310.jpg';
        $statuses = [
            '0' => 'pending',
            '1' => 'approved'
        ];
        
        foreach(range(1, 20) as $index) {

            $img_name = str_slug(str_random(32)) . '.jpg';
            $img = Image::make($image_url);
            // $img->backup();
            $img->save( public_path() . config('assets.listingImagesDirectory') . $img_name );
            $img->fit(340, 240);
            $img->save( public_path() . config('assets.listingImageThumbsDirectory') . $img_name );

            Listing::create([
                'id' => $index,
                'title' => $faker->text(30),
                'link' => $faker->url,
                'image' => $img_name,
                'description' => $faker->paragraph(7),
                'user_id' => 1,
                'plan_id' => rand(1,3),
                'category_id' => rand(1,10),
                'status' => $statuses[$faker->boolean(80)],
            ]);
        }

    }

}