<?php

use App\EmailTemplate;
use Illuminate\Database\Seeder;

class EmailTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('email_templates')->delete();
        Eloquent::unguard();

        $templates = [
            [
                'id' => 1,
                'view' => 'emails.auth.welcome',
                'subject' => 'Welcome to Brenko Web!',
                'content' => 'Welcome to Brenko Web, {{ $user->getDisplayName() }}!',
            ],
            [
                'id' => 2,
                'view' => 'emails.auth.suspended',
                'subject' => 'Your Account Has Been Suspended',
                'content' => '<p>Your account has been suspended.  If you think this is an error, please contact an administrator.</p>',
            ],
            [
                'id' => 3,
                'view' => 'emails.password',
                'subject' => 'Password Reset',
                'content' => '<h2>Password Reset</h2>
                    <div>
                        To reset your password, complete this form: [password_link].<br /> This link will expire in [expiration_time] minutes.
                    </div>',
            ],
            [
                'id' => 4,
                'view' => 'emails.banner.approved',
                'subject' => 'Banner Approved',
                'content' => '<p>Your ad has been approved!  To view your banner statistics, please visit "Banners" under [profile_link]your profile[/profile_link].</p>',
            ],
            [
                'id' => 5,
                'view' => 'emails.banner.lowcredit',
                'subject' => 'Low Banner Credit',
                'content' => '<p>Your banner (#[banner param=id]) is running low on credit.  To purchase more days or impressions please visit, [profile_link page=banners]your banners[/profile_link], click the appropriate banner and click "Add Credit."</p>',
            ],
            [
                'id' => 6,
                'view' => 'emails.banner.purchase',
                'subject' => 'Banner Credit Purchased',
                'content' => '<p>Thanks for your order at Brenko Web!  Your order number is #[order param=id].  To view your banner stats please visit [profile_link page=banners]your banners[/profile_link] under your profile</a>.
    <p>To view your order history, please visit [profile_link page=orders]your orders[/profile_link].',
            ],
            [
                'id' => 7,
                'view' => 'emails.banner.rejected',
                'subject' => 'Banner Rejected',
                'content' => '<p>Your banner has been rejected.  If you think this is an error, please contact an administrator.</p>',
            ],
            [
                'id' => 8,
                'view' => 'emails.banner.suspended',
                'subject' => 'Banner Suspended',
                'content' => '<p>Your banner has been suspended.  If you think this is an error, please contact an administrator.</p>',
            ],
            [
                'id' => 9,
                'view' => 'emails.listing.approved',
                'subject' => 'Listing Approved',
                'content' => '<p>Your directory listing has been approved!  To view it or make edits, please visit [profile_link]your profile.[/profile_link]</p>',
            ],
            [
                'id' => 10,
                'view' => 'emails.listing.purchase',
                'subject' => 'Sponsored Listing Purchased',
                'content' => '<p>Thanks for your order at Brenko Web!  Your order number is #[order param=id].  To view your listing please visit [profile_link page=listings]your profile listings[/profile_link] and login.</p>
    <p>To view your order history, please visit [profile_link page=orders]your orders page[/profile_link].</p>',
            ],
            [
                'id' => 11,
                'view' => 'emails.listing.rejected',
                'subject' => 'Listing Rejected',
                'content' => '<p>Your listing has been rejected.  If you think this is an error, please contact an administrator.</p>',
            ],
            [
                'id' => 12,
                'view' => 'emails.listing.suspended',
                'subject' => 'Listing Suspended',
                'content' => '<p>Your listing has been suspended.  If you think this is an error, please contact an administrator.</p>',
            ],
            [
                'id' => 13,
                'view' => 'emails.script.approved',
                'subject' => 'Your Script Has Been Approved',
                'content' => 'Your script has been approved at Brenko Web.  You can view it [script_link]here[/script_link].',
            ],
            [
                'id' => 14,
                'view' => 'emails.script.purchase',
                'subject' => 'Script Purchased',
                'content' => '<p>Thanks for your order at Brenko Web!  Your order number is #[order param=id].  To download your script, please visit [profile_link page=downloads]your downloads[/profile_link] and login.</p>
    <p>To view your order history, please visit [profile_link page=orders]your orders[/profile_link].',
            ],
            [
                'id' => 15,
                'view' => 'emails.script.rejected',
                'subject' => 'Script Rejected',
                'content' => '<p>Your script has been rejected.  If you think this is an error, please contact an administrator.</p>',
            ],
            [
                'id' => 16,
                'view' => 'emails.script.suspended',
                'subject' => 'Script Suspended',
                'content' => '<p>Your script has been suspended.  If you think this is an error, please contact an administrator.</p>',
            ],
            [
                'id' => 17,
                'view' => 'emails.article.approved',
                'subject' => 'Article Approved',
                'content' => '<p>Your article has been approved!  To view it, please visit [post_link]</p>',
            ],
            [
                'id' => 18,
                'view' => 'emails.article.rejected',
                'subject' => 'Article Rejected',
                'content' => '<p>Your article has been rejected.  If you think this is an error, please contact an administrator.</p><p>[post param=status_notes]</p>',
            ],
            [
                'id' => 19,
                'view' => 'emails.article.suspended',
                'subject' => 'Article Suspended',
                'content' => '<p>Your article has been suspended.  If you think this is an error, please contact an administrator.</p><p>[post param=status_notes]</p>',
            ],

        ];

        foreach ($templates as $template) {
            EmailTemplate::create($template);
        }

    }

}