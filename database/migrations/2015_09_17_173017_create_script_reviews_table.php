<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScriptReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('script_reviews', function(Blueprint $table)
        {
            $table->increments('id');
            $table->tinyInteger('rating');
            $table->string('title');
            $table->text('review');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->integer('script_id')->unsigned();
            $table->foreign('script_id')->references('id')->on('scripts')->onDelete('cascade');;
            $table->boolean('approved')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('script_reviews');
    }
}
