<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('caption')->nullable();
            $table->string('location')->index();
            $table->string('link');
            $table->string('image');
            $table->text('script');
            $table->date('end_date');
            $table->integer('max_impressions');
            $table->string('alert_status');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('banner_plans');
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banners');
    }
}
