<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title')->unique();
            $table->string('link');
            $table->string('image');
            $table->text('description');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('listing_plans');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('listing_categories');
            $table->string('status')->default('pending');
            $table->boolean('sponsored')->default(0);
            $table->integer('clicks')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listings');
    }
}
