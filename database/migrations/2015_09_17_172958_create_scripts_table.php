<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScriptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scripts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('version');
            $table->string('image')->nullable();
            $table->string('file');
            $table->string('preview_link')->nullable();
            $table->text('description');
            $table->text('features');
            $table->text('requirements');
            $table->decimal('price', 5, 2);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('script_categories');
            $table->string('status')->default('pending');
            $table->integer('views')->unsigned()->default(0);
            $table->integer('downloads')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scripts');
    }
}
