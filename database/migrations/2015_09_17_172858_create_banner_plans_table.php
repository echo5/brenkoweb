<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_plans', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->decimal('cost', 5, 2);
            $table->integer('impressions');
            $table->integer('days');
            $table->text('description');
            $table->boolean('public')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banner_plans');
    }
}
