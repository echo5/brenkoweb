<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Debugbar;
use Config;

class Banner extends Model
{

    /**
     * The fields that are fillable
     *
     * @var array
     */
    protected $fillable = array(
        'caption',
        'location',
        'link',
        'image',
        'script',
        'plan_id',
        'user_id',
        'end_date',
        'max_impressions',
        'alert_status'
    );

    /**
     * Validation rules
     */
    public static $rules = array(
        'status' => 'required',
        'location' => 'required',
        'plan_id' => 'required|integer',
        'link' => 'required|url',
        'max_impressions' => 'required|integer',
    );

    /**
     * Get location options
     * 
     * @return array
     */
    public static function getLocations()
    {
        return array(
            'content',
            'bottom',
            'left-sidebar'
        );
    }

    /**
     * Get status options
     * 
     * @return array
     */
    public static function getStatuses()
    {
        return array(
            'approved',
            'suspended',
            'rejected',
            'pending'
        );
    }

    /**
    * Plan relationship
    */
    public function plan()
    {
        return $this->belongsTo('App\BannerPlan');
    }

    /**
    * User relationship
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
    * Banner relationship
    */
    public function stats()
    {
        return $this->hasMany('App\BannerStat');
    }

    /**
     * Count remaining impressions
     * 
     * @return int
     */
    public function impressionsRemaining()
    {
        return $this->max_impressions - $this->stats->sum('impressions');
    }

    /**
     * Count days remaining
     * 
     * @return int
     */
    public function daysRemaining()
    {
        $end_date = date_create($this->end_date);
        $now = date_create(date('Y-m-d'));
        $diff = date_diff($now, $end_date);
        return $diff->format('%r%a');
    }

    /**
     * Check if banner has days or impressions remaining
     * 
     * @return boolean
     */
    public function hasCredit()
    {        
        if ( ($this->daysRemaining() > 0)  || ($this->impressionsRemaining() > 0) )
            return true;

        return false;
    }

    /**
     * Check if credit is low
     * 
     * @return bool
     */
    public function lowCredit()
    {
        $low_impressions = $this->impressionsRemaining() < intval(Config::get('settings.banner_low_impressions'));
        $low_days = $this->daysRemaining() < intval(Config::get('settings.banner_low_days'));
        if ($low_impressions && $low_days) {
            return true;
        }
        return false;
    }

    /**
     * Add click to banner
     */
    public function addClick()
    {
        $this->stats()
            ->whereRaw('YEAR(date) = ' . date('Y'))
            ->whereRaw('MONTH(date) = ' . date('m'))
            ->first()
            ->update(array (
                'clicks' => DB::raw('clicks + 1')
            ));
    }

}
