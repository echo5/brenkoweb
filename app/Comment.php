<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    /**
     * The fields that are fillable
     *
     * @var array
     */
    protected $fillable = array(
        'content',
        'parent_id',
        'post_id',
        'user_id',
    );

    /**
    * Parent relationship
    */
    public function parent()
    {
        return $this->belongsTo('App\Comment', 'parent_id');
    }

    /**
    * Children relationship
    */
    public function children()
    {
        return $this->hasMany('App\Comment', 'parent_id');
    }
    
    /**
    * User relationship
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
    * User relationship
    */
    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
