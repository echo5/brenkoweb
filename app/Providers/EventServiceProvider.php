<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\UserCreated' => [
            'App\Handlers\Events\SendWelcomeEmail',
            'App\Handlers\Events\LogUserCreated',
        ],
        'App\Events\LowBannerCredit' => [
            'App\Handlers\Events\UpdateBannerAlertStatus',
            'App\Handlers\Events\SendLowBannerCreditEmail',
        ],
        // Purchases
        'App\Events\ScriptPurchased' => [
            'App\Handlers\Events\AddScriptPermission',
            'App\Handlers\Events\SendOrderConfirmationEmail',
            'App\Handlers\Events\LogScriptPurchased',
        ],
        'App\Events\SponsoredListingPurchased' => [
            'App\Handlers\Events\SponsorListing',
            'App\Handlers\Events\SendSponsoredListingPurchasedEmail',
            'App\Handlers\Events\LogSponsoredListingPurchased',
        ],
        'App\Events\BannerCreditPurchased' => [
            'App\Handlers\Events\CreditBanner',
            'App\Handlers\Events\SendBannerCreditPurchasedEmail',
            'App\Handlers\Events\LogBannerCreditPurchased',
        ],
        // Approved
        'App\Events\ArticleApproved' => [
            'App\Handlers\Events\ApproveArticle',
            'App\Handlers\Events\SendArticleApprovedEmail',
        ],
        'App\Events\ScriptApproved' => [
            'App\Handlers\Events\ApproveScript',
            'App\Handlers\Events\SendScriptApprovedEmail',
        ],
        'App\Events\ListingApproved' => [
            'App\Handlers\Events\ApproveListing',
            'App\Handlers\Events\SendListingApprovedEmail',
        ],
        'App\Events\BannerApproved' => [
            'App\Handlers\Events\ApproveBanner',
            'App\Handlers\Events\SendBannerApprovedEmail',
        ],
        // Suspended
        'App\Events\ArticleSuspended' => [
            'App\Handlers\Events\SuspendArticle',
            'App\Handlers\Events\SendArticleSuspendedEmail',
        ],
        'App\Events\ScriptSuspended' => [
            'App\Handlers\Events\SuspendScript',
            'App\Handlers\Events\SendScriptSuspendedEmail',
        ],
        'App\Events\ListingSuspended' => [
            'App\Handlers\Events\SuspendListing',
            'App\Handlers\Events\SendListingSuspendedEmail',
        ],
        'App\Events\UserSuspended' => [
            'App\Handlers\Events\SuspendUser',
            'App\Handlers\Events\SendUserSuspendedEmail',
        ],
        'App\Events\BannerSuspended' => [
            'App\Handlers\Events\SuspendBanner',
            'App\Handlers\Events\SendBannerSuspendedEmail',
        ],
        // Rejected
        'App\Events\ArticleRejected' => [
            'App\Handlers\Events\RejectArticle',
            'App\Handlers\Events\SendArticleRejectedEmail',
        ],
        'App\Events\ScriptRejected' => [
            'App\Handlers\Events\RejectScript',
            'App\Handlers\Events\SendScriptRejectedEmail',
        ],
        'App\Events\ListingRejected' => [
            'App\Handlers\Events\RejectListing',
            'App\Handlers\Events\SendListingRejectedEmail',
        ],
        'App\Events\BannerRejected' => [
            'App\Handlers\Events\RejectBanner',
            'App\Handlers\Events\SendBannerRejectedEmail',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
