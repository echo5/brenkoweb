<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\AdminConfigSetter;

class ConfigServiceProvider extends ServiceProvider
{

    protected $defer = false;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        new AdminConfigSetter();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
