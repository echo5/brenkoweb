<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerPlan extends Model
{
    /**
     * Validation rules
     */
    public static $rules = array(
        'name' => 'required|unique:listing_plans',
        'cost' => 'required|numeric|min:0',
        'impressions' => 'required|integer|min:0',
        'days' => 'required|integer|min:0',
    );
}
