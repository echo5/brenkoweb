<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{

    /**
     * Turn off timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The fields that are fillable
     *
     * @var array
     */
    protected $fillable = array(
        'order_id',
        'type',
        'name',
        'price',
        'site_revenue',
        'model_id',
        'product_id',
    );

    /**
     * Order relationship
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

}
