<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Session;

class VerifyUserStatus
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (Auth::check()) {
            $user = Auth::user();
            switch ($user->status) {
                case 'approved':
                    break;
                case 'suspended':
                    Session::flash('message', 'Your account has been suspended.  If you think this is in error, please contact an administrator.' ); 
                    Session::flash('alert-class', 'alert-danger'); 
                    Auth::logout();
                    return redirect()->route('auth.login');
                    break;
                default:
                    Session::flash('message', 'There is a problem with your account.  Please contact an administrator.' ); 
                    Session::flash('alert-class', 'alert-warning'); 
                    Auth::logout();
                    return redirect()->route('auth.login');
                    break;
            }

        }

        return $response;
    }
}
