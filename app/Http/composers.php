<?php namespace App\Http;

use View;
use App;

App::singleton('App\Http\Composers\PostsComposer');
App::singleton('App\Http\Composers\DirectoriesComposer');
App::singleton('App\Http\Composers\ListingsComposer');
App::singleton('App\Http\Composers\ScriptsComposer');
App::singleton('App\Http\Composers\BannersComposer');

// View::composer('partials.sidebar', 'App\Http\Composers\SidebarComposer');
View::composer(['modules.sidebar_categories', 'posts.*'], 'App\Http\Composers\PostsComposer');
View::composer(['modules.sidebar_directories', 'modules.sidebar_popular-listings'], 'App\Http\Composers\DirectoriesComposer');
View::composer('modules.sidebar_listings', 'App\Http\Composers\DirectoriesComposer');
View::composer(['listings.*', 'modules.sidebar_new-listings', 'modules.sidebar_random-listings'], 'App\Http\Composers\ListingsComposer');
View::composer(['scripts.*', 'modules.sidebar_script-categories'], 'App\Http\Composers\ScriptsComposer');
View::composer('banners.*', 'App\Http\Composers\BannersComposer');
View::composer('admin.order', 'App\Http\Composers\AdminOrdersComposer');
View::composer('admin.dashboard', 'App\Http\Composers\AdminDashboardComposer');
View::composer('admin.activitylog', 'App\Http\Composers\AdminActivityLogComposer');
View::composer('emails.*', 'App\Http\Composers\EmailTemplatesComposer');