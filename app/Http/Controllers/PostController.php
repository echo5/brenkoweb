<?php

namespace App\Http\Controllers;

use App;
use View;
use Route;
use App\Repositories\PostRepository;
use App\Http\Requests\StoreArticleRequest;
use App\Http\Requests\StoreTutorialRequest;
use Auth;
use Session;
use Redirect;
use Image;
use Input;
use URL;
use Config;
use Mail;
use App\Activity;

class PostController extends Controller {

    /**
     * Create a new PostRepository instance.
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

	/**
	 * Show the category or post based on slug
	 *
	 * @return Response
	 */
    public function showBySlug($hierarchy)
    {
        return view('posts.base');
    }

    /**
     * Show a page by slug
     * 
     * @param  str $slug
     * @return Response
     */
    public function showPage($slug) 
    {
        $post = $this->postRepository->findBySlug($slug, 'page');
        if ($post) {
            return View::make($post->getTemplate() , compact('post'));
        }

        App::abort('404');
    }

    /**
     * Show category page
     * 
     * @return Response
     */
    public function showCategory()
    {
        return view('posts.index');
    }

    /**
     * Show the form for creating a new post.
     *
     * @return \Illuminate\Http\Response
     */
    public function createArticle()
    {
        if (Auth::check()) {
            return view('posts.createArticle');
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Show the form for creating a new tutorial.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTutorial()
    {
        if (Auth::check()) {
            return view('posts.createTutorial');
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeArticle(StoreArticleRequest $request)
    {
        $img_name = '';
        if($request->input('image')) {
            $img = Image::make($request->input('image'));
            $img->backup();
            $img_name = str_slug(str_random(32)) . '.' . Input::file('image')->getClientOriginalExtension();
            $img->save( public_path() . config('assets.postImagesDirectory') . $img_name );
            $img->fit(420, 210);
            $img->save( public_path() . config('assets.postImageThumbsDirectory') . 'small/' . $img_name );
            $img->reset();
            $img->fit(420, 380);
            $img->save( public_path() . config('assets.postImageThumbsDirectory') . 'medium/' . $img_name );
            $img->reset();
            $img->fit(880, 380);
            $img->save( public_path() . config('assets.postImageThumbsDirectory') . 'large/' . $img_name );
        }

        $data = array(
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'meta_description' => $request->input('meta_description'),
            'category_id' => $request->input('category_id'),
            'image' => $img_name,
            'content' => $request->input('content'),
            'type' => 'article',
            'user_id' => Auth::user()->id,
            'template_id' => 1,
            'approved' => 0,
        );

        $post = $this->postRepository->create($data);
        Activity::log([
            'user_id'     => Auth::id(),
            'contentId'   => $post->id,
            'contentType' => 'Article',
            'action'      => 'Create',
            'description' => 'New article created',
            'details'     => 'Article ID: '.$post->id,
        ]);
        
        Session::flash('message', 'Thanks for your submission!  We\'ll review your article and send you an email when it\'s approved.' ); 
        Session::flash('alert-class', 'alert-success'); 

        return Redirect::route('profile.articles', ['id' => Auth::id()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTutorial(StoreTutorialRequest $request)
    {
        $user = Auth::user();

        $zip_url = null;
        if (Input::file('zip')) {
            $file_name = Input::file('zip')->getClientOriginalName();
            $destination = Config::get('assets.tutorialZipsDirectory');
            $counter = 1;
            while (file_exists(public_path() . $destination . $file_name)) {
                $name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);
                $file_name = $name . '_' . $counter . '.' . $extension;
                $counter++;
            }
            $zip_file = Input::file('zip')->move(public_path() . $destination, $file_name);
            $zip_url = URL::to($destination, $file_name);
        }

        $data = array(
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'zip_url' => $zip_url,
        );

        Activity::log([
            'user_id'     => $user->id,
            'contentType' => 'Tutorial',
            'action'      => 'Create',
            'description' => 'New tutorial suggestion',
            'details'     => $zip_url,
        ]);
        
        Mail::send('emails.tutorial.suggestion', ['data' => $data, 'user' => $user], function ($m) use ($data) {
            $m->to(Config::get('settings.adminEmail'), Config::get('settings.siteName'))->subject('Tutorial Suggestion');
        });
        Session::flash('message', 'Thanks for your submission!  We\'ll review your tutorial and send you an email if it\'s approved.' ); 
        Session::flash('alert-class', 'alert-success'); 

        return Redirect::route('profile.articles', ['id' => $user->id]);
    }

}