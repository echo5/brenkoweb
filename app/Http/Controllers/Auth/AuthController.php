<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Auth;
use URL;
use Illuminate\Http\Request as Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Laravel\Socialite\Contracts\Factory as Socialite;
use App\Repositories\UserRepository;
use App\Events\UserCreated;
use Event;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Redirect to after login
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Socialite $socialite, UserRepository $users)
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        $this->socialite = $socialite;
        $this->users = $users;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $messages = [
          'g-recaptcha-response' => 'capcha'
        ];

        $rules = [
            'username' => 'required|max:30|unique:users',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'g-recaptcha-response' => 'required',
        ];

        $messages = [
            'g-recaptcha-response' => 'validation field',
        ];

        return Validator::make($data, $rules, [], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'username' => $data['username'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $response = Event::fire(new UserCreated($user));

        return $user;
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return 'username';
    }
    
    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        $field = filter_var($request->input($this->loginUsername()), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $request->merge([$field => $request->input($this->loginUsername())]);

        return $request->only($field, 'password');
    }

    /**
     * Get socialite provider
     * 
     * @param  $provider
     * @return Response
     */
    public function getSocialAuth($provider=null)
    {
        if (!config("services.$provider")) abort('404'); //just to handle providers that doesn't exist

        return $this->socialite->with($provider)->redirect();
    }

    /**
     * Catch socialite provider callback
     * 
     * @param  $provider
     * @return mixed
     */
    public function getSocialAuthCallback($provider=null)
    {
        if ($user = $this->socialite->with($provider)->user()) {
            $this->authenticateSocialUser($user, $provider);
            return redirect()->intended('/');
        } 
        else {
            return 'Something went wrong.  Please contact an administrator.';
        }
    }

    /**
     * Authenticate and login social user
     * 
     * @param  $socialUser
     * @param  $provider
     */
    public function authenticateSocialUser($socialUser, $provider)
    {
        $user = $this->users->findSocialUserOrCreate($socialUser, $provider);
        Auth::login($user);
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return URL::route('auth.login');
    }

}
