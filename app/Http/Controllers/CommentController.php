<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCommentRequest;
use App\Comment;
use Auth;
use Redirect;
use Session;

class CommentController extends Controller
{

    /**
     * @param Comment $comment
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentRequest $request)
    {
        $data = array(
            'post_id' => $request->input('post_id'),
            'content' => $request->input('content'),
            'user_id' => Auth::user()->id,
            'parent_id' => ($request->input('parent_id') == '' ? null : $request->input('parent_id'))
        );

        $newComment = $this->comment->create($data);

        Session::flash('message', 'Comment posted succesfully!'); 
        Session::flash('alert-class', 'alert-success'); 

        return Redirect::back();
    }


}
