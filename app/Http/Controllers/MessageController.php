<?php

namespace App\Http\Controllers;

use App\Message;
use App\Http\Requests\StoreMessageRequest;
use Session;
use Redirect;
use Input;
use Mail;
use Config;
use App\Activity;

class MessageController extends Controller {

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMessageRequest $request)
    {
        $data = array(
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'message' => $request->input('message'),
        );

        $message = Message::create($data);
        $string = (strlen($message->message) > 20) ? substr($message->message,0,17).'...' : $message->message;
        Activity::log([
            'contentId'   => $message->id,
            'contentType' => 'Message',
            'action'      => 'Create',
            'description' => $message->name,
            'details'     => $string,
        ]);

        Mail::send('emails.contact', ['data' => $data], function ($m) use ($data) {
            $m->to(Config::get('settings.adminEmail'), Config::get('settings.siteName'))->subject('Message From Contact on Brenko Web');
        });
        
        Session::flash('message', 'Thanks for your message!' ); 
        Session::flash('alert-class', 'alert-success'); 

        return Redirect::back();
    }
}