<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\BannerRepository;
use App\Repositories\BannerPlanRepository;
use App\Http\Requests\StoreBannerRequest;
use App\Http\Requests\TrackBannerRequest;
use App\Http\Requests\BannerAddCreditRequest;
use App\Banner;
use App\PayPalGateway;
use Route;
use App;
use Redirect;
use Auth;
use View;
use Input;
use Image;
use Session;
use App\Activity;
use URL;

class BannerController extends Controller
{

    /**
     * Create a new PostRepository instance.
     */
    public function __construct(BannerRepository $banner, BannerPlanRepository $plan)
    {
        $this->plan = $plan;
        $this->banner = $banner;
    }

    /**
     * Get link and add click
     * 
     * @return Response
     */
    public function getLink()
    {
        $banner_id = Route::getCurrentRoute()->getParameter('id');
        $banner = Banner::where('id', $banner_id)->first();
        if (!$banner)
            \App::abort(404);

        $banner->addClick();
        return Redirect::to($banner->link);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            $plans = $this->plan->all()->sortByDesc('created_at');
            $locations = $this->banner->getLocations();
            return View::make('banners.create', compact('plans', 'locations'));
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Track ad views
     *
     * @return bool
     */
    public function track(TrackBannerRequest $request)
    {
        if ($request->input('banner_ids')) {
            $this->banner->addImpression($request->input('banner_ids'));
            return 'Ad tracked successfully.';
        }
        return 'There was a problem tracking the ads.';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBannerRequest $request)
    {
        $img_name = '';
        if($img_file = Input::file('image')) {
            $img = Image::make($img_file);
            $img->backup();
            $img_name = str_slug(str_random(32)) . '.' . Input::file('image')->getClientOriginalExtension();
            $img->save( public_path() . config('assets.bannerImagesDirectory') . $img_name );
        }

        $data = array(
            'caption' => $request->input('caption'),
            'location' => $request->input('location'),
            'link' => $request->input('link'),
            'image' => $img_name,
            'plan_id' => $request->input('plan_id'),
            'user_id' => Auth::user()->id,
            'approved' => 0,
        );

        $banner = $this->banner->create($data);
        $plan = $this->plan->find($request->input('plan_id'));

        Activity::log([
            'user_id'     => Auth::id(),
            'contentId'   => $banner->id,
            'contentType' => 'Banner',
            'action'      => 'Create',
            'description' => 'New banner created',
            'details'     => 'Banner ID: '.$banner->id,
        ]);

        if ($plan->cost > 0) {
            $item = [
                'name' => $plan->name,
                'price' => $plan->cost,
                'description' => strip_tags($plan->description),
                'type' => 'DIGITAL'
            ];
            $paypal = new PayPalGateway;
            $paypal->setItems([$item]);
            $paypal->setCustom('user_id=' . Auth::id() . '&product_id=' . $plan->id . '&type=banner&model_id=' . $banner->id);
            return $paypal->postPayment();
        }
        
        Session::flash('message', 'Thanks for your submission!  We\'ll review your banner and send you an email when it\'s approved.' ); 
        Session::flash('alert-class', 'alert-success'); 

        return Redirect::back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addCredit(BannerAddCreditRequest $request)
    {
        $banner = $this->banner->find($request->input('banner_id'));
        $plan = $this->plan->find($request->input('plan_id'));

        Activity::log([
            'user_id'     => Auth::id(),
            'contentId'   => $banner->id,
            'contentType' => 'Banner',
            'action'      => 'Credit',
            'description' => 'Banner credit added',
            'details'     => 'Banner ID: '.$banner->id,
        ]);

        if ($plan->cost > 0) {
            $item = [
                'name' => $plan->name,
                'price' => $plan->cost,
                'description' => strip_tags($plan->description),
                'type' => 'DIGITAL'
            ];
            $paypal = new PayPalGateway;
            $paypal->setItems([$item]);
            $paypal->setCustom('user_id=' . Auth::id() . '&product_id=' . $plan->id . '&type=banner&model_id=' . $banner->id);
            return $paypal->postPayment();
        }
        
        Session::flash('message', 'You\'ve successfully added credit to your banner!' ); 
        Session::flash('alert-class', 'alert-success'); 

        return Redirect::back();
    }

}
