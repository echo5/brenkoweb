<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreScriptReviewRequest;
use App\ScriptReview;
use Auth;
use Session;
use Redirect;

class ScriptReviewController extends Controller
{

    /**
     * @param ScriptReview $scriptReview
     */
    public function __construct(ScriptReview $scriptReview)
    {
        $this->scriptReview = $scriptReview;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreScriptReviewRequest $request)
    {
        $data = array(
            'rating' => $request->input('rating'),
            'script_id' => $request->input('script_id'),
            'review' => $request->input('review'),
            'user_id' => Auth::user()->id,
        );

        $newReview = $this->scriptReview->create($data);

        Session::flash('message', 'Review posted succesfully!'); 
        Session::flash('alert-class', 'alert-success'); 

        return Redirect::back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

}
