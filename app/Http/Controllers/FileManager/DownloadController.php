<?php  

namespace App\Http\Controllers\FileManager;

use Tsawler\Laravelfilemanager\controllers\DownloadController as TsawlerDownloadController;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Auth;

/**
 * Class DownloadController
 * @package Tsawler\Laravelfilemanager\controllers
 */
class DownloadController extends TsawlerDownloadController {

    /**
     * @var
     */
    protected $file_location;


    /**
     * constructor
     */
    function __construct()
    {
        if (Auth::user()->level() >= 500) {
            if (Session::get('lfm_type') == "Images")   
                $this->file_location = Config::get('lfm.images_dir');
            else
                $this->file_location = Config::get('lfm.files_dir');
        }
        else {
            if (Session::get('lfm_type') == "Images")   
                $this->file_location = Config::get('lfm.images_dir') . 'users/' . Auth::id() . '/';
            else
                $this->file_location = Config::get('lfm.files_dir') . 'users/' . Auth::id() . '/';
        }
    }


    /**
     * Download a file
     *
     * @return mixed
     */
    public function getDownload()
    {
        $file_to_download = Input::get('file');
        $dir = Input::get('dir');
        return Response::download(base_path()
            .  "/"
            . $this->file_location
            .  $dir
            . "/"
            . $file_to_download);
    }

}
