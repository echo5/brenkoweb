<?php 

namespace App\Http\Controllers\FileManager;

use Tsawler\Laravelfilemanager\controllers\LfmController as TsawlerLfmController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Auth;

class LfmController extends TsawlerLfmController {

    /**
     * @var
     */
    protected $file_location;

    /**
     * Constructor
     */
    public function __construct()
    {
        if (Auth::user()->level() >= 500) {
            if ((Session::has('lfm_type')) && (Session::get('lfm_type') == 'Files'))
            {
                $this->file_location = Config::get('lfm.files_dir');
            } else
            {
                $this->file_location = Config::get('lfm.images_dir');
            }
        }
        else {
            if ((Session::has('lfm_type')) && (Session::get('lfm_type') == 'Files'))
            {
                $this->file_location = Config::get('lfm.files_dir') . 'users/' . Auth::id() . '/';
            } else
            {
                $this->file_location = Config::get('lfm.images_dir') . 'users/' . Auth::id() . '/';
            }
        }

    }


    /**
     * Show the filemanager
     *
     * @return mixed
     */
    public function show()
    {
        if ((Input::has('type')) && (Input::get('type') == "Files"))
        {
            Session::put('lfm_type', 'Files');
            $this->file_location = Config::get('lfm.files_dir');
        } else
        {
            Session::put('lfm_type', 'Images');
            $this->file_location = Config::get('lfm.images_dir');
        }

        if (Input::has('base'))
        {
            $working_dir = Input::get('base');
            $base = $this->file_location . Input::get('base') . "/";
        } else
        {
            $working_dir = "/";
            $base = $this->file_location;
        }

        if (Auth::user()->level() >= 500) {
            $user_base = '';
        }
        else {
            $user_base = 'users/' . Auth::id() . '/';
        }

        return View::make('laravel-filemanager::index')
            ->with('base', $base)
            ->with('working_dir', $working_dir)
            ->with('user_base', $user_base);
    }

}
