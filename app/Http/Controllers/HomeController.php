<?php

namespace App\Http\Controllers;

use App\Repositories\PostRepository;
use App\Repositories\ScriptRepository;
use App\Repositories\ListingRepository;
use Config;
use App\Post;

class HomeController extends Controller {

    /**
     * Create a new PostRepository instance.
     */
    public function __construct(PostRepository $article, postRepository $tutorial, ScriptRepository $script, ListingRepository $listing)
    {
        $this->article = $article;
        $this->tutorial = $tutorial;
        $this->script = $script;
        $this->listing = $listing;
    }

	/**
	 * Show the homepage to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $tutorials = $this->tutorial->getActiveByType('tutorial', Config::get('settings.home_tutorials'), 0, 'RAND()');
        $articles = $this->article->getActiveByType('article', Config::get('settings.home_articles'));
        $scripts = $this->script->getActive(Config::get('settings.home_scripts'), 0, 'RAND()');

        if (Config::get('settings.home_sponsored_listing')) {
            $sponsoredListings = $this->listing->getSponsored(1);
        }
        else {
            $sponsoredListings = null;
        }

		return view('pages.home', compact('articles', 'tutorials', 'scripts', 'sponsoredListings'));
	}

    /**
     * Load more posts for front page ajax
     * @param  integer $page
     * @return str
     */
    public function loadMore($page = 1)
    {
        $num_tutorials = 1;
        $num_articles = 4;
        $num_scripts = 1;
        $sponsoredListings = null;

        $tutorials = $this->tutorial->getActiveByType('tutorial', $num_tutorials, (($page - 1) * $num_tutorials) + Config::get('settings.home_tutorials'), 'RAND()');
        $articles = $this->article->getActiveByType('article', $num_articles, (($page - 1) * $num_articles) + Config::get('settings.home_articles'));
        $scripts = $this->script->getActive($num_scripts, (($page - 1) * $num_scripts) + Config::get('settings.home_scripts'), 'RAND()');

        $view = view('modules.home-grid', compact('articles', 'tutorials', 'scripts', 'sponsoredListings'));
        $content = (string) $view;
        return $view;
    }


}