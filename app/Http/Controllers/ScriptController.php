<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ScriptRepository;
use App\Http\Requests\StoreScriptRequest;
use App\PayPalGateway;
use App\Script;
use View;
use App;
use Config;
use Auth;
use Redirect;
use Session;
use Input;
use Image;
use Response;
use URL;

class ScriptController extends Controller
{

    /**
     * Create a new ScriptRepository instance.
     */
    public function __construct(ScriptRepository $script)
    {
        $this->script = $script;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('scripts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            if(!Auth::user()->paypal_email) {
                Session::flash('message', 'A valid PayPal email address is required to upload scripts.' ); 
                Session::flash('alert-class', 'alert-danger'); 
                return Redirect::route('profile.edit')->withErrors(['paypal_email'=>'Please add a PayPal email in order to upload scripts.']);
            }
            return View::make('scripts.create');
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreScriptRequest $request)
    {
        $img_name = '';
        if($img_file = Input::file('image')) {
            $img = Image::make($img_file);
            $img->backup();
            $img_name = str_slug(str_random(32)) . '.' . Input::file('image')->getClientOriginalExtension();
            $img->save( public_path() . config('assets.scriptImagesDirectory') . $img_name );
            $img->fit(340, 240);
            $img->save( public_path() . config('assets.scriptImageThumbsDirectory') . $img_name );
        }

        $file_name = '';
        if($file_name = Input::file('file')) {
            $file_name = str_slug(str_random(32)) . '.' . Input::file('file')->getClientOriginalExtension();
            Input::file('file')->move( public_path() . config('assets.scriptFilesDirectory'), $file_name );
        }

        $data = array(
            'name' => $request->input('name'),
            'version' => $request->input('version'),
            'preview_link' => $request->input('preview_link'),
            'description' => $request->input('description'),
            'features' => $request->input('features'),
            'requirements' => $request->input('requirements'),
            'price' => $request->input('price'),
            'file' => $file_name,
            'image' => $img_name,
            'category_id' => $request->input('category_id'),
            'user_id' => Auth::user()->id,
            'approved' => 0,
        );

        $post = $this->script->create($data);
        
        Session::flash('message', 'Thanks for your submission!  We\'ll review your listing and send you an email when it\'s approved.' ); 
        Session::flash('alert-class', 'alert-success'); 

        return Redirect::route('profile.scripts', ['id' => Auth::id()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $script = $this->script->findActiveById($id);
        $relatedScripts = $this->script->findRelated($script, 2);
        if (Auth::check())
            $purchased = Auth::user()->scriptPurchases()->where('script_id', $script->id)->first();
        else
            $purchased = false;
        if(!$script)
            App::abort('404');
        $script->increment('views');
        $script->views += 1;
        return View::make('scripts.show', compact('script', 'relatedScripts', 'purchased'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showSearch()
    {
        $scripts = Script::query();
        $results = null;
        if(Input::has('min_price'))
        {
            $scripts->where('price', '>=', Input::get('min_price'));
        }
        if(Input::has('max_price'))
        {
            $price = str_replace('$', '', Input::get('max_price'));
            $scripts->where('price', '<=', intval($price));
        }
        if(Input::has('category_id'))
        {
            $categories = json_decode(Input::get('category_id'));
            if (is_array($categories)) {
                $scripts->whereIn('category_id', $categories);
            } else {
                $scripts->where('category_id', Input::get('category_id'));
            }
        }
        if(Input::has('name'))
        {
            $scripts->where('name', 'LIKE', '%' . Input::get('name') . '%');
            $results = $scripts->get();
            $results = $results->where('status', 'approved');
        }
        return View::make('scripts.search', ['scripts' => $results]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Download script if purchased
     * 
     * @param  int $id
     * @return Response
     */
    public function download($id)
    {
        if (Auth::check()) {
            $script = $this->script->findActiveById($id);
            if ($script->price == 0 || Auth::user()->scriptPurchases()->where('script_id', $script->id)->first()) {
                $file_path = public_path() . config('assets.scriptFilesDirectory') . $script->file;
                if (file_exists($file_path)) {
                    $script->increment('downloads');
                    $script->downloads += 1;
                    $ext = pathinfo($file_path, PATHINFO_EXTENSION);
                    $response = Response::download($file_path, str_slug($script->name) . '-' . $script->version . '.' .$ext);             
                    ob_end_clean();
                    return $response;
                }
                return 'File does not exist.  Please contact an administrator.';
            }
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Forward to payment page
     * 
     * @param  int $id
     * @return Redirect
     */
    public function purchase($id)
    {
        if (Auth::check()) {
            $script = $this->script->findActiveById($id);
            if ($script->price > 0) {
                $paypal = new PayPalGateway;
                $receivers = [
                    [
                        'email' => Config::get('paypal.email'),
                        'amount' => $script->price,
                        'primary' => 'true'
                    ],
                    [
                        'email' => $script->user()->first()->email,
                        'amount' => $script->getPriceSplit()['user'],
                        'primary' => 'false'
                    ]
                ];
                Session::put('product_id', $script->id);
                $paypal->setReceivers($receivers);
                return $paypal->postChainedPayment();
            }
        }
        return Redirect::guest(URL::route('auth.login'));
    }

}
