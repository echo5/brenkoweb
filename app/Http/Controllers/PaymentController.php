<?php

namespace App\Http\Controllers;

use Config;
use URL;
use Session;
use Redirect;
use Input;
use View;
use Auth;
use Event;
use App\Events\ScriptPurchased;
use App\Events\BannerCreditPurchased;
use App\Events\SponsoredListingPurchased;
use App\PayPalGateway;
use App\Repositories\ListingRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\BannerRepository;
use App\Repositories\BannerPlanRepository;
use App\Repositories\UserRepository;
use App\Repositories\ScriptRepository;

class PaymentController extends Controller
{

    /**
     * Setup payment controller
     */
    public function __construct(
            ListingRepository $listing,
            OrderRepository $order,
            OrderItemRepository $orderItem,
            BannerRepository $banner,
            BannerPlanRepository $bannerPlan,
            UserRepository $user,
            ScriptRepository $script
        )
    {
        $this->listing = $listing;
        $this->order = $order;
        $this->orderItem = $orderItem;
        $this->banner = $banner;
        $this->bannerPlan = $bannerPlan;
        $this->user = $user;
        $this->script = $script;
    }

    /**
     * Execute payment
     * 
     * @return response
     */
    public function execute()
    {
        if (empty(Session::get('paypal_payment_id')) &&  (empty(Input::get('PayerID')) || empty(Input::get('token')))) {
            return Redirect::route('payment.failed')
                ->with('error', 'Payment failed');
        }

        $payment_id = Input::get('paymentId') ? Input::get('paymentId') : Session::get('paypal_payment_id');
        $paypal = new PayPalGateway;
        $payment = $paypal->getPayment($payment_id);
        $result = $paypal->executePayment($payment, Input::get('PayerID'));
        Session::forget('paypal_payment_id');

        if ($result->getState() == 'approved') {
            $custom = $this->getCustom($payment->id);
            $order = $this->storeOrder($payment, $custom);
            $this->storeOrderItems($payment, $order, $custom);
            return View::make('payment.success', compact('order'));
        }
        return Redirect::route('payment.failed')
            ->with('error', 'Payment failed');
    }

    /**
     * Execute adaptive payment
     * 
     * @return Response
     */
    public function executeAdaptive()
    {
        if (empty(Session::get('paypal_payment_id')) || empty(Session::get('product_id'))) {
        return Redirect::route('payment.failed')
            ->with('error', 'Payment failed');
        } 

        $payment_id = Session::get('paypal_payment_id');
        $script_id = Session::get('product_id');
        $paypal = new PayPalGateway;
        $payment = $paypal->getAdaptivePayment($payment_id);
        Session::forget('paypal_payment_id');
        Session::forget('product_id');

        if ($payment['Status'] == 'COMPLETED') {
            $order = $this->storeScriptPurchase($payment_id, $script_id);
            return View::make('payment.success', compact('order'));
        }
        return Redirect::route('payment.failed')
            ->with('error', 'Payment failed');

    }

    /**
     * Store order info
     * 
     * @param  $result
     * @param  Payment $payment
     * @return Order $order
     */
    public function storeOrder($payment, $custom)
    {
        $paypal = new PayPalGateway;
        $payer = $payment->getPayer();
        $payer_info = $payer->getPayerInfo();

        $data = [
            'payer_name' => $payer_info->first_name . ' ' . $payer_info->last_name,
            'payer_email' => $payer_info->email,
            'payer_id' => $payer_info->payer_id,
            'payment_status' => 'approved',
            'payment_id' => $payment->id,
            'payment_method' => $payer->payment_method,
            'type' => $custom['type'],
            'user_id' => $custom['user_id'],
        ];
        $order = $this->order->create($data);
        return $order;
    }

    /**
     * Store script purchase order info
     * @param  $payment_id
     * @return $order
     */
    public function storeScriptPurchase($payment_id, $script_id)
    {
        $paypal = new PayPalGateway;
        $payment = $paypal->getAdaptivePayment($payment_id);
        $xml_response = simplexml_load_string($payment['XMLResponse']);
        $data = [
            'payer_email' => $payment['SenderEmail'],
            'payer_id' => (string)$xml_response->sender->accountId,
            'payment_status' => 'approved',
            'payment_id' => $payment_id,
            'payment_method' => 'paypal',
            'type' => 'script',
            'user_id' => Auth::id(),
        ];
        $order = $this->order->create($data);

        $script = $this->script->find($script_id);
        $data = [
            'order_id' => $order->id,
            'type' => 'script',
            'name' => $script->name,
            'product_id' => $script->id,
            'model_id' => $script->id,
            'price' => $script->price,
            'site_revenue' => $script->getPriceSplit()['site']
        ];
        $orderItem = $this->orderItem->create($data);
        $this->creditAccount('script', Auth::id(), $script->id, $script->id, $order);

        return $order;
    }

    /**
     * Store items of order
     * 
     * @param  Payment $payment
     * @param  $order_id
     */
    public function storeOrderItems($payment, $order, $custom)
    {
        $paypal = new PayPalGateway;
        $items = $payment->getTransactions()[0]->getItemList()->items;

        foreach ($items as $item) {
            $data = [
                'order_id' => $order->id,
                'type' => $custom['type'],
                'name' => $item->name,
                'product_id' => $custom['product_id'],
                'model_id' => $custom['model_id'],
                'price' => $item->price,
                'site_revenue' => $item->price
            ];
            $orderItem = $this->orderItem->create($data);

            $this->creditAccount($custom['type'], $custom['user_id'], $custom['product_id'], $custom['model_id'], $order);
        }
    }

    /**
     * Get custom paramters from payment
     * 
     * @param  str $payment_id
     * @return array $custom
     */
    public function getCustom($payment_id)
    {
        $paypal = new PayPalGateway;
        $payment = $paypal->getPayment($payment_id);
        $custom = $paypal->parseCustom($payment);
        return $custom;
    }

    /**
     * Credit account with purchases
     * 
     * @param  $type       Purchase type
     * @param  $user_id
     * @param  $product_id Plan or product id
     * @param  $model_id   ID for model
     */
    public function creditAccount($type, $user_id, $product_id, $model_id = null, $order)
    {
        $user = $this->user->find($user_id);
        switch ($type) {
            case 'banner':
                $banner = $this->banner->find($model_id);
                $bannerPlan = $this->bannerPlan->find($product_id);
                $response = Event::fire(new BannerCreditPurchased($user, $order, $bannerPlan, $banner));
                Session::flash('message', 'Your banner credit has been added!  If this is a new submission we\'ll review your banner and send you an email when it\'s approved.' ); 
                Session::flash('alert-class', 'alert-success'); 
                break;
            case 'script':
                $script = $this->script->find($product_id);
                $response = Event::fire(new ScriptPurchased($user, $order, $script));
                Session::flash('message', 'Thanks for purchasing!  Please visit <a href="'. URL::route('profile.downloads') .'">your downloads</a> page to download the script.' ); 
                Session::flash('alert-class', 'alert-success'); 
                break;
            case 'listing':
                $listing = $this->listing->find($model_id);
                $response = Event::fire(new SponsoredListingPurchased($user, $order, $listing));
                Session::flash('message', 'Thanks for your submission!  We\'ll review your listing and send you an email when it\'s approved.' ); 
                Session::flash('alert-class', 'alert-success'); 
                break;
        }
    }

    /**
     * Canceled payment page
     * 
     * @return View
     */
    public function canceled()
    {
        return View::make('payment.canceled');
    }

    /**
     * Failed payment page
     * 
     * @return View
     */
    public function failed()
    {
        return View::make('payment.failed');
    }


}
