<?php

namespace App\Http\Controllers;

use App\User;
use App\OrderItem;
use App\Repositories\UserRepository;
use App\Repositories\BannerRepository;
use App\Repositories\BannerPlanRepository;
use App\Http\Requests\UpdateUserRequest;
use Auth;
use Redirect;
use Session;
use Image;
use Input;
use URL;

class UserController extends Controller
{
    /**
     * Create a new UserRepository instance.
     */
    public function __construct(UserRepository $user, BannerRepository $banner, BannerPlanRepository $plan)
    {
        $this->user = $user;
        $this->banner = $banner;
        $this->plan = $plan;
    }

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showProfile($id)
    {
        $user = $this->user->findProfile($id);
        if (!$user)
            \App::abort('404');
        return view('profile.show', compact('user'));
    }

    /**
     * Show the articles for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showArticles($id)
    {
        $user = $this->user->findProfile($id);
        if (!$user)
            \App::abort('404');
        return view('profile.articles', compact('user'));
    }

    /**
     * Show the scripts for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showScripts($id)
    {
        $user = $this->user->findProfile($id);
        if (!$user)
            \App::abort('404');
        return view('profile.scripts', compact('user'));
    }

    /**
     * Show the purchases for the logged in user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showDownloads()
    {
        if (Auth::check()) {
            $user = $this->user->findProfile(Auth::id());
            $scriptPurchases = $user->scriptPurchases->unique();
            if (!$user)
                \App::abort('404');
            return view('profile.downloads', compact('user', 'scriptPurchases'));
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Show the purchases for the logged in user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showOrders()
    {
        if (Auth::check()) {
            $user = $this->user->findProfile(Auth::id());
            $orders = $user->orders;
            if (!$user)
                \App::abort('404');
            return view('profile.orders', compact('user', 'orders'));
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Show the purchases for the logged in user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showOrder($id)
    {
        if (Auth::check()) {
            $user = $this->user->findProfile(Auth::id());
            $order = $user->orders->where('id', $id)->first();
            if (!$user || !$order)
                \App::abort('404');
            $orderItems = OrderItem::where('order_id', $order->id)->get();
            return view('profile.order', compact('user', 'order', 'orderItems'));
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Show the listings for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showListings($id)
    {
        $user = $this->user->findProfile($id);
        if (!$user)
            \App::abort('404');
        return view('profile.listings', compact('user'));
    }

    /**
     * Show the listings for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showReviews($id)
    {
        $user = $this->user->findProfile($id);
        if (!$user)
            \App::abort('404');
        return view('profile.reviews', compact('user'));
    }

    /**
     * Show the banners for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showBanners($id)
    {
        $user = $this->user->findProfile($id);
        if (!$user)
            \App::abort('404');
        return view('profile.banners', compact('user'));
    }

    /**
     * Show the banners for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showBannerStats($id)
    {
        if (Auth::check()) {
            $user = $this->user->findProfile(Auth::user()->id);
            $banner = $user->banners->find($id);
            $total_impressions = $banner->stats->sum('impressions');
            $total_clicks = $banner->stats->sum('clicks');
            if (!$banner)
                \App::abort('404');
            return view('profile.banner.show', compact('user','banner', 'total_impressions', 'total_clicks'));
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Show the banner add credit form
     *
     * @param  int  $id
     * @return Response
     */
    public function showBannerAddCredit($id)
    {
        if (Auth::check()) {
            $user = $this->user->findProfile(Auth::user()->id);
            $banner = $user->banners->find($id);
            $plans = $this->plan->all()->sortByDesc('created_at');
            if (!$banner)
                \App::abort('404');
            return view('profile.banner.addcredit', compact('user','banner', 'plans'));
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editProfile()
    {
        if (Auth::check()) {
            $user = $this->user->findProfile(Auth::user()->id);
            return view('profile.edit', ['user' => $user]);
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request)
    {
        if (Auth::check()) {
            if($request->input('avatar_file')) {
                if ($img = Image::make($request->input('avatar_file'))) {
                    $old_img = Auth::user()->avatar;
                    if (file_exists(public_path() . config('assets.userAvatarsDirectory') . 'large/' . $old_img))						
                    	unlink( public_path() . config('assets.userAvatarsDirectory') . 'large/' . $old_img );
                    if (file_exists(public_path() . config('assets.userAvatarsDirectory') . 'medium/' . $old_img))
                    	unlink( public_path() . config('assets.userAvatarsDirectory') . 'medium/' . $old_img );
                    if (file_exists(public_path() . config('assets.userAvatarsDirectory') . 'small/' . $old_img))
                        unlink( public_path() . config('assets.userAvatarsDirectory') . 'small/' . $old_img );
                } 
                $img->backup();
                $img_name = str_slug(str_random(32)) . '.' . Input::file('avatar_file')->getClientOriginalExtension();
                $img->fit(200, 200);
                $img->save( public_path() . config('assets.userAvatarsDirectory') . 'large/' . $img_name );
                $img->reset();
                $img->fit(100, 100);
                $img->save( public_path() . config('assets.userAvatarsDirectory') . 'medium/' . $img_name );
                $img->reset();
                $img->fit(30, 30);
                $img->save( public_path() . config('assets.userAvatarsDirectory') . 'small/' . $img_name );
                $request->merge(array('avatar' => $img_name));
            }
            $user = $this->user->update($request->all(), Auth::user()->id);
            Session::flash('message', 'User profile updated!' ); 
            Session::flash('alert-class', 'alert-success'); 
            return Redirect::back();
        }
        return Redirect::guest(URL::route('auth.login'));
    }

}