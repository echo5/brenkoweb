<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ListingPlanRepository;
use App\Repositories\ListingRepository;
use App\Repositories\OrderRepository;
use App\Http\Requests\StoreListingRequest;
use App\PayPalGateway;
use View;
use Auth;
use Redirect;
use Session;
use Input;
use Image;
use URL;
use App\Activity;

class ListingController extends Controller
{

    /**
     * Create a new ListingRepository instance.
     */
    public function __construct(ListingRepository $listing, ListingPlanRepository $plan, OrderRepository $order)
    {
        $this->plan = $plan;
        $this->listing = $listing;
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('listings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            $plans = $this->plan->all();
            return View::make('listings.create', compact('plans'));
        }
        return Redirect::guest(URL::route('auth.login'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreListingRequest $request)
    {
        $img_name = '';
        if($img_file = Input::file('image')) {
            $img = Image::make($img_file);
            $img->backup();
            $img_name = str_slug(str_random(32)) . '.' . Input::file('image')->getClientOriginalExtension();
            $img->save( public_path() . config('assets.listingImagesDirectory') . $img_name );
            $img->fit(340, 240);
            $img->save( public_path() . config('assets.listingImageThumbsDirectory') . $img_name );
        }

        $data = array(
            'title' => $request->input('title'),
            'link' => $request->input('link'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'plan_id' => $request->input('plan_id'),
            'image' => $img_name,
            'user_id' => Auth::user()->id,
            'approved' => 0,
        );

        $listing = $this->listing->create($data);
        $plan = $this->plan->find($request->input('plan_id'));

        Activity::log([
            'user_id'     => Auth::id(),
            'contentId'   => $listing->id,
            'contentType' => 'Listing',
            'action'      => 'Create',
            'description' => 'New listing created',
            'details'     => 'Listing ID: '.$listing->id,
        ]);

        if ($plan->cost > 0) {
            $item = [
                'name' => $plan->name,
                'price' => $plan->cost,
                'description' => strip_tags($plan->description),
                'type' => 'DIGITAL'
            ];
            $paypal = new PayPalGateway;
            $paypal->setItems([$item]);
            $paypal->setCustom('user_id=' . Auth::id() . '&product_id=' . $plan->id . '&type=listing&model_id=' . $listing->id);
            return $paypal->postPayment();
        }
        
        Session::flash('message', 'Thanks for your submission!  We\'ll review your listing and send you an email when it\'s approved.' ); 
        Session::flash('alert-class', 'alert-success'); 

        return Redirect::route('profile.listings', ['id' => Auth::id()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Redirect
     */
    public function show($id)
    {
        $listing = $this->listing->findActiveById($id);
        if(!$listing)
            App::abort('404');
        $listing->increment('clicks');
        $listing->clicks += 1;
        return Redirect::to($listing->link);
    }

}
