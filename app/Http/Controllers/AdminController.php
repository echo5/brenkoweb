<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UpdateReadActivitiesRequest;
use App\Http\Controllers\Controller;
use Auth;

class AdminController extends Controller
{
    /**
     * Update read activities in pivot
     *
     * @return bool
     */
    public function updateReadActivities(UpdateReadActivitiesRequest $request)
    {
        $activities = $request->input('activities');
        $user = Auth::user();
        $user->readActivities()->attach($activities);
        return 'updated';
    }


}
