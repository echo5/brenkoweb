<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Development
$router->get( '/_debugbar/assets/stylesheets', '\Barryvdh\Debugbar\Controllers\AssetController@css' );
$router->get( '/_debugbar/assets/javascript', '\Barryvdh\Debugbar\Controllers\AssetController@js' );

// Pages
Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);
Route::get('/loadmore/{page}', ['uses' => 'HomeController@loadMore', 'as' => 'loadmore']);

// Codeparser
Route::get('/codeparser', ['uses' => 'CodeparserController@show', 'as' => 'codeparser']);

// Ads/Banners
Route::get('/ad/create', ['uses' => 'BannerController@create', 'as' => 'banner.create']);
Route::post('/ad/create', ['uses' => 'BannerController@store', 'as' => 'banner.store']);
Route::post('/ad/track', ['uses' => 'BannerController@track', 'as' => 'banner.track']);
Route::post('/ad/{$id}/addcredit', ['uses' => 'BannerController@addCredit', 'as' => 'banner.addcredit']);
Route::get('/ad/{id}', ['uses' => 'BannerController@getLink', 'as' => 'banner']);

// Authentication routes
Route::get('login', ['uses' => 'Auth\AuthController@getLogin', 'as' => 'auth.login']);
Route::post('login', ['uses' => 'Auth\AuthController@postLogin', 'as' => 'auth.login', 'middleware'=>'auth.status']);
Route::get('logout', ['uses' => 'Auth\AuthController@getLogout', 'as' => 'auth.logout']);

// Password reset link request routes
Route::get('password/email', ['uses' => 'Auth\PasswordController@getEmail', 'as' => 'password.email']);
Route::post('password/email', ['uses' => 'Auth\PasswordController@postEmail', 'as' => 'password.email']);
Route::get('password/reset/{token}', ['uses' => 'Auth\PasswordController@getReset', 'as' => 'password.reset']);
Route::post('password/reset', ['uses' => 'Auth\PasswordController@postReset', 'as' => 'password.reset']);

// Social Login
Route::get('login/{provider?}',['uses' => 'Auth\AuthController@getSocialAuth', 'as'   => 'auth.social']);
Route::get('login/callback/{provider?}',[ 'uses' => 'Auth\AuthController@getSocialAuthCallback', 'as'   => 'auth.getSocialAuthCallback', 'middleware'=>'auth.status']);

// Registration routes
Route::get('register', ['uses' => 'Auth\AuthController@getRegister', 'as' => 'auth.register']);
Route::post('register', ['uses' => 'Auth\AuthController@postRegister', 'as' => 'auth.register']);

// User profile
Route::get('/user/{id}/profile', ['uses' => 'UserController@showProfile', 'as' => 'profile.show']);
Route::get('/user/{id}/articles', ['uses' => 'UserController@showArticles', 'as' => 'profile.articles']);
Route::get('/user/{id}/scripts', ['uses' => 'UserController@showScripts', 'as' => 'profile.scripts']);
Route::get('/user/{id}/listings', ['uses' => 'UserController@showListings', 'as' => 'profile.listings']);
Route::get('/user/{id}/banners', ['uses' => 'UserController@showBanners', 'as' => 'profile.banners']);
Route::get('/user/{id}/reviews', ['uses' => 'UserController@showReviews', 'as' => 'profile.reviews']);
Route::get('/user/banner/{id}', ['uses' => 'UserController@showBannerStats', 'as' => 'profile.banner.stats']);
Route::get('/user/banner/{id}/addcredit', ['uses' => 'UserController@showBannerAddCredit', 'as' => 'profile.banner.addcredit']);
Route::get('/user/downloads', ['uses' => 'UserController@showDownloads', 'as' => 'profile.downloads']);
Route::get('/user/orders', ['uses' => 'UserController@showOrders', 'as' => 'profile.orders']);
Route::get('/user/order/{id}', ['uses' => 'UserController@showOrder', 'as' => 'profile.order']);
Route::get('/user/profile/edit', ['uses' => 'UserController@editProfile', 'as' => 'profile.edit']);
Route::post('/user/profile/edit', ['uses' => 'UserController@update', 'as' => 'profile.edit']);

// Directory
Route::get('directory', ['uses' => 'ListingController@index', 'as' => 'listing.index']);
Route::get('listing/create', ['uses' => 'ListingController@create', 'as' => 'listing.create']);
Route::post('listing/create', ['uses' => 'ListingController@store', 'as' => 'listing.store']);
Route::get('directory/{hierarchy}', ['uses' => 'ListingController@index', 'as' => 'listing.index'])->where('hierarchy', '(.*)?');
Route::get('listing/{id}', ['uses' => 'ListingController@show', 'as' => 'listing.show']);

// Scripts
Route::get('marketplace', ['uses' => 'ScriptController@index', 'as' => 'scripts.index']);
Route::get('marketplace/search', ['uses' => 'ScriptController@showSearch', 'as' => 'script.search']);
Route::get('marketplace/item/create', ['uses' => 'ScriptController@create', 'as' => 'script.create']);
Route::post('marketplace/item/create', ['uses' => 'ScriptController@store', 'as' => 'script.store']);
Route::get('marketplace/category/{hierarchy}', ['uses' => 'ScriptController@index', 'as' => 'scripts.index'])->where('hierarchy', '(.*)?');
Route::get('marketplace/item/{id}', ['uses' => 'ScriptController@show', 'as' => 'script.show']);
Route::get('marketplace/item/download/{id}', ['uses' => 'ScriptController@download', 'as' => 'script.download']);
Route::get('marketplace/item/purchase/{id}', ['uses' => 'ScriptController@purchase', 'as' => 'script.purchase']);
Route::post('marketplace/item/{id}', ['uses' => 'ScriptReviewController@store', 'as' => 'review.store']);

// Payment
// Route::get('payment', ['as' => 'payment', 'uses' => 'PaymentController@postPayment']);
Route::get('payment/execute', ['as' => 'payment.execute', 'uses' => 'PaymentController@execute']);
Route::get('payment/executea', ['as' => 'payment.executeAdaptive', 'uses' => 'PaymentController@executeAdaptive']);
Route::get('payment/canceled', ['as' => 'payment.canceled', 'uses' => 'PaymentController@canceled']);
Route::get('payment/failed', ['as' => 'payment.failed', 'uses' => 'PaymentController@failed']);

// Override and add admin package scripts/styles
Route::post('admin/readactivities', ['uses' => 'AdminController@updateReadActivities', 'as' => 'admin.readactivities']);
View::composer(array('administrator::layouts.default'), function($view)
{
    $view->css['main'] = asset('css/admin/bootstrap.css');
    $view->css['font-awesome'] = asset('css/admin/font-awesome.min.css');
    $view->css['admin-lte'] = asset('css/admin/AdminLTE.css');
    $view->css['all-skins'] = asset('css/admin/_all-skins.css');
    $view->css['admin'] = asset('css/admin/main.css');
    // $view->css['skin-blue'] = asset('css/admin/skin-blue.css');
    
    $view->js['jquery'] = '';
    $view->js['bootstrap'] = asset('js/admin/bootstrap.min.js');
    $view->js['app'] = asset('js/admin/app.js');
    $view->js['knockout'] = asset('js/admin/knockout-2.2.1.js');
    $view->js['activity'] = asset('js/admin/activity.js');
    $view->js['ckeditor'] = asset('js/admin/ckeditor/ckeditor.js');
    $view->js['flot'] = asset('js/jquery.flot.min.js');
    $view->js['flot-selection'] = asset('js/jquery.flot.selection.min.js');
    $view->js['flot-time'] = asset('js/jquery.flot.time.min.js');
    $view->js['flot-pie'] = asset('js/jquery.flot.pie.js');
});

// Laravel filemanager routes
// Route::get('/filemanager', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\LfmController@show']);
// Route::any('/filemanager/upload', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\UploadController@upload']);
// Route::get('/filemanager/jsonimages', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\ItemsController@getImages']);
// Route::get('/filemanager/jsonfiles', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\ItemsController@getFiles']);
// Route::get('/filemanager/newfolder', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\FolderController@getAddfolder']);
// Route::get('/filemanager/deletefolder', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\FolderController@getDeletefolder']);
// Route::get('/filemanager/folders', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\FolderController@getFolders']);
Route::get('/filemanager', ['middleware' => 'auth', 'uses' => 'FileManager\LfmController@show']);
Route::any('/filemanager/upload', ['middleware' => 'auth', 'uses' => 'FileManager\UploadController@upload']);
Route::get('/filemanager/jsonimages', ['middleware' => 'auth', 'uses' => 'FileManager\ItemsController@getImages']);
Route::get('/filemanager/jsonfiles', ['middleware' => 'auth', 'uses' => 'FileManager\ItemsController@getFiles']);
Route::get('/filemanager/newfolder', ['middleware' => 'auth', 'uses' => 'FileManager\FolderController@getAddfolder']);
Route::get('/filemanager/deletefolder', ['middleware' => 'auth', 'uses' => 'FileManager\FolderController@getDeletefolder']);
Route::get('/filemanager/folders', ['middleware' => 'auth', 'uses' => 'FileManager\FolderController@getFolders']);

// Laravel filemanager admin options
// Route::get('/filemanager/download', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\DownloadController@getDownload']);
// Route::get('/filemanager/delete', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\DeleteController@getDelete']);
// Route::get('/filemanager/doresize', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\ResizeController@performResize']);
// Route::get('/filemanager/crop', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\CropController@getCrop']);
// Route::get('/filemanager/cropimage', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\CropController@getCropimage']);
Route::get('/filemanager/rename', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\RenameController@getRename']);
Route::get('/filemanager/resize', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\ResizeController@getResize']);
Route::get('/laravel-filemanager/cropimage', ['middleware' => 'level:100', 'uses' => '\Tsawler\Laravelfilemanager\controllers\CropController@getCropimage']);
Route::get('/filemanager/crop', ['middleware' => 'level:100', 'uses' => 'FileManager\CropController@getCrop']);
Route::get('/laravel-filemanager/doresize', ['middleware' => 'level:100', 'uses' => 'FileManager\ResizeController@performResize']);
Route::get('/filemanager/delete', ['middleware' => 'level:100', 'uses' => 'FileManager\DeleteController@getDelete']);
Route::get('/filemanager/download', ['middleware' => 'level:100', 'uses' => 'FileManager\DownloadController@getDownload']);

// Post types
Route::get('article/create', ['uses' => 'PostController@createArticle', 'as' => 'article.create', 'type' => 'article']);
Route::post('article/create', ['uses' => 'PostController@storeArticle', 'as' => 'article.store', 'type' => 'article']);
Route::get('tutorial/create', ['uses' => 'PostController@createTutorial', 'as' => 'tutorial.create', 'type' => 'tutorial']);
Route::post('tutorial/create', ['uses' => 'PostController@storeTutorial', 'as' => 'tutorial.store', 'type' => 'tutorial']);
Route::get('articles', ['uses' => 'PostController@showCategory', 'as' => 'post.articles', 'type' => 'articles']);
Route::get('tutorials', ['uses' => 'PostController@showCategory', 'as' => 'post.tutorials', 'type' => 'tutorials']);
Route::get('articles/{hierarchy}', ['uses' => 'PostController@showBySlug', 'as' => 'post.article', 'type' => 'articles'])->where('hierarchy', '(.*)?');
Route::post('articles/{hierarchy}', ['uses' => 'CommentController@store', 'as' => 'comment.store', 'type' => 'articles'])->where('hierarchy', '(.*)?');
Route::get('tutorials/{hierarchy}', ['uses' => 'PostController@showBySlug', 'as' => 'post.tutorial', 'type' => 'tutorials'])->where('hierarchy', '(.*)?');
Route::post('tutorials/{hierarchy}', ['uses' => 'CommentController@store', 'as' => 'comment.store', 'type' => 'tutorials'])->where('hierarchy', '(.*)?');
Route::post('/contact', ['uses' => 'MessageController@store', 'as' => 'contact.store']);
Route::get('{slug}', ['uses' => 'PostController@showPage', 'as' => 'post.page']);
