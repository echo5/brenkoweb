<?php namespace App\Http\Composers;

use App\Repositories\EmailTemplateRepository;
use App\Services\Shortcodes;
use Route;
use Shortcode;

class EmailTemplatesComposer {

    /**
     * EmailTemplate instance.
     *
     * @var EmailTemplate
     */
    protected $emailTemplate;

    /**
     * Create a new EmailTemplate instance.
     */
	public function __construct(EmailTemplateRepository $emailTemplate, Shortcodes $shortcodes)
    {
    	$this->emailTemplate = $emailTemplate;
        $this->shortcodes = $shortcodes;
    }
	
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $data = $view->getData();
        $view_name = $view->getName();
        
        if ($view_name != 'emails.contact' && $view_name != 'emails.base'){
            $this->shortcodes->setUser($data['user']);
        }
        if (preg_match('/emails.article.*/', $view_name) ) {
            $this->shortcodes->setPost($data['post']);
        }
        if (preg_match('/emails.banner.*/', $view_name) ) {
            $this->shortcodes->setBanner($data['banner']);
        }
        if (preg_match('/emails.listing.*/', $view_name) ) {
            $this->shortcodes->setListing($data['listing']);
        }
        if (preg_match('/emails.script.*/', $view_name) ) {
            $this->shortcodes->setScript($data['script']);
        }
        if (preg_match('/emails.*.purchase/', $view_name) ) {
            $this->shortcodes->setOrder($data['order']);
        }
        if ($view_name == 'emails.password') {
            $this->shortcodes->setToken($data['token']);
        }

        $this->emailTemplate = $this->emailTemplate->findBy('view', $view->getName());
        $this->emailTemplate['content'] = Shortcode::compile($this->emailTemplate['content']);
        $view->with(array('email' => $this->emailTemplate));
    }

}