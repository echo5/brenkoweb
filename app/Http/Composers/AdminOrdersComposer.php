<?php namespace App\Http\Composers;

use App\Order;
use App\OrderItem;
use Route;
use Input;

class AdminOrdersComposer {

    /**
     * Create a new Order instance.
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $order = $this->order->find(Input::get('id'));
        $view->with(array(
            'order' => $order,
        ));
    }

}