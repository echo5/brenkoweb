<?php namespace App\Http\Composers;

use App\ListingCategory;
use App\Repositories\ListingRepository;
use Route;

class ListingsComposer {

    /**
     * Category instances.
     */
    protected $categories;

    /**
     * Create a new ListingCategory instance.
     */
    public function __construct(ListingCategory $listingCategory, ListingRepository $listing)
    {
        $this->categories = $listingCategory
            ->with('children')
            ->with('parent')
            ->with('listings')
            ->get();
        $this->allCategories = $this->categories;
        $this->setCurrentCategory();
        $this->setCategories();


        $this->listing = $listing;
        $this->sponsoredListings = $listing->getSponsored(2);
        $limit = 9 - count($this->sponsoredListings);
        $this->currentListings = $this->getCurrentListings($limit);
        $this->randomListings = $listing->getRandom('4');
    }
    
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'allListingCategories' => $this->allCategories,
            'listingCategories' => $this->categories,
            'currentListings' => $this->currentListings,
            'sponsoredListings' => $this->sponsoredListings,
            'randomListings' => $this->randomListings,
            'currentCategory' => $this->currentCategory
        ));
    }

    /**
     * Determine and set current category
     */
    public function setCurrentCategory()
    {
        $hierarchy = Route::getCurrentRoute()->getParameter('hierarchy');
        $url_categories = explode('/', $hierarchy);
        $category_slug = end($url_categories);
        $this->currentCategory = $this->categories->where('slug', $category_slug)->first();
    }

    /**
     * Set categories for display
     */
    public function setCategories()
    {
        if ($this->currentCategory) 
            $this->categories = $this->categories->where('parent_id', $this->currentCategory->id);
        else
            $this->categories = $this->categories->where('parent_id', null);  
    }

    /**
     * Set listings for display
     */
    public function getCurrentListings($limit)
    {
        $this->currentListings = null;
        if ($this->currentCategory)
            return $this->listing->getActiveByCategoryId($this->currentCategory->id, $limit);

        return $this->listing->getAllActive($limit);
    }

}