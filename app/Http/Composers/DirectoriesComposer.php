<?php namespace App\Http\Composers;

use App\ListingCategory;
use Illuminate\Support\Collection;

class DirectoriesComposer {

    /**
     * Category instances.
     */
    protected $categories;

    /**
     * Create a new ListingCategory instance.
     */
    public function __construct(ListingCategory $listingCategory)
    {
        $this->categories = $listingCategory->where('parent_id', NULL)
            ->with('children')
            ->with('parent')
            ->with('listings')
            ->with('children.listings')
            ->get();
        $this->popularListings = $this->getPopularListings();
    }
    
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'listingCategories' => $this->categories,
            'popularListings' => $this->popularListings,
        ));
    }

    /**
     * Get top 5 popular listings by clicks
     *
     * @return Collection $popularListings
     */
    public function getPopularListings()
    {
        $popularListings = new Collection;
        foreach ($this->categories as $category) {
            $popularListings = $popularListings->merge($category->listings);
            if (count($category->children)) {
                foreach ($category->children as $child) {
                    $popularListings = $popularListings->merge($child->listings);
                }
            }
        }
        return $popularListings->sortByDesc('clicks')->take(5);
    }

}