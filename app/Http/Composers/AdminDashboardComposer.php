<?php namespace App\Http\Composers;

use App\Repositories\OrderRepository;
use App\Repositories\PostRepository;
use App\Repositories\PostCategoryRepository;
use App\Repositories\ScriptRepository;
use App\Repositories\UserRepository;
use App\Repositories\ListingRepository;
use Route;
use Input;
use Config;
use App\Script;

class AdminDashboardComposer {

    /**
     * Create a new Order instance.
     */
    public function __construct(
        OrderRepository $order,
        PostRepository $post,
        PostCategoryRepository $postCategory,
        ScriptRepository $script,
        UserRepository $user,
        ListingRepository $listing
    )
    {
        $this->order = $order;
        $this->post = $post;
        $this->postCategory = $postCategory;
        $this->script = $script;
        $this->user = $user;
        $this->listing = $listing;
    }
    
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $scripts = $this->script->getAllWithOrders();
        $topScripts = $scripts->sortByDesc(function ($script, $key) {
            return $script->totalSales();
        });
        $users = $this->user->getAllScriptsAndOrders();
        $topAuthors = $users->sortByDesc(function ($user, $key) {
            return $user->totalSales();
        });
        $postCategories = $this->postCategory->getAllWithPosts();
        $popularCategories = $postCategories->sortByDesc(function ($postCategory, $key) {
            return $postCategory->postCount();
        });
        $tutorialCategories = $popularCategories->where('post_type', 'tutorial');
        $articleCategories = $popularCategories->where('post_type', 'article');
        $orders = $this->order->getAllWithItems();
        $order_data = $this->getOrderData($orders);
        $total_articles = $this->post->countByType('article', 'approved');
        $pending_articles = $this->post->countByType('article', 'pending');
        $total_tutorials = $this->post->countByType('tutorial', 'approved');
        $pending_tutorials = $this->post->countByType('tutorial', 'pending');
        $total_scripts = $this->script->count('approved');
        $pending_scripts = $this->script->count('pending');
        $total_users = $this->user->count();
        $pending_listings = $this->listing->count('pending');


        $view->with(array(
            'topScripts' => $topScripts,
            'topAuthors' => $topAuthors,
            'tutorialCategories' => $tutorialCategories,
            'articleCategories' => $articleCategories,
            'orders' => $orders,
            'order_data' => $order_data,
            'total_articles' => $total_articles,
            'total_tutorials' => $total_tutorials,
            'total_scripts' => $total_scripts,
            'total_users' => $total_users,
            'pending_articles' => $pending_articles,
            'pending_tutorials' => $pending_tutorials,
            'pending_scripts' => $pending_scripts,
            'pending_listings' => $pending_listings
        ));
    }

    /**
     * Get order data
     * @param  Order $orders
     * @return array
     */
    public function getOrderData($orders)
    {
        $ordersByDate = $orders->groupBy(function($item) {
            return $item->created_at->format('Y-m');
        });
        $order_data = array();
        $total_amount = 0;
        $script_total = 0;
        $listing_total = 0;
        $banner_total = 0;
        foreach ($ordersByDate as $orders) {
            $time = strtotime( date('Y', strtotime($orders->first()->created_at)) . '-' . date('m', strtotime($orders->first()->created_at)) . '-' . '01') * 1000;
            $order_data['scripts'][$time] = '[' . $time;
            $order_data['listings'][$time] = '[' . $time;
            $order_data['banners'][$time] = '[' . $time;
            $script_amount = 0;
            $listing_amount = 0;
            $banner_amount = 0;
            foreach ($orders as $order) {
                foreach ($order->items as $item) {
                    switch ($item->type) {
                        case 'script':
                            $script_amount += $item->site_revenue;
                            $script_total += $item->site_revenue;
                            $total_amount += $item->site_revenue;
                            break;
                        case 'listing':
                            $listing_amount += $item->site_revenue;
                            $listing_total += $item->site_revenue;
                            $total_amount += $item->site_revenue;
                            break;
                        case 'banner':
                            $banner_amount += $item->site_revenue;
                            $banner_total += $item->site_revenue;
                            $total_amount += $item->site_revenue;
                            break;
                    }
                }
            }
            $order_data['scripts'][$time] .= ',' . $script_amount;
            $order_data['scripts'][$time] .= ']';
            $order_data['listings'][$time] .= ',' . $listing_amount;
            $order_data['listings'][$time] .= ']';
            $order_data['banners'][$time] .= ',' . $banner_amount;
            $order_data['banners'][$time] .= ']';
        }
        $order_data['total_amount'] = $total_amount;
        $order_data['script_total'] = $script_total;
        $order_data['listing_total'] = $listing_total;
        $order_data['banner_total'] = $banner_total;
        return $order_data;
    }

}