<?php namespace App\Http\Composers;

use App\Repositories\BannerRepository;

class BannersComposer {

    /**
     * Create a new BannerRepository instance.
     */
    public function __construct(BannerRepository $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
    }
    
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view_name = $view->getName();
        $location = substr($view_name, ($pos = strpos($view_name, '.')) !== false ? $pos + 1 : 0);

        if (!isset($this->activeBanners)) {
            $this->activeBanners = $this->bannerRepository->findActive();
        }

        $banner = $this->getBannerByLocation($location);

        $view->with(array(
            'banner' => $banner->first(),
        ));
    }

    /**
     * Get banner by location
     * 
     * @param  str $location
     * @return Banner
     */
    public function getBannerByLocation($location)
    {
        $banner = $this->activeBanners->where('location', $location)->take(1);
        if (!$banner){
            return 'No banner for this location.';
        }
        else {
            return $banner;
        }
    }

}