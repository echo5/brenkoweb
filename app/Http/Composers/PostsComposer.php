<?php namespace App\Http\Composers;

use App\PostCategory;
use App\Post;
use App\Repositories\PostRepository;
use App\Repositories\PostCategoryRepository;
use App\Services\Shortcodes;
use Shortcode;
use Route;
use App;
use Debugbar;

class PostsComposer {


    /**
     * Create a new PostCategory instance.
     */
	public function __construct(PostRepository $postRepository, PostCategoryRepository $categoryRepository, Shortcodes $shortcodes)
    {
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
        $this->shortcodes = $shortcodes;

        $post_type = $this->getPostType();
        $this->title = ucfirst($post_type);
        $this->template_view = 'posts.index';
        $this->posts = null;
        $this->post = null;
        $this->next = null;
        $this->previous = null;
        $this->comments = null;
        $this->post_type = $postRepository->getTypeFromUrl($post_type);
        $this->categories = $categoryRepository->allWithChildrenParents();

        $this->setCurrentCategory();
        $this->setCurrentPosts();
        $this->setCategories();
    }

    /**
     * Determine and set current category
     */
    public function setCurrentCategory()
    {
        $hierarchy = Route::getCurrentRoute()->getParameter('hierarchy');
        $this->url_categories = explode('/', $hierarchy);
        $this->end_slug = end($this->url_categories);
        $this->currentCategory = $this->categories->where('slug', $this->end_slug)->first();
    }

    /**
     * Set categories for display
     */
    public function setCategories()
    {
        $this->articleCategories = $this->categories->where('post_type', 'article')->where('parent_id', null);
        $this->tutorialCategories = $this->categories->where('post_type', 'tutorial')->where('parent_id', null);

        if ($this->currentCategory) 
            $this->categories = $this->categories->where('parent_id', $this->currentCategory->id)->where('post_type', $this->post_type);
        else
            $this->categories = $this->categories->where('parent_id', null)->where('post_type', $this->post_type);  
    }

    /**
     * Set post or posts for display
     */
    public function setCurrentPosts()
    {
        if ($this->currentCategory) {
            if ($this->categoryRepository->validateAncestors($this->currentCategory, $this->url_categories)) {
                $this->posts = $this->postRepository->findByCategoryId($this->currentCategory->id, $this->post_type);
            }
            else {
                App::abort('404');
            }
        }
        else {
            if (end($this->url_categories) == '') {
                $orderBy = ($this->post_type == 'tutorial') ? 'ASC' : 'DESC';
                $this->posts = $this->postRepository->allByType($this->post_type, $orderBy);
            }
            elseif (!$this->setSinglePost()) {
                if ($this->currentCategory) {
                    $this->posts = $this->postRepository->allByType($this->post_type);
                }
                else {
                    App::abort('404');
                }
            }
        }
    }

    /**
     * Set a single post if slug matches
     *
     * @return  boolean
     */
    public function setSinglePost()
    {
        $this->post = $this->postRepository->findBySlug($this->end_slug, $this->post_type);

        if ($this->post) {
            $categories = $this->url_categories;
            array_pop($categories);
            $main_category = $this->categories->where('slug', end($categories))->first();
            $this->next = $this->post->next();
            $this->previous = $this->post->previous();
            $this->setTemplate();
            $this->comments = $this->post->comments->where('parent_id', null);
            // Remove final URL category
            array_pop($this->url_categories);

            if ($this->categoryRepository->validateAncestors($main_category, $this->url_categories)) {
                $this->currentCategory = $main_category;
                return true;
            }
        }

        return false;
    }

    /**
     * Get post type from action
     *    
     * @return str
     */
    public function getPostType()
    {
        $action = Route::getCurrentRoute()->getAction();
        return (array_key_exists('type', $action) ? $action['type'] : 'page');
    }

    /**
     * Set template string if single post
     */
    public function setTemplate()
    {
        if ($this->post)
            $this->template_view = $this->post->getTemplate();
    }

    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        if ($this->post) {
            $this->post['content'] = $this->filterTags($this->post->content);
            $this->post['content'] = Shortcode::compile($this->post['content']);
        }

        $view->with(array(
            'currentCategory' => $this->currentCategory,
            'posts' => $this->posts,
            'post' => $this->post,
            'categories' => $this->categories,
            'articleCategories' => $this->articleCategories,
            'tutorialCategories' => $this->tutorialCategories,
            'title' => $this->title,
            'next' => $this->next,
            'previous' => $this->previous,
            'template_view' => $this->template_view,
            'comments' => $this->comments,
            'post_type' => $this->post_type
        ));
    }

    /**
     * Filter <pre> tags in HTML
     * @param  str $content
     * @return str
     */
    public function filterTags($content)
    {
        $dom = new \DOMDocument;
        @$dom->loadHTML($content);
		/*@$dom->loadHTML($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);*/
        $preTags = $dom->getElementsByTagName('pre');
        while($preTags->length && $preTags->item(0)->hasAttributes())
        {
            $pre = $preTags->item(0);
            if ($pre->hasAttributes())
            {
                $height = $pre->getAttribute('data-height');
                $width = $pre->getAttribute('data-width');
                $parser_type = $pre->getAttribute('data-parser-type') ? $pre->getAttribute('data-parser-type') : 'codeparser';

                $codeparser_before = new \DOMText('['. $parser_type .' decode=true height='.$height.' width='.$width.']');
                $codeparser_after = new \DOMText('[/'. $parser_type .']');

                $fragment = $dom->createDocumentFragment();
                $fragment->appendChild($codeparser_before);
                while ($pre->childNodes->length > 0) {
                    $fragment->appendChild($pre->childNodes->item(0));
                }
                $fragment->appendChild($codeparser_after);
                $pre->parentNode->replaceChild($fragment, $pre);

            }
        }
	
		$content = preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array('<html>', '</html>', '<body>', '</body>'), array('', '', '', ''), $dom->saveHTML()));	
        /*$content = $dom->saveHTML();*/
        return $content;
    }
}