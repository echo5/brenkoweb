<?php namespace App\Http\Composers;

use App\Repositories\ActivityRepository;
use Route;
use Auth;

class AdminActivityLogComposer {


    /**
     * Create a new ActivityRepository instance.
     */
    public function __construct(ActivityRepository $activity)
    {
        $this->activity = $activity->getLatest();
        $this->readActivities = $this->getReadActivities();
        $this->messages = $this->getMessages();
        $this->pendingItems = $this->getPendingItems();
        $this->notifications = $this->getNotifications();
        $this->unreadMessages = $this->countUnread($this->messages, $this->readActivities);
        $this->unreadPendingItems = $this->countUnread($this->pendingItems, $this->readActivities);
        $this->unreadNotifications = $this->countUnread($this->notifications, $this->readActivities);
    }
    
    /**
     * Get pending items that need approval
     * 
     * @return Activity
     */
    public function getPendingItems()
    {
        $newArticles = $this->activity
            ->where('action', 'Create')
            ->where('content_type', 'Article');
        $newBanners = $this->activity
            ->where('action', 'Create')
            ->where('content_type', 'Banner');
        $newListings = $this->activity
            ->where('action', 'Create')
            ->where('content_type', 'Listing');
        $tutorialSuggestions = $this->activity
            ->where('action', 'Create')
            ->where('content_type', 'Tutorial');

        $pendingItems = $newArticles;
        $pendingItems = $pendingItems->merge($newBanners);
        $pendingItems = $pendingItems->merge($newListings);
        $pendingItems = $pendingItems->merge($tutorialSuggestions);
        $pendingItems = $pendingItems->sortByDesc('created_at');

        return $pendingItems;
    }

    /**
     * Get array of read activities for current user
     * 
     * @return array
     */
    public function getReadActivities()
    {
        $readActivities = Auth::user()->readActivities()->select('activity_log.id')->lists('id');
        foreach ($readActivities as $activity)
            $activity_ids[] = $activity;

        if (isset($activity_ids))
            return $activity_ids;

        return array();
    }

    /**
     * Get messages
     * 
     * @return Activity
     */
    public function getMessages()
    {
        $messages = $this->activity->where('content_type', 'Message');
        return $messages;
    }

    /**
     * Get notification items (purchases and new users)
     * 
     * @return Activity
     */
    public function getNotifications()
    {
        $purchases = $this->activity
            ->where('action', 'Purchase');
        $users = $this->activity
            ->where('action', 'Create')
            ->where('content_type', 'User');

        $otherItems = $purchases;
        $otherItems = $otherItems->merge($users);
        $otherItems = $otherItems->sortByDesc('created_at');

        return $otherItems;
    }

    /**
     * Return a number for unread activities
     * 
     * @param  Activity $activities
     * @param  array    $readActivities
     * @return int
     */
    public function countUnread($activities, array $readActivities)
    {
        $count = 0;
        foreach ($activities as $activity) {
            if (!in_array($activity->id, $readActivities))
                $count++;
        }
        return $count;
    }

    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'activityMessages' => $this->messages,
            'activityPendingItems' => $this->pendingItems,
            'activityNotifications' => $this->notifications,
            'unreadMessages' => $this->unreadMessages,
            'unreadPendingItems' => $this->unreadPendingItems,
            'unreadNotifications' => $this->unreadNotifications,
            'readActivities' => $this->readActivities,
        ));
    }

}