<?php namespace App\Http\Composers;

use App\ScriptCategory;
use App\Repositories\ScriptRepository;
use Route;

class ScriptsComposer {

    /**
     * Category instances.
     */
    protected $categories;

    /**
     * Create a new PostCategory instance.
     */
    public function __construct(ScriptCategory $scriptCategory, ScriptRepository $script)
    {
        $this->categories = $scriptCategory
            ->with('children')
            ->with('parent')
            ->with('scripts')
            ->get();
        $this->script = $script;

        $this->scriptCategories = $this->categories->where('parent_id', null)->sortBy('name');
        $this->setCurrentCategory();
        $this->setCategories();
        
        $this->currentScripts = $this->getCurrentScripts();
        $this->popularScripts = $script->getPopular();
        $this->topRatedScripts = $script->getTopRated();
    }
    
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'scriptCategories' => $this->scriptCategories,
            'currentCategories' => $this->currentCategories,
            'currentScripts' => $this->currentScripts,
            'topRatedScripts' => $this->topRatedScripts,
            'popularScripts' => $this->popularScripts,
            'currentCategory' => $this->currentCategory,
        ));
    }

    /**
     * Determine and set current category
     */
    public function setCurrentCategory()
    {
        $hierarchy = Route::getCurrentRoute()->getParameter('hierarchy');
        $url_categories = explode('/', $hierarchy);
        $category_slug = end($url_categories);
        $this->currentCategory = $this->categories->where('slug', $category_slug)->first();
    }

    /**
     * Set categories for display
     */
    public function setCategories()
    {
        if ($this->currentCategory) 
            $this->currentCategories = $this->categories->where('parent_id', $this->currentCategory->id)->sortBy('created_at');
		else
            $this->currentCategories = $this->categories = $this->categories->where('parent_id', null)->sortBy('created_at');  
    }

    /**
     * Set listings for display
     */
    public function getCurrentScripts()
    {
        if ($this->currentCategory)
            return $this->script->getActiveByCategoryId($this->currentCategory->id);
        return $this->script->getAllActive();
    }

}