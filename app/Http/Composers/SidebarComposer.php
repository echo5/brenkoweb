<?php namespace App\Http\Composers;

use App\PostCategory;

class CategoriesComposer {

    /**
     * Category instances.
     */
    protected $articleCategories;
    protected $tutorialCategories;

    /**
     * Create a new PostCategory instance.
     */
	public function __construct(PostCategory $postCategory)
    {
        // $this->currentCategory = Route::getCurrentRoute()->getParameter('category');


        $categories = $postCategory->where('parent_id', NULL)->with('children')->get();

        $this->articleCategories = $categories->where('post_type', 'article');
        $this->tutorialCategories = $categories->where('post_type', 'tutorial');

    }
	
    /**
     * Compose view
     * 
     * @param  $view
     */
    public function compose($view)
    {
        $view->with(array(
            'articleCategories' => $this->articleCategories,
            'tutorialCategories' => $this->tutorialCategories,
        ));
    }

}