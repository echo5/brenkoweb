<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use App\Post;

class StoreArticleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|unique:posts',
            'content' => 'required|min:250',
            'meta_description' => 'required|max:160|min:10',
            'category_id' => 'required|integer',
            'image' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg'
        ];
    }

    /**
     * Format input (doesn't work directly in L5)
     * 
     * @return array
     */
    public function formatInput()
    {
        $input = array_map('trim', $this->all());
        $input['slug'] = str_slug($input['title'], '-');
        $this->replace($input);
        return $this->all();
    }

    /**
     * Override validator
     * 
     * @return Validator
     */
    public function getValidatorInstance() {
        $validator = parent::getValidatorInstance();

        $validator->after(function() use ($validator) {
            $input = $this->formatInput();
            $input['slug'] = str_slug($input['title'], '-');

            $post = Post::where('slug', $input['slug'])->get();
            if(count($post)) {
                $validator->errors()->add('title', 'This title\'s slug has already been taken, please try another.');
            }

        });

        return $validator;
    }
}
