<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use App\Listing;

class StoreListingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|unique:listings',
            'description' => 'required|min:25',
            'link' => 'required|active_url|unique:listings',
            'image' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            'category_id' => 'required|integer',
            'plan_id' => 'required|integer',
        ];
    }

}
