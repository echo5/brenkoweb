<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class UpdateReadActivitiesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->level() >= 100;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'activities' => 'required|array',
        ];
    }
}
