<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use App\Post;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'email' => 'required|email|unique:users,email,' . Auth::user()->id,
            'website' => 'url|max:255',
            'paypal_email' => 'email',
            'password' => 'confirmed',
            'avatar_file' => 'image|mimes:jpeg,jpg,png,bmp,gif,svg'
        ];
    }

    /**
     * Format input (doesn't work directly in L5)
     * 
     * @return array
     */
    public function formatInput()
    {
        $input = array_map('trim', $this->all());
        if ($input['password'] == ''){
            unset($input['password']);
            unset($input['password_confirmation']);
        }
        $this->replace($input);
        return $this->all();
    }

    /**
     * Override validator
     * 
     * @return Validator
     */
    public function getValidatorInstance() {
        $validator = parent::getValidatorInstance();

        $validator->after(function() use ($validator) {
            $input = $this->formatInput();
        });

        return $validator;
    }
}
