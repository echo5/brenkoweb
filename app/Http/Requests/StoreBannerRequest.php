<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class StoreBannerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'caption' => 'min:5',
            'location' => 'required',
            'link' => 'required|active_url',
            'image' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            'plan_id' => 'required|integer',
        ];
    }

}
