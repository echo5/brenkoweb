<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class StoreScriptRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|unique:scripts',
            'version' => 'required',
            'image' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            'file' => 'required|mimes:zip',
            'preview_link' => 'active_url',
            'description' => 'required|min:10',
            'features' => 'required|min:10',
            'requirements' => 'required|min:10',
            'category_id' => 'required|integer',
            'price' => 'required|numeric|max:200',
        ];
    }

}
