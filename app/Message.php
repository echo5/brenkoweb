<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The fields that are fillable
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'email',
        'message',
    );


}
