<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\PostCategory;
use Illuminate\Container\Container as App;

class PostCategoryRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\PostCategory';
    }

    /**
     * All categories with children and parents
     * 
     * @return PostCategory
     */
    public function allWithChildrenParents()
    {
        return $this->model
            ->with(array(
                'children' => function ($query) {
                    $query->orderBy('created_at', 'asc');
                },
                'parent' => function ($query) {
                    $query->orderBy('created_at', 'asc');
                },
            ))
            ->orderBy('created_at', 'asc')
            ->get();
    }

    /**
     * Validate category ancestors
     *
     * @return bool
     */
    public function validateAncestors($main_category, $categories)
    {
        if ($main_category) {
            array_pop($categories);
            $ancestors = $main_category->getAncestors();

            if (count($ancestors) && empty($categories))
                return false;

            $valid = true;
            $n = 0;
            foreach ($categories as $category)
            {
                if (!isset($ancestors[$n])) {
                    $valid = false;
                    break;
                } 
                else {
                    if ($category !== $ancestors[$n]->slug)
                    {
                        $valid = false;
                        break;
                    }
                    $n++;
                }
            }
            return $valid;
        }
        return false;
    }

    /**
     * Get all with posts
     * @return mixed
     */
    public function getAllWithPosts($type = null)
    {
        if ($type)
            return PostCategory::where('post_type', $type)->with('posts')->get();
        
        return PostCategory::with('posts')->get();
    }
}