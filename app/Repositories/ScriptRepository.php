<?php

namespace App\Repositories;

use App\Repositories\Repository;
use Illuminate\Container\Container as App;
use App\Script;
use DB;

class ScriptRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Script';
    }

    /**
     * Find approved and active by ID
     * 
     * @param  $id
     * @return Script
     */
    public function findActiveById($id)
    {
        return $this->model->where('id', $id)
            ->where('status', 'approved')
            ->with('reviews')
            ->with('reviews.user')
            ->with('user')
            ->first();
    }

    /**
     * Find related scripts by category
     * 
     * @param  Script  $script
     * @param  integer $limit 
     * @return Script
     */
    public function findRelated(Script $script, $limit = 10)
    {
        return Script::where('category_id', $script->category_id)
            ->where('id', '!=', $script->id)
            ->where('status', 'approved')
            ->take($limit)
            ->get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return Script::create($data);
    }

    /**
     * Get all active scripts
     * 
     * @return mixed
     */
    public function getActive($limit = 2147483647, $offset = 0, $order_by = 'created_at')
    {
        return $this->model->where('status', 'approved')
            ->skip($offset)
            ->with('category')
            ->with('category.parent')
            ->with('user')
            ->with('reviews')
            ->orderByRaw($order_by . ' desc')
            ->limit($limit)
            ->get();
    }

    /**
     * Get active by category and paginate
     * 
     * @param  integer $category_id
     * @return mixed
     */
    public function getActiveByCategoryId($category_id)
    {
        return $this->model->where('status', 'approved')
            ->where('category_id', $category_id)
            ->with('category')
            ->with('category.parent')
            ->with('user')
            ->with('reviews')
            ->paginate(12);
    }

    /**
     * Get all active and paginate
     * 
     * @return mixed
     */
    public function getAllActive()
    {
        return $this->model->where('status', 'approved')
            ->with('category')
            ->with('category.parent')
            ->with('user')
            ->with('reviews')
            ->paginate(12);
    }

    /**
     * Get popular scripts by num of views
     * 
     * @param  integer $limit
     * @return mixed
     */
    public function getPopular($limit = 5)
    {
        return Script::where('status', 'approved')
            ->with('category')
            ->with('category.parent')
            ->orderBy('views', 'desc')
            ->limit($limit)
            ->get();
    }

    /**
     * Get top rated scripts with more than 4 reviews
     * 
     * @param  integer $limit
     * @return mixed
     */
    public function getTopRated($limit = 5)
    {
        return Script::where('status', 'approved')
            ->join('script_reviews', 'scripts.id', '=', 'script_reviews.script_id')
            ->select(DB::raw('scripts.id, scripts.name, scripts.image, scripts.preview_link, scripts.status, count(script_reviews.script_id) as `reviews_count`, AVG(script_reviews.rating) as script_rating'))
            ->havingRaw('count(script_reviews.script_id) >= 3')
            ->groupBy('scripts.id')
            ->orderBy('script_rating', 'desc')
            ->limit($limit)
            ->get();
    }

    /**
     * Count total
     * @return int
     */
    public function count($status = null)
    {
        if ($status)
            return Script::where('status', $status)->count();

        return Script::count();
    }

    /**
     * Get all scripts with orders
     * @return mixed
     */
    public function getAllWithOrders()
    {
        return Script::with('orders')->with('category')->with('user')->get();
    }

}