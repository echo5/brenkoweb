<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\EmailTemplate;

class EmailTemplateRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\EmailTemplate';
    }

}