<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Post;
use Illuminate\Container\Container as App;

class PostRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Post';
    }

    /**
     * Get all posts based on post type
     * 
     * @return Post
     */
    public function allByType($type, $orderBy = 'DESC')
    {
        return $this->model->where('type', $type)
            ->with('category')
            ->where('status', 'approved')
            ->with('category.parent')
            ->orderBy('created_at', $orderBy)
            ->paginate(10);
    }

    /**
     * Find by slug and type
     * @param  $slug
     * @param  $type
     * @return Post
     */
    public function findBySlug($slug, $type)
    {
        return $this->model->where('slug', $slug)
            ->where('type', $type)
            ->where('status', 'approved')
            ->with('template')
            ->with('comments')
            ->with('comments.children')
            ->with('comments.user')
            ->first();
    }

    /**
     * Find by category ID and type
     * @param  $slug
     * @param  $type
     * @return Post
     */
    public function findByCategoryId($category_id, $type)
    {
        return $this->model->where('category_id', $category_id)
            ->where('type', $type)
            ->where('status', 'approved')
            ->with('category')
            ->with('category.parent')
            ->orderBy('created_at', 'ASC')
            ->paginate(10);
    }

    /**
     * Get the post type string for use in queries
     * 
     * @return str
     */
    public function getTypeFromUrl($type = null)
    {
        $types = Post::getTypes();
        return array_search($type, $types);
    }

    /**
     * Get feed for post grid
     * @return Post
     */
    public function getFeed()
    {
        return $this->model->with('user')
            ->where('status', 'approved')
            ->where(function ($query) {
                $query->where('type', 'tutorial')
                    ->orWhere('type', 'article');
            })
            ->with('category')
            ->with('category.parent')
            ->orderBy('created_at', 'DESC')
            ->limit(8)
            ->get();
    }

    /**
     * Get posts by post type
     * 
     * @param  $type
     * @param  int $limit 
     * @return Post
     */
    public function getActiveByType($type, $limit, $offset = 0, $order_by = 'created_at')
    {
        return $this->model->with('user')
            ->skip($offset)
            ->where('status', 'approved')
            ->where('type', $type)
            ->with('category')
            ->with('category.parent')
            ->orderByRaw($order_by . ' DESC')
            ->limit($limit)
            ->get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        return Post::create($data);
    }

    /**
     * Count total by type
     * @param  str $type
     * @return integer
     */
    public function countByType($type, $status = null)
    {
        if ($status)
            return Post::where('status', $status)->where('type', $type)->count();

        return Post::where('type', $type)->count();
    }


}