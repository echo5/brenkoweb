<?php

namespace App\Repositories;

use App\Repositories\Repository;
use Illuminate\Container\Container as App;

class BannerPlanRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\BannerPlan';
    }

}