<?php

namespace App\Repositories;

use App\Repositories\Repository;
use Illuminate\Container\Container as App;
use App\Banner;
use App\BannerStat;
use DB;
use Config;
use Event;
use App\Events\LowBannerCredit;

class BannerRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Banner';
    }

    /**
     * Get banner locations
     * 
     * @return array
     */
    public static function getLocations()
    {
        return array(
            'content' => 'Content',
            'bottom' => 'Bottom',
            'left-sidebar' => 'Left Sidebar',
        );
    }


    /**
     * Get approved banners with remaining impressions/days
     * 
     * @return Banner
     */
    public function findActive()
    {
        // Commented out is an optimized select random and limit by row option
        // DB::statement(DB::raw('set @num := 0, @location := "";'));
        // $banners = DB::select(DB::raw(
        //     'SELECT banners.id, location, caption, script, SUM(banner_stats.impressions) AS impressions, max_impressions
        //         -- @num := if(@location = location, @num + 1, 1) as row_number,
        //         -- @location := location as dummy
        //     FROM banners force index(location)
        //     JOIN banner_stats
        //     ON banners.id=banner_stats.banner_id
        //     WHERE banners.approved=true
        //     GROUP BY location, banners.id
        //     HAVING (impressions < max_impressions)
        //     -- HAVING row_number <= 1
        //     -- ORDER BY RAND()'
        // ));

        $banners = $this->model->where('status', 'approved')
            ->with('stats')
            ->orderByRaw("RAND()")
            ->select('id', 'location', 'script', 'image', 'max_impressions', 'end_date', 'caption', 'user_id', 'alert_status')->get();

        $activeBanners = $banners->filter(function ($banner) {
            return $banner->hasCredit();
        });

        return $activeBanners;

    }

    /**
     * Add impression to banners
     * 
     * @param array $banner_ids
     */
    public function addImpression($banner_ids)
    {
        $update_rows = array();
        $insert_data = array();

        foreach($banner_ids as $banner_id) {
            $banner = Banner::with('stats')->find(intval($banner_id));
            if ($banner) {
                $currentMonthStat = $banner->stats->filter(function ($stat)
                {
                    return $stat->isCurrentMonth();
                })->first();
                if ($currentMonthStat) {
                    $update_rows[] = $currentMonthStat->id;
                }
                else {
                    $insert_data[] = array (
                        'banner_id' => $banner->id,
                        'date' => DB::raw('CURDATE()'),
                        'impressions' => 1
                    );
                }
                if ($banner->lowCredit() && $banner->alert_status == '') {
                    $response = Event::fire(new LowBannerCredit($banner));
                }
            }
        }

        if (!empty($update_rows)) {
            DB::table('banner_stats')
                ->whereIn('id', $update_rows)
                ->update(array (
                    'impressions' => DB::raw('impressions + 1')
                ));
        }

        if (!empty($insert_data)) {
            BannerStat::insert($insert_data);
        }
    }

    /**
     * Add to max impressions
     * @param int $num Number of impressions to add
     * @param $id  Banner ID
     */
    public function addToMaxImpressions($num, $id, $attribute="id")
    {
        return $this->model->where($attribute, '=', $id)->first()->update(['max_impressions' => DB::raw('max_impressions + ' . $num)]);
    }

    /**
     * Add to end date
     * @param int $num Number of days to add
     * @param $id  Banner ID
     */
    public function addToEndDate($num, $id, $attribute="id")
    {
        $banner = $this->model->where($attribute, '=', $id)->first();
        if ($banner->end_date == '0000-00-00' || $banner->daysRemaining() < 1)
            return $banner->update(['end_date' => DB::raw('DATE_ADD(CURDATE(), INTERVAL ' . $num . ' DAY)')]);
        else
            return $banner->update(['end_date' => DB::raw('DATE_ADD(end_date, INTERVAL ' . $num . ' DAY)')]); 
    }

    /**
     * Find by ID with stats
     * @param  $id
     * @return Banner
     */
    public function findWithStats($id)
    {
        $this->model->find($id)
            ->with('stats')
            ->with('user')
            ->with('plan')
            ->first();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        return Banner::create($data);
    }

}