<?php

namespace App\Repositories;

use App\Repositories\Repository;
use Illuminate\Container\Container as App;

class ListingPlanRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\ListingPlan';
    }

}