<?php

namespace App\Repositories;

use App\Repositories\Repository;
use Illuminate\Container\Container as App;

class ActivityRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Activity';
    }

    /**
     * Get latest activities for feed
     * @return Post
     */
    public function getLatest($n = 50)
    {
        return $this->model
            ->orderBy('created_at', 'DESC')
            ->limit($n)
            ->get();
    }


}