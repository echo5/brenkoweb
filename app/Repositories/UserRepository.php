<?php

namespace App\Repositories;

use App\User;

class UserRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\User';
    }

    /**
     * Find a social user by ID or create new
     * 
     * @param  $userData
     * @param  $provider Socialite provider
     * @return User $user
     */
    public function findSocialUserOrCreate($userData, $provider)
    {
        $user = $this->model->where('provider_id', '=', $userData->id)->where('provider', '=', $provider)->first();
        if(!$user) {
            $user = $this->create([
                'provider' => $provider,
                'provider_id' => $userData->id,
                'name' => $userData->name,
                'username' => $userData->nickname,
                'email' => $userData->email,
                'status' => 'approved'
            ]);
        }

        $this->checkSocialUpdates($userData, $user);
        return $user;
    }

    /**
     * Check array for differences in social info
     * 
     * @param  $userData
     * @param  User $user
     * @return null
     */
    public function checkSocialUpdates($userData, $user)
    {

        $socialData = [
            'email' => $userData->email,
            'name' => $userData->name,
            'username' => $userData->nickname,
        ];
        $dbData = [
            'email' => $user->email,
            'name' => $user->name,
            'username' => $user->username,
        ];

        if (!empty(array_diff($socialData, $dbData))) {
            $user->email = $userData->email;
            $user->name = $userData->name;
            $user->username = $userData->nickname;
            $user->save();
        }
    }

    /**
     * Find profile items
     * 
     * @param  User $id
     * @return User
     */
    public function findProfile($id)
    {
        return $this->model->where('id', $id)
            ->with('posts')
            ->with('posts.category')
            ->with('posts.category.parent')
            ->with('scripts')
            ->with('listings')
            ->with('comments')
            ->with('comments.post')
            ->with('scriptReviews')
            ->with('banners')
            ->with('banners.stats')
            ->with('scriptReviews')
            ->with('scriptReviews.user')
            ->with('scriptReviews.script')
            ->where('status', 'approved')
            ->first();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return User::create($data);
    }

    public function getAllScriptsAndOrders()
    {
        return User::with('scripts')->with('scripts.orders')->get();
    }
}