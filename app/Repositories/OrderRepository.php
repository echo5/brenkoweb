<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Order;
use Illuminate\Container\Container as App;

class OrderRepository extends Repository {

    protected $items;

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Order';
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) 
    {
        return Order::create($data);
    }

    /**
     * Get latest orders with items
     * @param  integer $limit
     * @return mixed
     */
    public function getLatest($limit = 10) 
    {
        return Order::with('items')->orderBy('created_at', 'desc')->limit($limit)->get();
    }

    /**
     * Get all orders with items
     * @return mixed
     */
    public function getAllWithItems()
    {
        return Order::with('items')->orderBy('created_at', 'desc')->get();
    }

}