<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\OrderItem;
use Illuminate\Container\Container as App;

class OrderItemRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\OrderItem';
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        return OrderItem::create($data);
    }

}