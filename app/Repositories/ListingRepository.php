<?php

namespace App\Repositories;

use App\Repositories\Repository;
use Illuminate\Container\Container as App;
use App\Listing;

class ListingRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Listing';
    }

    /**
     *  Create new Listing
     * 
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return Listing::create($data);
    }

    /**
     * Get all active listings
     * 
     * @return mixed
     */
    public function getActive()
    {
        return $this->model->where('status', 'approved')
            ->with('category')
            ->with('category.parent')
            ->with('user')
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * Get all active and paginate
     * @param  integer $limit
     * @return mixed
     */
    public function getAllActive($limit = 12)
    {
        return $this->model->where('status', 'approved')
            ->with('category')
            ->with('category.parent')
            ->with('user')
            ->orderBy('created_at', 'desc')
            ->paginate($limit);
    }

    /**
     * Get active by category id and paginate
     * @param  integer $category_id
     * @return mixed
     */
    public function getActiveByCategoryId($category_id, $limit = 12)
    {
        return $this->model->where('status', 'approved')
            ->where('category_id', $category_id)
            ->with('category')
            ->with('category.parent')
            ->with('user')
            ->orderBy('created_at', 'desc')
            ->paginate($limit);
    }

    /**
     * Get random listings
     * @param  integer $limit
     * @return mixed
     */
    public function getRandom($limit = 1)
    {
        return Listing::where('status', 'approved')
            ->with('category')
            ->with('category.parent')
            ->limit($limit)
            ->orderByRaw("RAND()")
            ->get();
    }

    /**
     * Find active listing by ID
     * 
     * @return Listing
     */
    public function findActiveById($id)
    {
        return $this->model->where('id', $id)
            ->where('status', 'approved')
            ->first();
    }

    /**
     * Get sponsored listings
     * 
     * @return mixed
     */
    public function getSponsored($limit = 1)
    {
        return Listing::where('status', 'approved')
            ->where('sponsored', '1')
            ->with('category')
            ->with('category.parent')
            ->with('user')
            ->limit($limit)
            ->orderByRaw("RAND()")
            ->get();
    }

    /**
     * Count total
     * @return int
     */
    public function count($status = null) {
        if ($status)
            return Listing::where('status', $status)->count();
        
        return Listing::count();
    }
}