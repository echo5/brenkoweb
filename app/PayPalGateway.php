<?php

namespace App;

use Config;
use URL;
use Session;
use Redirect;
use Input;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Repositories\OrderRepository;
use angelleye\PayPal\Adaptive;

class PayPalGateway
{

    /**
     * API credentials
     * 
     * @var ApiContext
     */
    private $_api_context;
 
    protected $custom;

    /**
     * Setup PayPal api vars
     */
    public function __construct()
    {
        $paypal_conf = Config::get('paypal');
        $paypal_adaptive_conf = Config::get('paypal_adaptive');
        $this->_adaptive_api_context = $paypal_adaptive_conf;
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
        $this->return_url = URL::route('payment.execute');
        $this->adaptive_return_url = URL::route('payment.executeAdaptive');
        $this->cancel_url = URL::route('payment.canceled');
        $this->adaptive = new Adaptive($this->_adaptive_api_context);
    }

    /**
     * Post payment and redirect to PayPal
     * 
     * @return Redirect
     */
    public function postPayment()
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item = $this->items;

        $item_list = new ItemList();
        $item_list->setItems($this->getItemArray($item));
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($this->getSubTotal());

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setCustom($this->getCustom())
            ->setDescription('Purchase from Brenko Web');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl($this->return_url)
            ->setCancelUrl($this->cancel_url);

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            if (\Config::get('app.debug')) {
                dd($ex);
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                exit;
            } else {
                die('There was an error in your transaction.  Please try again or contact us for help.');
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {
            return Redirect::away($redirect_url);
        }

        return Redirect::back()
            ->with('error', 'Unknown error occurred');
    }

    /**
     * Post chained payment
     * 
     * @return Redirect
     */
    public function postChainedPayment()
    {
        $pay_request_fields = [
            'ActionType' => 'PAY',
            'CancelURL' => $this->cancel_url,
            'ReturnURL' => $this->adaptive_return_url,
            'CurrencyCode' => 'USD',
            'FeesPayer' => 'EACHRECEIVER',
            'ReverseAllParallelPaymentsOnError' => '',
        ];

        $client_details_fields = [];

        $funding_types = array('ECHECK', 'BALANCE', 'CREDITCARD');

        $receivers = array();
        for ($n = 0; $n < count($this->receivers); $n++) {
            $receivers[$n]['Email'] = $this->receivers[$n]['email']; 
            $receivers[$n]['Amount'] = $this->receivers[$n]['amount'];
            $receivers[$n]['Primary'] = $this->receivers[$n]['primary'];
            $receivers[$n]['PaymentType'] = '';
            $receivers[$n]['PaymentSubType'] = '';
            $receivers[$n]['Phone'] = '';
            $receivers[$n]['AccountID'] = '';
            $receivers[$n]['InvoiceID'] = '';
            $receivers[$n]['Phone']['CountryCode'] = '';
        }

        $sender_identifier_fields = [];

        $account_identifier_fields = [];

        $request_data = [
            'PayRequestFields' => $pay_request_fields,
            'ClientDetailsFields' => $client_details_fields, 
            //'FundingTypes' => $funding_types, 
            'Receivers' => $receivers, 
            'SenderIdentifierFields' => $sender_identifier_fields, 
            'AccountIdentifierFields' => $account_identifier_fields
        ];

        try {
            $payment = $this->adaptive->Pay($request_data);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            if (\Config::get('app.debug')) {
                dd($ex);
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                exit;
            } else {
                die('There was an error in your transaction.  Please try again or contact us for help.');
            }
        }
        if($payment['RedirectURL']) {
            $redirect_url = $payment['RedirectURL'];
        }

        Session::put('paypal_payment_id', $payment['PayKey']);

        if(isset($redirect_url)) {
            return Redirect::away($redirect_url);
        }

        Session::flash('message', 'An unkown error occurred.' ); 
        Session::flash('alert-class', 'alert-danger'); 
        return Redirect::back();
    }

    /**
     * Set payment receivers for chained payments
     * 
     * @param array $receivers
     */
    public function setReceivers($receivers)
    {
        $this->receivers = $receivers;
    }

    /**
     * Get status for a payment
     *
     * @param  str $payment_id
     * @return Payment
     */
    public function getPayment($payment_id)
    {
        return Payment::get($payment_id, $this->_api_context);
    }

    /**
     * Get payment status for adaptive payment
     * 
     * @param  str $payment_id
     * @return PaymentDetails
     */
    public function getAdaptivePayment($payment_id)
    {
        $payment_fields = [
            'PaymentDetailsFields' => [
                'PayKey' => $payment_id
            ]
        ];
        return $this->adaptive->PaymentDetails($payment_fields);
    }

    /**
     * Parse custom variable string
     * 
     * @param  Payment $payment
     * @return array
     */
    public function parseCustom(Payment $payment)
    {
        $transactions = $payment->getTransactions();
        $custom = $transactions[0]->getCustom();
        parse_str($custom, $parsed);
        return $parsed;
    }

    /**
     * Execute a payment
     *
     * @param  Payment $payment
     * @param  PayerId $payer_id
     * @return array
     */
    public function executePayment(Payment $payment, $payer_id)
    {
        $execution = new PaymentExecution();
        $execution->setPayerId($payer_id);
        $result = $payment->execute($execution, $this->_api_context);
        return $result;
    }

    /**
     * Execute payment for chained adaptive payment
     * 
     * @param  str $pay_key
     * @param  str $funding_plan_id
     * @return array
     */
    public function executeChainedPayment($pay_key, $funding_plan_id = '')
    {
        $paypal_request_data = [
            'ExecutePaymentFields' => [
                'PayKey' => $pay_key,
                'FundingPlanID' => $funding_plan_id
            ]
        ];
        return $this->adaptive->executePayment($paypal_request_data);
    }

    /**
     * Get Custom PayPal var
     * 
     * @return str $custom
     */
    public function getCustom()
    {
        return $this->custom ? $this->custom : null;
    }

    /**
     * Set custom PayPal var
     * 
     * @param str $custom
     */
    public function setCustom($custom)
    {
        $this->custom = $custom;
    }

    /**
     * Set items for payment
     * 
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * Get sub total for items
     * 
     * @return float $sub_total
     */
    public function getSubTotal()
    {
        $sub_total = 0;
        foreach ($this->items as $item) {
            $sub_total += $item['price'];
        }
        return $sub_total;
    }

    /**
     * Convert item array to PayPal object Item array
     * 
     * @param  array $items
     * @return array Item array
     */
    public function getItemArray($items)
    {
        $item_array = array();
        foreach ($items as $item) {
            $paypalItem = new Item();
            $item_array[] = $paypalItem->setName($item['name'])
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($item['price'])
                ->setDescription($item['description'])
                ->setCategory($item['type']);
        }
        return $item_array;
    }


}
