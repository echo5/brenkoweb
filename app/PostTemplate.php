<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTemplate extends Model
{
    /**
     * Timestamps
     * @var boolean
     */
    public $timestamps = false;
    
}
