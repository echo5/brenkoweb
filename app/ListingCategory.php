<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingCategory extends Model
{
    /**
     * Table Name
     * @var string
     */
    protected $table = 'listing_categories';

    /**
     * Validation rules
     */
    public static $rules = array(
        'name' => 'required|unique:listing_categories',
        'slug' => 'required|alpha_dash|unique:listing_categories',
    );

    /**
    * Parent relationship
    */
    public function parent()
    {
        return $this->belongsTo('App\ListingCategory', 'parent_id');
    }

    /**
    * Children relationship
    */
    public function children()
    {
        return $this->hasMany('App\ListingCategory', 'parent_id');
    }

    /**
    * Listing relationship
    */
    public function listings()
    {
        return $this->hasMany('App\Listing', 'category_id');
    }

    /**
     * Get url for listing category
     *
     * @param  $parent_slug
     * @return str
     */
    public function getUrl($parent_slug = null)
    {
        $hierarchy = '';
        if ($parent_slug)
            $hierarchy = $parent_slug . '/';
        elseif ($this->parent)
            $hierarchy = $this->parent->slug . '/';

        $hierarchy .= $this->slug;

        return Route(
            'listing.index',
            [
                'hierarchy' => $hierarchy
            ]
        );
    }

    /**
     * Get total listings of category and all subcategories
     *
     * @return int
     */
    public function totalListings()
    {
        $count = $this->listings->where('status', 'approved')->count();
        if ($this->parent_id == null) {
            if (count($this->children)) {
                foreach ($this->children as $child) {
                    $count += $child->listings->where('status', 'approved')->count();
                }
            }
        }
        return $count;
    }

}
