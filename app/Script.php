<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class Script extends Model
{

    /**
     * The fields that are fillable
     *
     * @var array
     */
    protected $fillable = array(
        'name',
        'version',
        'image',
        'file',
        'preview_link',
        'description',
        'features',
        'requirements',
        'user_id',
        'category_id',
        'price',
        'waive_commission',
    );

    /**
     * Validation rules
     */
    public static $rules = array(
        'status' => 'required',
        'name' => 'required|min:5|unique:scripts',
        'version' => 'required',
        'image' => 'required',
        'file' => 'required',
        'preview_link' => 'active_url',
        'description' => 'required|min:10',
        'features' => 'required|min:10',
        'requirements' => 'required|min:10',
        'category_id' => 'required|integer',
        'price' => 'required|numeric|max:200',
    );

    /**
     * Get status options
     * 
     * @return array
     */
    public static function getStatuses()
    {
        return array(
            'approved',
            'suspended',
            'rejected',
            'pending'
        );
    }

    public function getTotalReviews()
    {
        return $this->hasMany('App\ScriptReview')->count();
    }

    /**
    * Category relationship
    */
    public function category()
    {
        return $this->belongsTo('App\ScriptCategory');
    }

    /**
    * User relationship
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
    * Reviews relationship
    */
    public function reviews()
    {
        return $this->hasMany('App\ScriptReview');
    }

    /**
     * Buyers relationship
     */
    public function buyers()
    {
        return $this->belongsToMany('App\User', 'script_purchases')->withTimestamps();
    }

    /**
     * Orders relationship
     */
    public function orders()
    {
        return $this->hasMany('App\OrderItem', 'model_id')->where('type', '=', 'script');
    }

    /**
     * Get average rating for script
     * 
     * @return float
     */
    public function getAvgRating()
    {
        if (count($this->reviews))
            return round($this->reviews->sum('rating') / count($this->reviews), 2);

        return false;
    }

    /**
     * Get rounded rating for star system
     * 
     * @return float
     */
    public function getRoundRating()
    {
        if ($avg_rating = $this->getAvgRating())
            return round($avg_rating * 2) / 2;

        return false;
    }

    /**
     * Get split for cost of script
     * 
     * @return array
     */
    public function getPriceSplit()
    {
        if ($this->waive_commission) {
            return [
                'site' => '0',
                'user' => $this->price,
            ];
        }
        $site_split = Config::get('paypal.price_split') / 100 * $this->price;
        $site_split = number_format((float)$site_split, 2, '.', '');
        $user_split = $this->price - $site_split;
        $user_split = number_format((float)$user_split, 2, '.', '');
        return [
            'site' => $site_split,
            'user' => $user_split,
        ];
    }

    /**
     * Total sales for script
     * 
     * @return float
     */
    public function totalSales()
    {
        $total = 0;
        foreach ($this->orders as $order) {
            $total += $order->price;
        }
        return number_format((float)$total, 2, '.', '');
    }


}
