<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScriptReview extends Model
{
    /**
     * The fields that are fillable
     *
     * @var array
     */
    protected $fillable = array(
        'rating',
        'review',
        'script_id',
        'user_id',
    );

    /**
     * Validation rules
     */
    public static $rules = array(
        'rating' => 'required|integer',
        'review' => 'required',
        'script_id' => 'required|integer',
        'user_id' => 'required|integer',
    );

    /**
    * Script relationship
    */
    public function script()
    {
        return $this->belongsTo('App\Script');
    }

    /**
    * User relationship
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
