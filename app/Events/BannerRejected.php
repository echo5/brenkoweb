<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;
use App\Banner;

class BannerRejected extends Event
{
    use SerializesModels;

    public $user;
    public $banner;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Banner $banner)
    {
        $this->user = $user;
        $this->banner = $banner;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
