<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ScriptPurchased extends Event
{
    use SerializesModels;

    public $user;
    public $order;
    public $script;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $order, $script)
    {
        $this->user = $user;
        $this->order = $order;
        $this->script = $script;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
