<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Order;
use App\Repositories\UserRepository;
use App\Repositories\BannerRepository;
use App\Repositories\BannerPlanRepository;

class BannerCreditPurchased extends Event
{
    use SerializesModels;

    public $banner;
    public $bannerPlan;
    public $order;
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $order, $bannerPlan, $banner)
    {
        $this->user = $user;
        $this->banner = $banner;
        $this->bannerPlan = $bannerPlan;
        $this->order = $order;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
