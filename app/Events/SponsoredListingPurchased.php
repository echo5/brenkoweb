<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Order;

class SponsoredListingPurchased extends Event
{
    use SerializesModels;

    public $user;
    public $listing;
    public $order;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $order, $listing)
    {
        $this->user = $user;
        $this->listing = $listing;
        $this->order = $order;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
