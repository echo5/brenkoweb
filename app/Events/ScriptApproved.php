<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;
use App\Script;

class ScriptApproved extends Event
{
    use SerializesModels;

    public $user;
    public $script;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Script $script)
    {
        $this->user = $user;
        $this->script = $script;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
