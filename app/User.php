<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Hash;

class User extends Model implements AuthenticatableContract,
                                    CanResetPasswordContract,
                                    HasRoleAndPermissionContract
{
    use Authenticatable, CanResetPassword, HasRoleAndPermission;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'email',
        'avatar',
        'website',
        'paypal_email',
        'password',
        'provider',
        'provider_id',
        'status'
    ];

    /**
     * Validation rules
     */
    public static $rules = array(
        'name' => 'required',
        'username' => 'unique:users',
        'email' => 'required|email',
        'status' => 'required',
    );

    /**
     * Get status options
     * 
     * @return array
     */
    public static function getStatuses()
    {
        return array(
            'approved',
            'suspended',
        );
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Mutator for password
     *
     * @param $password
     */
    public function setPasswordAttribute($password){
        if (!empty($password)) {
            if(Hash::needsRehash($password)) {
                $this->attributes['password'] = Hash::make($password);
            }
            else {
                $this->attributes['password'] = $password;
            }
        }
    }

    /**
     * Get a user's avatar URL
     * 
     * @param  $size
     * @return str
     */
    public function getAvatar($size = 100)     
    {   
        $avatar_sizes = ['30' => 'small/', '100' => 'medium/', '200' => 'large/'];

        if (!empty($this->avatar))
            return url('/') . config('assets.userAvatarsDirectory') . $avatar_sizes[$size] . $this->avatar;

        $default = "mm";
        if($this->provider){     
            switch ($this->provider) {
                case 'facebook':
                    return "http://graph.facebook.com/{$this->provider_id}/picture?type=large&width={$size}&height={$size}";     
            }
        }     
        return "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $this->email ) ) ) . "?s=" . $size . "&d=" . $default;     
    }

    /**
     * Get name for display on site or emails
     * 
     * @return str
     */
    public function getDisplayName()
    {
        if (!empty($this->name))
            return $this->name;
        return $this->username;
    }
    
    /**
    * Posts relationship
    */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    /**
    * Scripts relationship
    */
    public function scripts()
    {
        return $this->hasMany('App\Script');
    }

    /**
    * Script Reviews relationship
    */
    public function scriptReviews()
    {
        return $this->hasMany('App\ScriptReview');
    }

    /**
    * Listings relationship
    */
    public function listings()
    {
        return $this->hasMany('App\Listing');
    }

    /**
    * Banners relationship
    */
    public function banners()
    {
        return $this->hasMany('App\Banner');
    }

    /**
    * Comments relationship
    */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
    * Orders relationship
    */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * Script Purchases relationship
     */
    public function scriptPurchases()
    {
        return $this->belongsToMany('App\Script', 'script_purchases')->withTimestamps();
    }

    /**
     * Read activities relationship
     */
    public function readActivities()
    {
        return $this->belongsToMany('App\Activity', 'activity_log_readers')->withTimestamps();
    }

    /**
     * Total sales for users scripts
     * 
     * @return float
     */
    public function totalSales()
    {
        $total = 0;
        foreach ($this->scripts as $script) {
            $total += $script->totalSales();
        }
        return number_format((float)$total, 2, '.', '');
    }

}
