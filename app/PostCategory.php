<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{

    /**
     * Validation rules
     */
    public static $rules = array(
        'name' => 'required|unique:post_categories',
        'slug' => 'required|alpha_dash|unique:post_categories',
    );

    /**
    * Parent relationship
    */
    public function parent()
    {
        return $this->belongsTo('App\PostCategory', 'parent_id');
    }

    /**
    * Children relationship
    */
    public function children()
    {
        return $this->hasMany('App\PostCategory', 'parent_id');
    }

    /**
    * Post relationship
    */
    public function posts()
    {
        return $this->hasMany('App\Post', 'category_id');
    }

    /**
     * Get ancestor categories
     * 
     * @return array
     */
    public function getAncestors()
    {
        $ancestors = $this->where('id', '=', $this->parent_id)->get();

        while ($ancestors->last() && $ancestors->last()->parent_id !== null)
        {
            $parent = $this->where('id', '=', $ancestors->last()->parent_id)->get();
            $ancestors = $ancestors->merge($parent);
        }

        return $ancestors;
    }

    /**
     * Get the post type string for use in URL
     * 
     * @return str
     */
    public function getTypeForUrl()
    {

        $types = [
            'tutorial' => 'tutorials',
            'article' => 'articles',
            'page' => ''
        ];

        return $types[$this->post_type];
    }

    /**
     * Get url for post category
     *
     * @param  $parent_slug
     * @return str
     */
    public function getUrl($parent_slug = null)
    {
        $hierarchy = '';
        if ($parent_slug)
            $hierarchy = $parent_slug . '/';
        elseif ($this->parent)
            $hierarchy = $this->parent->slug . '/';

        $hierarchy .= $this->slug;

        return Route(
            'post.' . $this->post_type,
            [
                'hierarchy' => $hierarchy
            ]
        );
    }

    /**
     * Get post count
     * @return int
     */
    public function postCount()
    {
        return $this->posts->count();
    }

}
