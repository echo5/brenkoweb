<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScriptCategory extends Model
{

    /**
     * Validation rules
     */
    public static $rules = array(
        'name' => 'required|unique:post_categories',
        'slug' => 'required|alpha_dash|unique:post_categories',
    );

    /**
    * Parent relationship
    */
    public function parent()
    {
        return $this->belongsTo('App\ScriptCategory', 'parent_id');
    }

    /**
    * Children relationship
    */
    public function children()
    {
        return $this->hasMany('App\ScriptCategory', 'parent_id');
    }

    /**
    * Listing relationship
    */
    public function scripts()
    {
        return $this->hasMany('App\Script', 'category_id');
    }

    /**
     * Total scripts for category and child categories
     * 
     * @return int
     */
    public function totalScripts()
    {
        $count = $this->scripts->where('status', 'approved')->count();
        if ($this->children) {
            foreach ($this->children as $child) {
                $count += $child->scripts->where('status', 'approved')->count();
            }
        }
        return $count;
    }

    /**
     * Get url for script category
     *
     * @param  $parent_slug
     * @return str
     */
    public function getUrl($parent_slug = null)
    {
        $hierarchy = '';
        if ($parent_slug)
            $hierarchy = $parent_slug . '/';
        elseif ($this->parent)
            $hierarchy = $this->parent->slug . '/';

        $hierarchy .= $this->slug;

        return Route(
            'scripts.index',
            [
                'hierarchy' => $hierarchy
            ]
        );
    }
}
