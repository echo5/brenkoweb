<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerStat extends Model
{
    /**
     * Turn off timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'impressions',
        'clicks'
    ];

    /**
    * Banner relationship
    */
    public function banner()
    {
        return $this->belongsTo('App\Banner');
    }

    /**
     * Test for current month
     *
     * @return bool
     */
    public function isCurrentMonth()
    {
        $date = \DateTime::createFromFormat("Y-m-d", $this->date);

        if ($date->format('Y') == date('Y') && $date->format('m') == date('m')) {
            return true;
        }
        return false;
    }
}
