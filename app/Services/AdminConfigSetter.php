<?php

namespace App\Services;

use Config;

class AdminConfigSetter
{

    public $site_settings;

    /**
     * Construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->site_settings = json_decode(file_get_contents(storage_path() . '/administrator_settings/site.json'));
        $this->setConfig();
    }

    public function setConfig()
    {
        Config::set('settings.siteName', $this->site_settings->site_name);
        Config::set('settings.adminEmail', $this->site_settings->admin_email);
        Config::set('paypal.price_split', $this->site_settings->price_split);
        Config::set('settings.home_articles', $this->site_settings->home_articles);
        Config::set('settings.home_tutorials', $this->site_settings->home_tutorials);
        Config::set('settings.home_scripts', $this->site_settings->home_scripts);
        // Config::set('settings.home_sponsored_script', $this->site_settings->home_sponsored_script);
        Config::set('settings.home_sponsored_listing', $this->site_settings->home_sponsored_listing);
        Config::set('settings.banner_low_impressions', $this->site_settings->banner_low_impressions);
        Config::set('settings.banner_low_days', $this->site_settings->banner_low_days);
        Config::set('settings.upload_image_max_width', $this->site_settings->upload_image_max_width);
        Config::set('settings.upload_image_max_size', $this->site_settings->upload_image_max_size);
    }


}
