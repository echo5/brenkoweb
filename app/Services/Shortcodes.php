<?php namespace App\Services;

use Shortcode;
use URL;

class Shortcodes {

    /**
     * Init shortcodes
     * 
     * @return null
     */
    public function __construct() {
        $this->registerShortcodes();
    }

    /**
     * Register sitewide shortcodes
     * 
     * @return mixed
     */
    public function registerShortcodes()
    {

        // Auth
        Shortcode::register('user_name', function($attr, $content = null, $name = null)
        {
            return $this->user->getDisplayName();
        });  
        Shortcode::register('user', function($attr, $content = null, $name = null)
        {
            $param = array_get($attr, 'param');
            return $this->user->{$param};
        });
        Shortcode::register('password_link', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            if(!empty($text))
                return '<a href="'.url('password/reset/'.$this->token).'">'. $text .'</a>';
            else
                return url('password/reset/'.$this->token);
        });
        Shortcode::register('profile_link', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            $page = array_get($attr, 'page');
            $page = ($page ? $page : 'show');
            if(!empty($text))
                return '<a href="'.URL::route('profile.' . $page, ['id' => $this->user->id]).'">'. $text .'</a>';
            else
                return URL::route('profile.'. $page, ['id' => $this->user->id]);
        });
        Shortcode::register('expiration_time', function($attr, $content = null, $name = null)
        {
            return config('auth.reminder.expire', 60);
        });

        // Posts
        Shortcode::register('post', function($attr, $content = null, $name = null)
        {
            $param = array_get($attr, 'param');
            return $this->post->{$param};
        });
        Shortcode::register('post_link', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            if(!empty($text))
                return '<a href="'.URL::to($this->post->getUrl()).'">'. $text .'</a>';
            else
                return URL::to($this->post->getUrl());
        });

        // Banners
        Shortcode::register('banner', function($attr, $content = null, $name = null)
        {
            $param = array_get($attr, 'param');
            return $this->banner->{$param};
        });

        // Listings
        Shortcode::register('listing', function($attr, $content = null, $name = null)
        {
            $param = array_get($attr, 'param');
            return $this->listing->{$param};
        });

        // Scripts
        Shortcode::register('script', function($attr, $content = null, $name = null)
        {
            $param = array_get($attr, 'param');
            return $this->script->{$param};
        });
        Shortcode::register('script_link', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            if(!empty($text))
                return '<a href="'.URL::route('script.show', ['id' => $this->script->id]).'">'. $text .'</a>';
            else
                return URL::route('script.show', ['id' => $this->script->id]);
        });

        // Orders
        Shortcode::register('order', function($attr, $content = null, $name = null)
        {
            $param = array_get($attr, 'param');
            return $this->order->{$param};
        });

        // Code Parser
        Shortcode::register('codeparser', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            $height = array_get($attr, 'height');
            $height = (isset($height) ? $height : '300');
            $preview = array_get($attr, 'preview');
            $preview = (isset($preview) ? $preview : true);
            $preview = filter_var($preview, FILTER_VALIDATE_BOOLEAN);
            $width = array_get($attr, 'width');
            $width = (isset($width) ? $width : '50%');
            $decode = array_get($attr, 'decode');
            $decode = (isset($decode) ? $decode : false);
            $div_id = uniqid();

            // Strip plain text script tags
            $html = $text;
            $html = preg_replace("/<script type=\"text\/plain\">/", "", $html);
            $html = preg_replace("/<\/script type=\"text\/plain\">/", "", $html);

            if ($decode) {
                $html = htmlspecialchars_decode($html);
            }

            ob_start();
            ?>
            <div id="codeparser-wrap-<?php echo $div_id ?>" class="codeparser-wrap">
                <textarea id="codeparser-<?php echo $div_id ?>" name="codeparser" class="codeparser" style="height:<?php echo $height; ?>px;width:<?php echo $width; ?>;"><?php echo htmlspecialchars($html) ?></textarea>
                <?php if ($preview) { ?>
                    <iframe id="codeparser-preview-<?php echo $div_id ?>" class="codeparser-preview" style="height:<?php echo $height; ?>px;width:<?php echo $width; ?>;"></iframe>
                <?php } ?>
                <script>
                    var delay;
                    var editor<?php echo $div_id; ?> = CodeMirror.fromTextArea(document.getElementById('codeparser-<?php echo $div_id ?>'), {
                        mode: 'application/x-httpd-php'
                    });
                    editor<?php echo $div_id; ?>.setSize("<?php echo $width; ?>", <?php echo $height; ?>);
                    editor<?php echo $div_id; ?>.on("change", function() {
                        clearTimeout(delay);
                        delay = setTimeout(updatePreview<?php echo $div_id; ?>, 300);
                    });
                    <?php if ($preview) { ?>
                        function updatePreview<?php echo $div_id; ?>() {
                            var previewFrame = document.getElementById('codeparser-preview-<?php echo $div_id ?>');
                            var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
                            preview.open();
                            preview.write(editor<?php echo $div_id; ?>.getValue());
                            preview.close();
                        }
                        setTimeout(updatePreview<?php echo $div_id; ?>, 300);
                    <?php } ?>
                </script>
            </div>
            <?php
            $output = ob_get_clean();

            return $output;
        });

        // Code Widget
        Shortcode::register('codewidget', function($attr, $content = null, $name = null)
        {
            $text = Shortcode::compile($content);
            $height = array_get($attr, 'height');
            $height = (isset($height) ? $height : '300');
            $width = array_get($attr, 'width');
            $width = (isset($width) ? $width : '100');
            $decode = array_get($attr, 'decode');
            $decode = (isset($decode) ? $decode : false);
            $div_id = uniqid();

            // Strip plain text script tags
            $html = $text;
            $html = preg_replace("/<script type=\"text\/plain\">/", "", $html);
            $html = preg_replace("/<\/script type=\"text\/plain\">/", "", $html);

            if ($decode) {
                $html = htmlspecialchars_decode($html);
            }

            ob_start();
            ?>
            <iframe id="iframe-<?php echo $div_id; ?>" class="codeparser" src="<?php echo route('codeparser'); ?>?height=<?php echo $height; ?>" style="height:<?php echo $height + 42; ?>px;width:<?php echo $width; ?>;">
            </iframe>
            <script>
            $( document ).ready(function() {
                function addCodeToIframe<?php echo $div_id; ?>() {
                    var iframe = document.getElementById('iframe-<?php echo $div_id; ?>');
                    var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
                    editor = innerDoc.getElementsByClassName('CodeMirror')[0].CodeMirror;
                    var content = <?php echo json_encode($html); ?>;
                    editor.getDoc().setValue(content);
                }
                setTimeout(addCodeToIframe<?php echo $div_id; ?>, 600);
            });
            </script>
            <?php
            $output = ob_get_clean();

            return $output;
        });

    }

    /**
     * Set a token
     * 
     * @param $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Set user
     * 
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Set post
     * 
     * @param Post $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    /**
     * Set banner
     * 
     * @param Banner $banner,
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
    }

    /**
     * Set listing
     * 
     * @param Listing $listing
     */
    public function setListing($listing)
    {
        $this->listing = $listing;
    }

    /**
     * Set script
     * 
     * @param Script $script
     */
    public function setScript($script)
    {
        $this->script = $script;
    }

    /**
     * Set order
     * @param Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

}