<?php

namespace App\Handlers\Events;

use App\Events\ListingRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RejectListing
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ListingRejected  $event
     * @return void
     */
    public function handle(ListingRejected $event)
    {
        $event->listing->status = 'rejected';
        $event->listing->save();
    }
}
