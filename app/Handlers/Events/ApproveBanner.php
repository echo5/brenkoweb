<?php

namespace App\Handlers\Events;

use App\Events\BannerApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveBanner
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BannerApproved  $event
     * @return void
     */
    public function handle(BannerApproved $event)
    {
        $event->banner->status = 'approved';
        $event->banner->save();
    }
}
