<?php

namespace App\Handlers\Events;

use App\Events\SponsoredListingPurchased;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Activity;

class LogSponsoredListingPurchased
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SponsoredListingPurchased  $event
     * @return void
     */
    public function handle(SponsoredListingPurchased $event)
    {
        Activity::log([
            'user_id'     => $event->user->id,
            'contentId'   => $event->order->id,
            'contentType' => 'Order',
            'action'      => 'Purchase',
            'description' => 'Sponsored listing purchased',
            'details'     => 'Listing ID: '.$event->listing->id.' Order ID: ' .$event->order->id,
        ]);
    }
}
