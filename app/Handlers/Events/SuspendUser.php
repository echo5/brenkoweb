<?php

namespace App\Handlers\Events;

use App\Events\UserSuspended;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuspendUser
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserSuspended  $event
     * @return void
     */
    public function handle(UserSuspended $event)
    {
        $event->user->status = 'suspended';
        $event->user->save();
    }
}
