<?php

namespace App\Handlers\Events;

use App\Events\ArticleRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Config;
use App\Repositories\EmailTemplateRepository;

class SendArticleRejectedEmail
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct(EmailTemplateRepository $email)
    {
        $this->email = $email;
    }

    /**
     * Handle the event.
     *
     * @param  ArticleRejected  $event
     * @return void
     */
    public function handle(ArticleRejected $event)
    {
        $user = $event->user;
        $email = $this->email->findBy('view', 'emails.article.rejected');

        Mail::send($email->view, $data = ['user' => $user, 'post' => $event->post], function($message) use ($user, $email)
        {
          $message->from(Config::get('settings.adminEmail'), Config::get('settings.siteName'));
          $message->to($user->email, $user->getDisplayName())->subject($email->subject);
        });  
    }
}
