<?php

namespace App\Handlers\Events;

use App\Events\BannerRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RejectBanner
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BannerRejected  $event
     * @return void
     */
    public function handle(BannerRejected $event)
    {
        $event->banner->status = 'rejected';
        $event->banner->save();
    }
}
