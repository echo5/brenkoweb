<?php

namespace App\Handlers\Events;

use App\Events\ArticleSuspended;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuspendArticle
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ArticleSuspended  $event
     * @return void
     */
    public function handle(ArticleSuspended $event)
    {
        $event->post->status = 'suspended';
        $event->post->save();
    }
}
