<?php

namespace App\Handlers\Events;

use App\Events\BannerSuspended;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuspendBanner
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BannerSuspended  $event
     * @return void
     */
    public function handle(BannerSuspended $event)
    {
        $event->banner->status = 'suspended';
        $event->banner->save();
    }
}
