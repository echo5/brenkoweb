<?php

namespace App\Handlers\Events;

use App\Events\ArticleApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveArticle
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ArticleApproved  $event
     * @return void
     */
    public function handle(ArticleApproved $event)
    {
        $event->post->status = 'approved';
        $event->post->save();
    }
}
