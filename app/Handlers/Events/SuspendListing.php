<?php

namespace App\Handlers\Events;

use App\Events\ListingSuspended;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuspendListing
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ListingSuspended  $event
     * @return void
     */
    public function handle(ListingSuspended $event)
    {
        $event->listing->status = 'suspended';
        $event->listing->save();
    }
}
