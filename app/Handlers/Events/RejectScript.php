<?php

namespace App\Handlers\Events;

use App\Events\ScriptRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RejectScript
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScriptRejected  $event
     * @return void
     */
    public function handle(ScriptRejected $event)
    {
        $event->script->status = 'rejected';
        $event->script->save();
    }
}
