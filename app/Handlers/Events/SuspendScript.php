<?php

namespace App\Handlers\Events;

use App\Events\ScriptSuspended;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuspendScript
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScriptSuspended  $event
     * @return void
     */
    public function handle(ScriptSuspended $event)
    {
        $event->script->status = 'suspended';
        $event->script->save();
    }
}
