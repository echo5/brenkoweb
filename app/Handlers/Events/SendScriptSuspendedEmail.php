<?php

namespace App\Handlers\Events;

use App\Events\ScriptSuspended;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Config;
use Mail;
use App\Repositories\EmailTemplateRepository;

class SendScriptSuspendedEmail
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct(EmailTemplateRepository $email)
    {
        $this->email = $email;
    }

    /**
     * Handle the event.
     *
     * @param  ScriptSuspended  $event
     * @return void
     */
    public function handle(ScriptSuspended $event)
    {
        $user = $event->user;
        $email = $this->email->findBy('view', 'emails.script.suspended');

        Mail::send($email->view, $data = ['user' => $user, 'script' => $event->script], function($message) use ($user, $email)
        {
          $message->from(Config::get('settings.adminEmail'), Config::get('settings.siteName'));
          $message->to($user->email, $user->getDisplayName())->subject($email->subject);
        });   
    }
}
