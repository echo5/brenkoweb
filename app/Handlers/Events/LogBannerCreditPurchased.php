<?php

namespace App\Handlers\Events;

use App\Events\BannerCreditPurchased;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Activity;

class LogBannerCreditPurchased
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BannerCreditPurchased  $event
     * @return void
     */
    public function handle(BannerCreditPurchased $event)
    {
        Activity::log([
            'user_id'     => $event->user->id,
            'contentId'   => $event->order->id,
            'contentType' => 'Order',
            'action'      => 'Purchase',
            'description' => 'Sponsored banner purchased',
            'details'     => 'Banner ID: '.$event->banner->id.' Order ID: ' .$event->order->id,
        ]);
    }
}
