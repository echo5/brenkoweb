<?php

namespace App\Handlers\Events;

use App\Events\ArticleRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RejectArticle
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ArticleRejected  $event
     * @return void
     */
    public function handle(ArticleRejected $event)
    {
        $event->post->status = 'rejected';
        $event->post->save();
    }
}
