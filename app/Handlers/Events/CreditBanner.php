<?php

namespace App\Handlers\Events;

use App\Events\BannerCreditPurchased;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\BannerRepository;

class CreditBanner
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct(BannerRepository $banner)
    {
        $this->banner = $banner;
    }

    /**
     * Handle the event.
     *
     * @param  BannerCreditPurchased  $event
     * @return void
     */
    public function handle(BannerCreditPurchased $event)
    {
        if ($event->bannerPlan->impressions > 0)
            $this->banner->addToMaxImpressions($event->bannerPlan->impressions, $event->banner->id);
        if ($event->bannerPlan->days > 0)
            $this->banner->addToEndDate($event->bannerPlan->days, $event->banner->id);
    }
}
