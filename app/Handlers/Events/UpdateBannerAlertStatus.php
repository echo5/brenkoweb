<?php

namespace App\Handlers\Events;

use App\Events\LowBannerCredit;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateBannerAlertStatus
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LowBannerCredit  $event
     * @return void
     */
    public function handle(LowBannerCredit $event)
    {
        if ($event->banner->alert_status == 'sent')
            dd('already udpated!!');
        $event->banner->alert_status = 'sent';
        $event->banner->save();
    }
}
