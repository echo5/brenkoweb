<?php

namespace App\Handlers\Events;

use App\Events\ScriptRejected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Config;
use Mail;
use App\Repositories\EmailTemplateRepository;

class SendScriptRejectedEmail
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct(EmailTemplateRepository $email)
    {
        $this->email = $email;
    }

    /**
     * Handle the event.
     *
     * @param  ScriptRejected  $event
     * @return void
     */
    public function handle(ScriptRejected $event)
    {
        $user = $event->user;
        $email = $this->email->findBy('view', 'emails.script.rejected');

        Mail::send($email->view, $data = ['user' => $user, 'script' => $event->script], function($message) use ($user, $email)
        {
          $message->from(Config::get('settings.adminEmail'), Config::get('settings.siteName'));
          $message->to($user->email, $user->getDisplayName())->subject($email->subject);
        });   
    }
}
