<?php

namespace App\Handlers\Events;

use App\Events\SponsoredListingPurchased;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SponsorListing
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SponsoredListingPurchased  $event
     * @return void
     */
    public function handle(SponsoredListingPurchased $event)
    {
        $event->listing->sponsored = '1';
        $event->listing->save();
    }
}
