<?php

namespace App\Handlers\Events;

use App\Events\ScriptPurchased;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Activity;

class LogScriptPurchased
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScriptPurchased  $event
     * @return void
     */
    public function handle(ScriptPurchased $event)
    {
        Activity::log([
            'user_id'     => $event->user,
            'contentId'   => $event->order->id,
            'contentType' => 'Order',
            'action'      => 'Purchase',
            'description' => 'Script purchased',
            'details'     => 'Script ID: '.$event->script->id.' Order ID: ' .$event->order->id,
        ]);
    }
}
