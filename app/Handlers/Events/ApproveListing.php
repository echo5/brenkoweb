<?php

namespace App\Handlers\Events;

use App\Events\ListingApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveListing
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ListingApproved  $event
     * @return void
     */
    public function handle(ListingApproved $event)
    {
        $event->listing->status = 'approved';
        $event->listing->save();
    }
}
