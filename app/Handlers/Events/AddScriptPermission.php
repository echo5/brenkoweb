<?php

namespace App\Handlers\Events;

use App\Events\ScriptPurchased;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddScriptPermission
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScriptPurchased  $event
     * @return void
     */
    public function handle(ScriptPurchased $event)
    {
        $event->user->scriptPurchases()->attach($event->script->id);
    }
}
