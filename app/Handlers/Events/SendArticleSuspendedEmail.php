<?php

namespace App\Handlers\Events;

use App\Events\ArticleSuspended;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Config;
use App\Repositories\EmailTemplateRepository;

class SendArticleSuspendedEmail
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct(EmailTemplateRepository $email)
    {
        $this->email = $email;
    }

    /**
     * Handle the event.
     *
     * @param  ArticleSuspended  $event
     * @return void
     */
    public function handle(ArticleSuspended $event)
    {
        $user = $event->user;
        $email = $this->email->findBy('view', 'emails.article.suspended');

        Mail::send($email->view, $data = ['user' => $user, 'post' => $event->post], function($message) use ($user, $email)
        {
          $message->from(Config::get('settings.adminEmail'), Config::get('settings.siteName'));
          $message->to($user->email, $user->getDisplayName())->subject($email->subject);
        }); 
    }
}
