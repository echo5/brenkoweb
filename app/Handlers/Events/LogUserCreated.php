<?php

namespace App\Handlers\Events;

use App\Events\UserCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Activity;

class LogUserCreated
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $user = $event->user;
        Activity::log([
            'user_id'     => $user->id,
            'contentId'   => $user->id,
            'contentType' => 'User',
            'action'      => 'Create',
            'description' => 'New user signup',
            'details'     => 'Name: '.$user->getDisplayName(),
        ]);
    }
}
