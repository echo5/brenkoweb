<?php

namespace App\Handlers\Events;

use App\Events\ScriptPurchased;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\UserRepository;
use Mail;
use Config;
use App\Repositories\EmailTemplateRepository;
use App\Repositories\ScriptRepository;

class SendOrderConfirmationEmail
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct(UserRepository $user, EmailTemplateRepository $email, ScriptRepository $script)
    {
        $this->user = $user;
        $this->email = $email;
        $this->script = $script;
    }

    /**
     * Handle the event.
     *
     * @param  ScriptPurchased  $event
     * @return void
     */
    public function handle(ScriptPurchased $event)
    {
        $user = $event->user;
        $email = $this->email->findBy('view', 'emails.script.purchase');
        $script = $event->script;

        Mail::send($email->view, $data = ['user' => $user, 'order' => $event->order, 'script' => $script], function($message) use ($user, $email)
        {
          $message->from(Config::get('settings.adminEmail'), Config::get('settings.siteName'));
          $message->to($user->email, $user->getDisplayName())->subject($email->subject);
        });   
    }
}
