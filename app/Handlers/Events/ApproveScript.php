<?php

namespace App\Handlers\Events;

use App\Events\ScriptApproved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveScript
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScriptApproved  $event
     * @return void
     */
    public function handle(ScriptApproved $event)
    {
        $event->script->status = 'approved';
        $event->script->save();
    }
}
