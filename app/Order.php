<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The fields that are fillable
     *
     * @var array
     */
    protected $fillable = array(
        'payer_name',
        'payer_email',
        'payment_method',
        'payment_id',
        'payer_id',
        'payment_status',
        'payment_date',
        'user_id',
    );

    /**
    * User relationship
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Items relationship
     */
    public function items()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function total()
    {
        $amount = 0;
        foreach ($this->items as $item) {
            $amount += $item->price;
        }
        return number_format((float)$amount, 2, '.', '');
    }


}
