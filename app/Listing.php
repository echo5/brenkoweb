<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{

    /**
     * The fields that are fillable
     *
     * @var array
     */
    protected $fillable = array(
        'title',
        'link',
        'image',
        'description',
        'user_id',
        'plan_id',
        'category_id',
        'sponsored',
    );

    /**
     * Validation rules
     */
    public static $rules = array(
        'title' => 'required|unique:listings',
        'description' => 'required',
        'link' => 'required|active_url|unique:listings',
        'image' => 'required',
        'category_id' => 'required|integer',
        'plan_id' => 'required|integer',
    );

    /**
     * Get status options
     * 
     * @return array
     */
    public static function getStatuses()
    {
        return array(
            'approved',
            'suspended',
            'rejected',
            'pending'
        );
    }

    /**
    * Category relationship
    */
    public function category()
    {
        return $this->belongsTo('App\ListingCategory');
    }

    /**
    * Plan relationship
    */
    public function plan()
    {
        return $this->belongsTo('App\ListingPlan');
    }

    /**
    * User relationship
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
