<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The fields that are fillable
     *
     * @var array
     */
    protected $fillable = array(
        'title',
        'slug',
        'type',
        'image',
        'meta_description',
        'content',
        'user_id',
        'template_id',
        'category_id',
    );

    /**
     * Validation rules
     */
    public static $rules = array(
        'status' => 'required',
        'title' => 'required|unique:posts',
        'slug' => 'required|alpha_dash|unique:posts',
        'content' => 'required',
        'user_id' => 'required',
    );

    /**
     * Get post types
     * 
     * @return array
     */
    public static function getTypes()
    {
        return array(
            'tutorial' => 'tutorials',
            'article' => 'articles',
            'page' => ''
        );
    }

    /**
     * Get status options
     * 
     * @return array
     */
    public static function getStatuses()
    {
        return array(
            'approved',
            'suspended',
            'rejected',
            'pending'
        );
    }

    /**
    * Category relationship
    */
    public function category()
    {
        return $this->belongsTo('App\PostCategory');
    }

    /**
    * Template relationship
    */
    public function template()
    {
        return $this->belongsTo('App\PostTemplate');
    }

    /**
    * User relationship
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
    * Comments relationship
    */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Get the next post
     *     
     * @return Post
     */
    public function next()
    {
        return $this->where('id', '>', $this->id)
            ->where('status', 'approved')
            ->where('type', $this->type)
            ->orderBy('id','asc')
            ->take(1)
            ->get()
            ->first();
    }

    /**
     * Get the previous post
     *     
     * @return Post
     */
    public function previous()
    {
        return $this->where('id', '<', $this->id)
            ->where('status', 'approved')
            ->where('type', $this->type)
            ->orderBy('id','desc')
            ->take(1)
            ->get()
            ->first();
    }

    /**
     * Get the post type string for use in URL
     * 
     * @return str
     */
    public function getTypeForUrl()
    {
        $types = $this->getTypes();
        return $types[$this->type];
    }

    /**
     * Get url for post
     * 
     * @return str
     */
    public function getUrl()
    {
        if ($this->category) {
            $url = Route(
                'post.' . $this->type,
                [
                    'hierarchy' => (($this->category->parent) ? $this->category->parent->slug . '/' : '') . $this->category->slug . '/' . $this->slug
                ]
            );
        }
        else {
            $url = Route(
                'post.' . $this->type,
                [
                    'hierarchy' => $this->slug
                ]
            );
        }
        return $url; 
    }


    /**
     * Get Image URL for post
     * 
     * @param  string $size
     * @return str
     */
    public function getImageUrl($size = 'medium')
    {
        $image = '';
        if ($this->image) {
            return config('assets.postImageThumbsDirectory') . $size . '/' . $this->image;
        }
        if ($this->category) {
            if ($this->category->image) {
                return config('assets.postCategoryImageThumbsDirectory') . $size . '/' . $this->category->image;
            }
            if ($this->category->parent) {
                if ($this->category->parent->image) {
                    return config('assets.postCategoryImageThumbsDirectory') . $size . '/' . $this->category->parent->image;
                }
            }
        }
        return $image;
    }

    /**
     * Get image html output
     * @param  string $size [description]
     * @return [type]       [description]
     */
    public function getImage($size = 'medium')
    {
        $image = '';
        if (($src = $this->getImageUrl($size)) != '') {
            $image = '<img src="'. $src . '" alt="' . $this->title . '">';
        }
        return $image;
    }

    /**
     * Show post template or default
     * 
     * @param  $default
     * @return str
     */
    public function getTemplate($default = 'default')
    {
        if ($this->template) {
            if (view()->exists($this->template->view)) {
                return $this->template->view;
            }
        }
        return 'posts.' . $default;
    }

}
