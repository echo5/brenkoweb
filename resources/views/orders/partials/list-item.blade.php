<div class="panel panel-default">
	<div class="panel-heading">Order History</div>
	<table class="table">
		<thead>
			<tr>
				<th>Order #</th>
				<th>Payment ID</th>
				<th>Date</th>
				<th>Username</th>
			</tr>
		</thead>
		<tbody>
			<tr>
			<th scope="row">{{ $order->id }}</th>
			<td>{{ $order->payment_id }}</td>
			<td>{{ $order->created_at }}</td>
			<td></td>
			</tr>
		</tbody>
    </table>
</div>