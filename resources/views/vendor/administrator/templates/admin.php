<div class="table_container">

	<div class="results_header content-header">
		<!-- <h1 data-bind="text: modelTitle"></h1> -->

		<div class="action_message" data-bind="css: { error: globalStatusMessageType() == 'error', success: globalStatusMessageType() == 'success' },
										notification: globalStatusMessage "></div>
	</div>


    <div class="col-xs-12">
        <div class="box box-body">
            <div class="box-header with-border">
                <h3 class="box-title" data-bind="text: modelTitle"></h3>
                <div class="actions">
                    <!-- ko if: globalActions().length -->
                        <!-- ko foreach: globalActions -->
                            <!-- ko if: has_permission -->
                                <input type="button" data-bind="click: function(){$root.customAction(false, action_name, messages, confirmation)}, value: title,
                                                                                attr: {disabled: $root.freezeForm() || $root.freezeActions()}" />
                            <!-- /ko -->
                        <!-- /ko -->
                    <!-- /ko -->
                    <!-- ko if: actionPermissions.create -->
                        <a class="new_item btn btn-primary"
                            data-bind="attr: {href: base_url + modelName() + '/new'},
                                        text: '<?php echo trans('administrator::administrator.new') ?> ' + modelSingle()"></a>
                    <!-- /ko -->
                </div>
            </div>
            <div class="box-body">
                <table class="results table table-bordered table-hover dataTable" border="0" cellspacing="0" id="customers" cellpadding="0">
                    <thead>
                        <tr>
                            <!-- ko foreach: columns -->
                                <th data-bind="visible: visible, css: {sortable: sortable,
                'sorted-asc': (column_name == $root.sortOptions.field() || sort_field == $root.sortOptions.field()) && $root.sortOptions.direction() === 'asc',
                'sorted-desc': (column_name == $root.sortOptions.field() || sort_field == $root.sortOptions.field()) && $root.sortOptions.direction() === 'desc'}">
                                    <!-- ko if: sortable -->
                                        <div data-bind="click: function() {$root.setSortOptions(sort_field ? sort_field : column_name)}, text: title"></div>
                                    <!-- /ko -->

                                    <!-- ko ifnot: sortable -->
                                        <div data-bind="text: title"></div>
                                    <!-- /ko -->
                                </th>
                            <!-- /ko -->
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ko foreach: rows -->
                            <tr data-bind="click: function() {$root.clickItem($data[$root.primaryKey].raw); return true},
                                        css: {result: true, even: $index() % 2 == 1, odd: $index() % 2 != 1,
                                                selected: $data[$root.primaryKey].raw == $root.itemLoadingId()}">
                                <!-- ko foreach: $root.columns -->
                               		<!-- ko if: $data[column_name] == 'Title' || $data[column_name] == 'title' -->
                                    <td data-bind="text: $parentContext.$data[column_name].rendered, visible: visible"></td> 
                                    <!-- /ko -->
                                    <!-- ko if: $data[column_name] != 'Title' && $data[column_name] != 'title' -->
                                    <td data-bind="html: $parentContext.$data[column_name].rendered, visible: visible"></td> 
                                    <!-- /ko -->
                                <!-- /ko -->
                            </tr>
                        <!-- /ko -->
                    </tbody>
                </table>  

                <div class="page_container">
                    <div class="per_page">
                        <input type="hidden" class="" data-bind="value: rowsPerPage, select2: {minimumResultsForSearch: -1, data: {results: rowsPerPageOptions},
                                                        allowClear: false}" />
                        <span> <?php echo trans('administrator::administrator.itemsperpage') ?></span>
                    </div>
                    <div class="paginator">
                        <input class="paginate_button previous disabled" type="button" value="<?php echo trans('administrator::administrator.previous') ?>"
                                data-bind="css: { disabled: pagination.isFirst() || !pagination.last() || !initialized() }, attr: {disabled: pagination.isFirst() || !pagination.last() || !initialized() }, click: function() {page('prev')}" />
                        <input class="paginate_button previous disabled"  type="button" value="<?php echo trans('administrator::administrator.next') ?>"
                                data-bind="css: { disabled: pagination.isLast() || !pagination.last() || !initialized() }, attr: {disabled: pagination.isLast() || !pagination.last() || !initialized() }, click: function() {page('next')}" />
                        <input type="text" data-bind="attr: {disabled: pagination.last() === 0 || !initialized() }, value: pagination.page" />
                        <span data-bind="text: ' / ' + pagination.last()"></span>
                    </div>
                </div>

            </div>
        </div>

        <div class="loading_rows" data-bind="visible: loadingRows">
            <div><?php echo trans('administrator::administrator.loading') ?></div>
        </div>

        <div class="no_results" data-bind="visible: pagination.last() === 0">
            <div><?php echo trans('administrator::administrator.noresults') ?></div>
        </div>
    </div>



</div>

<div class="item_edit_container" data-bind="itemTransition: activeItem() !== null || loadingItem(), style: {width: expandWidth() + 'px'}">
	<div class="item_edit" data-bind="template: 'itemFormTemplate', style: {width: (expandWidth() - 27) + 'px'}"></div>
</div>