<header class="main-header">

	<a class="logo" href="{{route('admin_dashboard')}}">{{config('administrator.title')}}</a>

	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
	  <!-- Sidebar toggle button-->
	  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
	    <span class="sr-only">Toggle navigation</span>
	  </a>
	  <!-- Navbar Right Menu -->
	  <div class="navbar-custom-menu">
	    <ul class="nav navbar-nav">
	      @include('admin.activitylog')
	      <!-- User Account: style can be found in dropdown.less -->
	      <li class="dropdown user user-menu">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	          <i class="fa fa-user"></i>
	          <span class="hidden-xs">{{ Auth::user()->name }}</span>
	        </a>
	        <ul class="dropdown-menu">
	          <!-- User image -->
	          <li class="user-header">
	            <img src="{{ Auth::user()->getAvatar() }}" class="img-circle">
	            <p>
	              {{ Auth::user()->name }}
	              <small>Member since {{ date('F', strtotime(Auth::user()->created_at)) }} {{ date('jS', strtotime(Auth::user()->created_at)) }}, {{ date('Y', strtotime(Auth::user()->created_at)) }}</small>
	            </p>
	          </li>
	          <!-- Menu Body -->
{{-- 	          <li class="user-body">
	            <div class="col-xs-4 text-center">
	              <a href="#">Followers</a>
	            </div>
	            <div class="col-xs-4 text-center">
	              <a href="#">Sales</a>
	            </div>
	            <div class="col-xs-4 text-center">
	              <a href="#">Friends</a>
	            </div>
	          </li> --}}
	          <!-- Menu Footer-->
	          <li class="user-footer">
	            <div class="pull-left">
	              <a href="{{ URL::route('profile.show', ['id' => Auth::id()]) }}" class="btn btn-default btn-flat">Profile</a>
	            </div>
	            <div class="pull-right">
	            @if(config('administrator.logout_path'))
	            	<a href="{{url(config('administrator.logout_path'))}}" class="btn btn-default btn-flat btn-logout">{{trans('administrator::administrator.logout')}}</a>
	            @endif
	            </div>
	          </li>
	        </ul>
	      </li>
	      <!-- Control Sidebar Toggle Button -->
	      <li>
	        <a href="#" data-toggle="control-sidebar"><i class="fa fa-filter"></i></a>
	      </li>
	    </ul>
	  </div>

	</nav>
</header>