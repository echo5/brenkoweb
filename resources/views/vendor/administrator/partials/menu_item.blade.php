@if (is_array($item))
	<li class="menu treeview menu-item-{{ str_slug($key) }} @foreach ($item as $k => $subitem) @if (route('admin_index', array($k)) == Request::url()) active @endif  @endforeach">
		<a href="#">{{$key}} <i class="fa fa-angle-left pull-right"></i></a>
		<ul class="treeview-menu">
			@foreach ($item as $k => $subitem)
				<?php echo view("administrator::partials.menu_item", array(
					'item' => $subitem,
					'key' => $k,
					'settingsPrefix' => $settingsPrefix,
					'pagePrefix' => $pagePrefix,
					'subMenuItem' => true,
				))?>
			@endforeach
		</ul>
	</li>
@else
	<li class="item menu-item-{{ str_slug($key) }} @if(route('admin_index', array($key)) == Request::url()) active @endif">
		@if (strpos($key, $settingsPrefix) === 0)
			<a href="{{route('admin_settings', array(substr($key, strlen($settingsPrefix))))}}">
				@if (isset($subMenuItem))
					<i class="fa fa-circle-o"></i>
				@endif
				{{$item}}
			</a>
		@elseif (strpos($key, $pagePrefix) === 0)
			<a href="{{route('admin_page', array(substr($key, strlen($pagePrefix))))}}">
				@if (isset($subMenuItem))
					<i class="fa fa-circle-o"></i>
				@endif
				{{$item}}
			</a>
		@else
			<a href="{{route('admin_index', array($key))}}">
				@if (isset($subMenuItem))
					<i class="fa fa-circle-o"></i>
				@endif
				{{$item}}
			</a>
		@endif
	</li>
@endif
