@extends('layouts.app')
@section('meta-description') Web Universe, by Brenkoweb @stop
@section('title') Home - @parent @stop

@section('body-class') error-404 @stop

@section('page-header')

<div class="page-header page-header-with-image page-header-404">
	<div class="container">
        <h1>404</h1>
        <h2>Page Not Found</h2>
        <p>Sorry, we couldn't find the page you're looking for.  Please try going back or contact an administrator if you think there is a problem.</p>
    </div>
</div>

@endsection

@section('content')


@endsection
