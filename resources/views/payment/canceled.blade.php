@extends('layouts.app')

{{-- Web site Title --}}
@section('title') Payment Canceled - @parent @stop

{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="row">
        	<div class="col-md-6 col-md-push-3 panel panel-default panel-payment-canceled">
        	    <div class="panel-heading">
        	        <h1 class="h3">Payment Canceled</h1>
        	    </div>
        		<div class="panel-body">
					@include('partials.messages')
					<div class="row">
	        			<div class="col-xs-2">
		        			<i class="icon icon_minus_alt2"></i>
	        			</div>
	        			<div class="col-xs-10">
		    				<p>Your payment has canceled.  Please try again or contact an administrator for help.</p>  
	    				</div>
    				</div>
        		</div>
        	</div>
        </div>
    </div>
@endsection