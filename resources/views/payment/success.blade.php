@extends('layouts.app')

{{-- Web site Title --}}
@section('title') Payment Successful - @parent @stop

{{-- Content --}}
@section('content')

    <div class="container-fluid">
        <div class="row">
        	<div class="col-md-6 col-md-push-3 panel panel-default panel-payment-success">
        	    <div class="panel-heading">
        	        <h1 class="h3">Payment Successful</h1>
        	    </div>
        		<div class="panel-body">
					@include('partials.messages')
					<div class="row">
	        			<div class="col-xs-2">
		        			<i class="icon icon_check_alt2"></i>
	        			</div>
	        			<div class="col-xs-10">
		    				<p>Thank you for your order!  Your order number is <strong>#{{ $order->id }}</strong>.  Please keep this in a safe place for reference.</p>  
	    				</div>
    				</div>
        		</div>
        	</div>
        </div>
    </div>
@endsection