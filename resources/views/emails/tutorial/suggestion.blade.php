@extends('emails.base')

@section('content')

	<p><strong>User ID:</strong> {{ $user->id }}</p>
	<p><strong>User Email:</strong> {{ $user->email }}</p>
	<p><strong>Title:</strong> {{ $data['title'] }}</p>
	<p><strong>Description:</strong> {{ $data['description'] }}</p>
	@if ($data['zip_url'])
		<p><strong>Zip File:</strong> {{ $data['zip_url'] }}</p>
	@else
		<p>No zip file included.</p>
	@endif
@endsection