@extends('layouts.app')

@if ($currentCategory)
	@section('meta-description'){{ $currentCategory->meta_description }}@stop
@else
	@section('meta-description') BrankoWeb's Marketplace is involved in and providing various platform templates, such as Wordpress, Joomla, Drupal, Magento, and other frameworks used to create websites. BrenkoWeb's Marketplace is also providing many scripts and codes to purchase or download for free, for instance JavaScript, Pyhton and PHP scripting, or tools, etc... @stop
@endif

@section('meta-keywords') Joomla Marketplace, Wordpress Marketplace, Magento Marketplace, Drupal Marketplace, Scripts and Templates to purchase, Scripts and Templates to sell, Programming scripts and coding for download @stop

@section('title') @if ($currentCategory) {{ $currentCategory->name}} @else Joomla Marketplace, Wordpress Marketplace, ... @endif - @parent @stop

@section('page-header')

<div class="page-header page-header-scripts page-header-with-image invert-section">
	<div class="row">
		<div class="col-md-10 col-md-push-1">
			<h1>@if ($currentCategory) {{ $currentCategory->name }} @else Marketplace @endif</h1>
           	 <!--<a href="#">
				<div id="scroll-down" class="icon arrow_down scroll-down" style="position:absolute;top:0;left:0;width:60px;height:60px;line-height:60px;"></div>
        	</a>-->
			@if (count($currentCategories))
  				<div class="row categories-grid">
					<?php $n = 1; ?>
					@foreach ($currentCategories as $category)
						<div class="col-md-3 col-sm-6"> 
							<div class="category">
								<i class="icon @if ($category->icon) {{ $category->icon }} @else icon_documents_alt @endif"></i>
								{{-- <h4><a href="{{ $category->getUrl() }}">{{ $category->name }} ({{ $category->scripts->where('status', 'approved')->countTotal() }})</a></h4> --}}
								<h4><a href="{{ $category->getUrl() }}">{{ $category->name }} ({{ $category->totalScripts() }})</a></h4>
								{{-- <p>{{ $category->description }}</p> --}}
							</div>
						</div>
						@if ($n%4 == 0) <div class="clearfix"></div> @endif
						<?php $n++; ?>
					@endforeach				</div>
			@endif
		</div>
		<a href="#">
    		<div id="scroll-down" class="icon arrow_down scroll-down"></div>
    	</a>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-9 pull-right-md">
		<div class="row">
			<div class="col-md-8">
				@if (count($currentScripts))
					<div class="row latest-scripts container-xs">
						<div class="col-md-12">
							<div class="section-header no-margin-top">
								<div class="h3">@if ($currentCategory) {{ $currentCategory->name }} @else Latest Uploads @endif</div>
							</div>	
							<div class="row">
								<?php $n = 1; ?>
								@foreach($currentScripts as $script)
									<div class="col-md-4 col-sm-6">
										@include('scripts.partials.grid-item')
									</div>
									@if ($n%3 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
									@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
									<?php $n++; ?>
								@endforeach	
							</div>		
						</div>
						<div class="col-md-12">
							{!! $currentScripts->render() !!}
						</div>
					</div>
				@endif
			</div>
			<div class="col-md-4">
				@include('scripts.partials.sidebar-right')
			</div>
		</div>
	</div>

	<div class="col-md-3">
		@include('scripts.partials.sidebar')
	</div>


@stop