@extends('layouts.app')
{{-- Web site Title --}}
@section('title'){!!  $script->name !!} - @parent @stop

@section('body-id') script-{{ $script->id }} @stop
@section('body-class') script-single @stop

@section('content')

	<div class="col-md-12">
		@include('partials.messages')
	</div>

	<div class="col-md-9 pull-right-md">

		@include('modules.social-share', ['image' => config('assets.scriptImagesDirectory') . $script->image, 'description' => $script->name])
		<div class="row">
			<div class="page-header clearfix">
				<div class="col-md-7">
					<h1>{{ $script->name }}</h1>
					@if ($script->getRoundRating())
						@include('scripts.partials.stars', ['rating' => $script->getRoundRating()])
					@endif
				</div>

				<div class="script-stats pull-right-md col-md-3 col-xs-9">
					<div class="script-downloads col-xs-6">
						<i class="icon icon_cloud-download_alt"></i>
						<span class="h4 script-stat-count">{{ $script->downloads  }}</span>
					</div>
					<div class="script-views col-xs-6">
						<i class="icon icon_datareport"></i>
						<span class="h4 script-stat-count">{{ $script->views  }}</span>
					</div>
				</div>
			</div>
		</div>



		<div class="row script-details-container">
			<div class="col-md-5">
        		<img src="{{ config('assets.scriptImagesDirectory') . $script->image }}">
			</div>
			<div class="col-md-7">
				<div class="script-description">
					{!! $script->description !!}
				</div>
				<div class="script-details">
					@if ($script->price > 0)
						<div class="script-detail">Price: <span class="script-price">${{ $script->price }}</span></div>
					@endif
					<div class="script-detail">Version: {{ $script->version }}</div>
					<div class="script-detail">Category: {{ $script->category->name }}</div>
					<div class="script-detail">Submitted On: {{ date('F', strtotime($script->created_at)) }} {{ date('jS', strtotime($script->created_at)) }}, {{ date('Y', strtotime($script->created_at)) }}</div>
					<div class="script-detail">Last Updated: {{ date('F', strtotime($script->updated_at)) }} {{ date('jS', strtotime($script->updated_at)) }}, {{ date('Y', strtotime($script->updated_at)) }}</div>
					<div class="script-detail">Author: {{ $script->user->name }}</div>
				</div>


				<div class="row">
					<div class="col-md-6 pull-right-md">
						@if ($script->price > 0 && !$purchased)
							{{-- <h3 class="script-price pull-right">${{ $script->price }}</h3> --}}
							<a href="{{ URL::route('script.purchase', ['id' => $script->id]) }}" class="btn btn-primary">Purchase</a>
						@else
							<a href="{{ URL::route('script.download', ['id' => $script->id]) }}" class="btn btn-primary">Download</a>
						@endif
					</div>
					<div class="col-md-6 pull-right-md">
						@if ($script->preview_link)
							<a href="{{ $script->preview_link }}" class="btn btn-default" target="_blank">Live Preview</a>
						@endif
					</div>
				</div>

			</div>
		</div>

		<div class="row script-features-container">
			<div class="col-md-12">
				<div class="section-header">
					<h3>Features</h3>
				</div>
				<div class="script-features">{!! $script->features !!}</div>
			</div>

		</div>

		<div class="row">			
			<div class="col-md-12">
				@include('banners.bottom')
			</div>
		</div>

		<div class="row script-requirements-container">
			<div class="col-md-6">
				<div class="section-header">
					<h3>Requirements</h3>
				</div>
				<div class="script-requirements">{!! $script->requirements !!}</div>
			</div>
			<div class="col-md-6">
				<div class="section-header">
					<h3>Related Scripts</h3>
				</div>
					@if (count($relatedScripts))
						<div class="related-scripts">
							<div class="row">
								@foreach ($relatedScripts as $relatedScript)
									<div class="col-md-6">
										<div class="related-script" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45),rgba(0, 0, 0, 0.45)), url({{ config('assets.scriptImagesDirectory') . $script->image }});">
											<div class="related-script-name h4">{{ $relatedScript->name }}</div>
											<div class="related-script-link"><a href="{{ URL::route('script.show', ['id' => $relatedScript->id]) }}" class="btn btn-default">View</a></div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					@endif
			</div>
		</div>

		@include('scripts.partials.reviews')

	</div>

	<div class="col-md-3">
		@include('scripts.partials.sidebar')
	</div>


@stop