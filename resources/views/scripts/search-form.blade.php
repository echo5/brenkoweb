{!! Form::open(array('url' => URL::route('script.search'), 'method' => 'get', 'class' => 'marketplace-search-form')) !!}
    <?php 
    $category_array = array();
    foreach ($scriptCategories as $category) {
    	$category_array[''] = 'Choose category';
    	$category_parent_array = array($category->id);
		if (count($category->children)) {
			foreach ($category->children as $child) {
				$category_parent_array[] = $child->id;
			}
		}
    	if (!empty($category_parent_array)) {
    		$category_id = json_encode($category_parent_array);
    	} else {
    		$category_id = $category->id;
    	}
		$category_array[$category_id] = $category->name;
		if (count($category->children)) {
			foreach ($category->children as $child) {
				$category_array[$child->id] = '-- ' . $child->name;
			}
		}
    }
    ?>
    <div class="row">
    	<div class="col-md-5">
        	<div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
        	    <div class="controls">
        	        {!! Form::text('name', (isset($_GET['name']) ? $_GET['name'] : null), array('class' => 'form-control', 'placeholder' => 'Keywords')) !!}
        	        <span class="help-block">{{ $errors->first('name', ':message') }}</span>
        	    </div>
        	</div>
    	</div>
    	<div class="col-md-3">
        	<div class="form-group  {{ $errors->has('category_id') ? 'has-error' : '' }}">
        	    <div class="controls">
        	        {!! Form::select('category_id', $category_array, '1', array('class' => 'form-control')) !!}
        	        <span class="help-block">{{ $errors->first('category_id', ':message') }}</span>
        	    </div>
        	</div>
    	</div>
    	<div class="col-md-2">
        	<div class="form-group  {{ $errors->has('max_price') ? 'has-error' : '' }}">
        	    <div class="controls">
        	        {!! Form::text('max_price', (isset($_GET['max_price']) ? $_GET['max_price'] : null), array('class' => 'form-control', 'placeholder' => 'Max Price')) !!}
        	        <span class="help-block">{{ $errors->first('max_price', ':message') }}</span>
        	    </div>
        	</div>
    	</div>
    	<div class="col-md-2">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    Search
                </button>
            </div>
    	</div>
    </div>


{!! Form::close() !!}