@extends('layouts.app')
{{-- Web site Title --}}
@section('title') Submit a Script - @parent @stop

@section('styles')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
@endsection

@section('body-class') script-create @stop

@section('page-header')
	@include('partials.messages')
@stop

{{-- Content --}}
@section('content')

	<div class="col-md-3">
		@include('posts.partials.sidebar')
	</div>


    <div class="col-md-9">
        <div class="page-header">
            <h1>Submit Software</h1>
        </div>
    	<div class="row">
    		<div class="col-md-12">
	            {!! Form::open(array('url' => URL::route('script.store'), 'method' => 'post', 'files' => true)) !!}
		            <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
		                {!! Form::label('name', 'Name', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('image') ? 'has-error' : '' }}">
		                {!! Form::label('image', 'Image', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::file('image', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('image', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('file') ? 'has-error' : '' }}">
		                {!! Form::label('file', 'File', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::file('file', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('file', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('version') ? 'has-error' : '' }}">
		                {!! Form::label('version', 'Version', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::text('version', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('version', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('preview_link') ? 'has-error' : '' }}">
		                {!! Form::label('preview_link', 'Preview Link', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::text('preview_link', 'http://', array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('preview_link', ':message') }}</span>
		                </div>
		            </div>
		            <?php 
		            $category_array = array();
		            foreach ($scriptCategories as $category) {
	            		$category_array[$category->id] = $category->name;
	            		if (count($category->children)) {
            				foreach ($category->children as $child) {
            					$category_array[$child->id] = '-- ' . $child->name;
            				}
	            		}
		            }
		            ?>
		            <div class="form-group  {{ $errors->has('category_id') ? 'has-error' : '' }}">
		                {!! Form::label('category_id', 'Category', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::select('category_id', $category_array, null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('category_id', ':message') }}</span>
		                </div>
		            </div>
                    <div>
                    	<p><span style="color:red">Note</span>: Use <strong>Description</strong> field's <strong>first line</strong> for category suggestions.</p>
                    </div>
		            <div class="form-group  {{ $errors->has('description') ? 'has-error' : '' }}">
		                {!! Form::label('description', 'Description', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::textarea('description', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('description', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('features') ? 'has-error' : '' }}">
		                {!! Form::label('features', 'Features', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::textarea('features', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('features', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('requirements') ? 'has-error' : '' }}">
		                {!! Form::label('requirements', 'Requirements', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::textarea('requirements', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('requirements', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('price') ? 'has-error' : '' }}">
		                {!! Form::label('price', 'Price', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::text('price', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">Enter 0 for free scripts.</span>
		                    <span class="help-block">{{ $errors->first('price', ':message') }}</span>
		                </div>
		            </div>
                     <div>
                    	<h4>Attention: Read the <strong><a href="http://brenkoweb.com/terms-of-service#tos_product">Terms of Service</a></strong> before submitting.</h4>
                    </div>
		            <div class="form-group">
	                    <button type="submit" class="btn btn-primary">
	                        Submit For Review
	                    </button>
		            </div>
	            {!! Form::close() !!}
    		</div>
        </div>
    </div>
    
@endsection

@section('scripts')
	<script>
	    CKEDITOR.replace( 'description' );
	    CKEDITOR.replace( 'features' );
	    CKEDITOR.replace( 'requirements' );
	</script>
@endsection