@extends('layouts.app')

@if ($currentCategory)
	@section('meta-description'){{ $currentCategory->meta_description }}@stop
@else
	@section('meta-description')Scripts by authors of Brenko Web. @stop
@endif

@section('title') @if ($currentCategory) {{ $currentCategory->name}} @else Marketplace @endif - @parent @stop

@section('page-header')

<div class="page-header page-header-scripts page-header-with-image invert-section">
	<div class="row">
		<div class="col-md-8 col-md-push-2">
			<h1>Search Marketplace</h1>
			@include('scripts.search-form')
		</div>
	</div>
</div>

@endsection

@section('content')

	<div class="col-md-9 pull-right-md">
		<div class="row">
			<div class="col-md-8">
				@if (count($scripts))
					<div class="row latest-scripts container-xs">
						<div class="col-md-12">
							<div class="row">
								<?php $n = 1; ?>
								@foreach($scripts as $script)
									<div class="col-md-4 col-sm-6">
										@include('scripts.partials.grid-item')
									</div>
									@if ($n%3 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
									@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
									<?php $n++; ?>
								@endforeach	
							</div>		
						</div>
					</div>
				@elseif(isset($scripts))
					<div class="alert alert-info">Sorry, no scripts matched your search.</div>
				@else

				@endif
			</div>
			<div class="col-md-4">
				@include('scripts.partials.sidebar-right')
			</div>
		</div>
	</div>

	<div class="col-md-3">
		@include('scripts.partials.sidebar')
	</div>


@stop