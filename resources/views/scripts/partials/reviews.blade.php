<div class="reviews-section">
	<div class="row">
		<div class="col-md-8">
			<div class="section-header">
				<h3>Reviews</h3>
			</div>

			@if (count($script->reviews))
				<div class="rating-avg">
					@include('scripts.partials.stars', ['rating' => $script->getAvgRating()]) 
					<span class="rating-text">{{ $script->getAvgRating() }} based on {{ count($script->reviews) }} ratings.</span>
				</div>
				<div class="reviews">
					@foreach ($script->reviews as $review)
						@include('scripts.partials.review')
					@endforeach
				</div>

			@else
				<p>No reviews have been left yet for this script.</p>
			@endif
			
				@if (!($script->price > 0) || $purchased)
					<div class="row">
						<div id="review-form" class="review-form">
						    {!! Form::open(array('url' => Request::url(), 'method' => 'post')) !!}
							    {!! Form::hidden('script_id', $script->id) !!}
							    <div class="col-xs-2">
							    	<div class="review-user-avatar">
							    		@if (Auth::check())
							    			<img src="{{ Auth::user()->getAvatar() }}">
						    			@else
							    			<img src="{{ asset('img/user-default.png') }}">
						    			@endif
							    	</div>
							    </div>
							    <div class="col-xs-10">
							        <div class="form-group {{ $errors->has('rating') ? 'has-error' : '' }}"> 
							            {!! Form::label('rating', 'Rating', array('class' => 'control-label')) !!}
							        	<div class="controls">
				            	    		@if (Auth::check())
									            {!! Form::select('rating', array('1' => '1 - Poor', '2' => '2 - Below Average', '3' => '3 - Average', '4' => '4 - Good', '5' => '5 - Excellent'), '5', array('class' => 'form-control')) !!}
								            @else
									            {!! Form::select('rating', array('1' => '1 - Poor', '2' => '2 - Below Average', '3' => '3 - Average', '4' => '4 - Good', '5' => '5 - Excellent'), '5', array('class' => 'form-control disabled', 'disabled' => true)) !!}
							            	@endif
							        	</div>
							        </div>
							        <div class="form-group  {{ $errors->has('review') ? 'has-error' : '' }}">
							            {{-- {!! Form::label('review', 'Leave a Review', array('class' => 'control-label')) !!} --}}
							            <div class="controls">
				            	    		@if (Auth::check())
								                {!! Form::textarea('review', null, array('class' => 'form-control', 'placeholder' => 'Leave a Review')) !!}
							                @else
								                {!! Form::textarea('review', null, array('class' => 'form-control disabled', 'placeholder' => 'Please login to leave a review' , 'disabled' => true)) !!}
				                			@endif
							                <span class="help-block">{{ $errors->first('review', ':message') }}</span>
							            </div>
							        </div>
							    </div>
						        <div class="col-md-12">
							        <div class="form-group">
			            	    		@if (Auth::check())
							                <button type="submit" class="btn btn-primary pull-right">Post review</button>
						                @else
							                <button type="submit" class="btn btn-primary pull-right disabled" onclick="return false;">Post review</button>
						                @endif
							        </div>        	
						        </div>
						    {!! Form::close() !!}
						</div>
					</div>
				@else
					<div class="alert alert-info">You must be logged in and purchase this script in order to leave a review.</div>
				@endif


		</div>
		<div class="col-md-4">
			@include('banners.content')
		</div>
	</div>
</div>