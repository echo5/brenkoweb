<div class="sidebar-item sidebar-add-listing sidebar-no-title">
	<h4>Have a script or a template?</h4>
	<a href="{{ URL::route('script.create') }}" class="btn btn-default">Upload Software</a>
</div>
@include('modules.sidebar_script-search')
@include('modules.sidebar_popular-scripts')
@include('banners.content')