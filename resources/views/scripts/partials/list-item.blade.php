<div class="script">
	<div class="row">					
		<div class="col-md-3">
			<div class="scripts-img-container">
            		<a href="{{ URL::route('script.show', ['id' => $script->id]) }}">
            		<img src="{{ config('assets.scriptImageThumbsDirectory') . $script->image }}">
    			</a>
			</div>
		</div>
		<div class="col-md-9">
			@include('scripts.partials.stars', ['rating' => $script->getRoundRating()])
			<h3><a href="{{ URL::route('script.show', ['id' => $script->id]) }}">{{ $script->name }}</a></h3>
			<p>{!! $script->description !!}</p>
			<div>
				<a href="{{ URL::route('script.download', ['id' => $script->id]) }}" class="btn btn-default btn-sm">Download</a>
				<a href="{{ URL::route('script.show', ['id' => $script->id]) }}" class="btn btn-primary btn-sm">Details</a>
			</div>
		</div>
	</div>
</div>