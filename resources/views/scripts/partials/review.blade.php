<div id="review-{{ $review->id }}" class="review">
	<div class="row">
		<div class="col-xs-2">
			<div class="review-user-avatar">
				<img src="{{ $review->user->getAvatar() }}">
			</div>
		</div>
		<div class="col-xs-10">
			<div class="clearfix"></div>
			<div class="review-rating">
				@include('scripts.partials.stars', ['rating' => $review->rating])
			</div>
			<div class="review-title h4">{{ $review->title }}</div>
			<div class="review-content">{{ $review->review }}</div>
			<div class="reviewd-by h6">
				Review by <span class="review-user"><a href="{{ URL::route('profile.show', ['id' => $review->user->id]) }}">{{ $review->user->name }}</a></span><span class="review-date">on {{ date('F', strtotime($review->created_at)) }} {{ date('jS', strtotime($review->created_at)) }}, {{ date('Y', strtotime($review->created_at)) }}</span>
			</div>
		</div>
	</div>
</div>