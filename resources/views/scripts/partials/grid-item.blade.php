<div class="script">
	<div class="row">					
		<div class="col-md-12">
			<div class="script-img-container">
            	<img src="{{ config('assets.scriptImageThumbsDirectory') . $script->image }}">
                <div class="script-img-overlay">
                    <div class="script-links">
                    	@if ($script->preview_link)
	                        <a href="{{ $script->preview_link }}" class="btn btn-default btn-sm" target="_blank">Preview</a>
                        @endif
                        @if (!isset($pending))
                        	<a href="{{ URL::route('script.show', ['id' => $script->id]) }}" class="btn btn-primary btn-sm">Details</a>
                    	@endif
                    </div>
                </div>
			</div>
		</div>
		<div class="col-md-12">
            <h4>@if (!isset($pending))<a href="{{ URL::route('script.show', ['id' => $script->id]) }}">@endif{{ $script->name }}@if (!isset($pending))</a>@endif</h4>
            <div class="script-rating-container">
                @if ($script->getRoundRating())
        			@include('scripts.partials.stars', ['rating' => $script->getRoundRating()])
                    <span class="script-ratings h5">{{ $script->reviews->count() }} Ratings</span>
                @endif
            </div>
            <div class="script-category h5">
                {!! ($script->category->parent) ? $script->category->parent->name . ' / ' : '' !!}{{ $script->category->name }}
            </div>
			<!-- <p>{!! $script->description !!}</p> -->
		</div>
	</div>
</div>