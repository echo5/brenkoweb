<div class="script-rating">
<?php
	if (isset($rating)) {
		if ($rating > 0) {
		    for ($x = 1; $x <= $rating; $x++) {
		        echo '<i class="icon icon_star"></i>';
		    }
		    if (strpos($rating,'.')) {
		        echo '<i class="icon icon_star-half_alt"></i>';
		        $x++;
		    }
		    while ($x <= 5) {
		        echo '<i class="icon icon_star_alt"></i>';
		        $x++;
		    }
		}
	}
?>
</div>