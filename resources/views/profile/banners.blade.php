@extends('layouts.app')
{{-- Web site Title --}}
@section('title') {!!  $user->name !!}'s Banners - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>{{ $user->name }}'s Banners</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@if (count($user->banners))
			<div class="row latest-banners">
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Active Banners</div>
					</div>
					@foreach ($user->banners as $banner)
						@if ($banner->status == 'approved')
							@include('profile.banner.partials.list-item')
						@endif
					@endforeach			
				</div>
			</div>
		@endif

		@if (count($user->banners))
			<div class="row pending-banners">
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Pending Banners</div>
					</div>
					@foreach ($user->banners as $banner)
						@if (!$banner->status == 'pending')
							@include('profile.banner.partials.list-item')
						@endif
					@endforeach			
				</div>
			</div>
		@endif

	</div>

@stop