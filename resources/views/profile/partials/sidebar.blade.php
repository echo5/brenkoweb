<div class="user-info-box">
	<div class="user-info-avatar">
		<img src="{{ $user->getAvatar(200) }}" class="user-avatar" alt="{{ $user->name . ' avatar' }}">
	</div>
	<div class="row">	
		<div class="col-md-12">
			<div class="user-info h3">{{ $user->name }}</div>
		</div>
	</div>

	<div class="section-header">
		<h4>Basic Info</h4>
		@if (Auth::id() == $user->id)
			<a href="{{ URL::route('profile.edit') }}" class="edit-profile-link">Edit Profile</a>
		@endif
	</div>
	@if ($user->username)
		<div class="row">	
			<div class="col-md-12">
				<div class="user-info">{{ '@' . $user->username }}</div>
			</div>
		</div>
	@endif
	@if ($user->website)
		<div class="row">	
			<div class="col-md-12">
				<div class="user-info"><a href="{{ $user->website }}" target="_blank">{{ $user->website }}</a></div>
			</div>
		</div>
	@endif
	<div class="row">
		<div class="col-md-12">
			<div class="user-info">Member since {{ date('F', strtotime($user->created_at)) }} {{ date('jS', strtotime($user->created_at)) }}, {{ date('Y', strtotime($user->created_at)) }}</div>
		</div>
	</div>
</div>
<div class="sidebar-item">
	<div class="sidebar-item-title h4">User Menu</div>
	<ul class="nav nav-pills nav-stacked nav-user">
		<li>
			<a href="{{ URL::route('profile.show', ['id' => $user->id]) }}">
				<i class="icon icon_cogs"></i>
				Profile
			</a>
		</li>
		<li>
			<a href="{{ URL::route('profile.articles', ['id' => $user->id]) }}">
				<i class="icon icon_paperclip"></i>
				Articles
			</a>
		</li>
		<li>
			<a href="{{ URL::route('profile.scripts', ['id' => $user->id]) }}">
				<i class="icon icon_tools"></i>
				Scripts
			</a>
		</li>
		<li>
			<a href="{{ URL::route('profile.listings', ['id' => $user->id]) }}">
				<i class="icon icon_search-2"></i>
				Listings
			</a>
		</li>
		<li>
			<a href="{{ URL::route('profile.reviews', ['id' => $user->id]) }}">
				<i class="icon icon_star_alt"></i>
				Reviews
			</a>
		</li>
		@if (Auth::id() == $user->id)
			<li>
				<a href="{{ URL::route('profile.banners', ['id' => $user->id]) }}">
					<i class="icon icon_datareport"></i>
					Banners
				</a>
			</li>	
			<li>
				<a href="{{ URL::route('profile.orders') }}">
					<i class="icon icon_cart_alt"></i>
					Orders
				</a>
			</li>	
			<li>
				<a href="{{ URL::route('profile.downloads') }}">
					<i class="icon icon_cloud-download_alt"></i>
					Downloads
				</a>
			</li>	
		@endif
	</ul>
</div>