@extends('layouts.app')
{{-- Web site Title --}}
@section('title') Your Downloads - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>Your Downloads</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@if (count($scriptPurchases))
			<div class="row latest-downloads">
				<div class="col-md-12">
					@foreach ($scriptPurchases as $script)
						{{-- @if ($script->status == 'approved') --}}
							@include('scripts.partials.list-item')
						{{-- @endif --}}
					@endforeach			
				</div>
			</div>
		@else
			<div class="row">
				<div class="alert alert-info">
					You haven't made any script purchases yet.
				</div>
			</div>
		@endif

	</div>

@stop