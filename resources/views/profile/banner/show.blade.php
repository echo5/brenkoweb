@extends('layouts.app')
{{-- Web site Title --}}
@section('title') {!!  $user->name !!}'s Banners - @parent @stop

@section('styles')
	<script src="{{ asset('js/jquery.flot.min.js') }}"></script>
	<script src="{{ asset('js/jquery.flot.time.min.js') }}"></script>
	<script src="{{ asset('js/jquery.flot.selection.min.js') }}"></script>
@endsection

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>Banner Stats</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@if ($banner)
			<div class="row banners-stats-container">
				<div class="col-md-12">
					@if (!$banner->hasCredit())
						<div class="alert alert-danger">
							You have no impressions or days remaining left on this ad.  Would you like to <a href="{{ route('profile.banner.addcredit', ['id' => $banner->id] ) }}">purchase more</a> now?
						</div>
					@elseif ($banner->lowCredit())
						<div class="alert alert-warning">
							You only have @if ($banner->daysRemaining() > 0) {{ $banner->daysRemaining() }} days @else {{ $banner->impressionsRemaining() }} impressions @endif left on this ad.  Would you like to <a href="{{ route('profile.banner.addcredit', ['id' => $banner->id] ) }}">purchase more</a> now?
						</div>
					@endif
				</div>
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Banner ID: {{$banner->id}}</div> 
						<a href="{{ route('profile.banner.addcredit', ['id' => $banner->id] ) }}" class="btn btn-primary btn-add-credit">Add Credit</a>
					</div>

					<div class="banner-stats">
						<div class="row">
							<div class="col-md-3">
								<div class="info-box info-box-top">
									<i class="icon icon_images"></i>
									<div class="info-box-info h2">{{ $total_impressions }}</div>
									<div class="info-box-label h4">Total Impressions</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="info-box info-box-top">
									<i class="icon icon_cursor_alt"></i>
									<div class="info-box-info h2">{{ $total_clicks }}</div>
									<div class="info-box-label h4">Clicks</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="info-box info-box-top">
									<i class="icon icon_piechart"></i>
									<div class="info-box-info h2">{{ round($total_clicks / $total_impressions * 100, 2) }}%</div>
									<div class="info-box-label h4">CTR</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="info-box info-box-top">
										@if ($banner->daysRemaining() > 0)
											<i class="icon icon_calendar"></i>
											<div class="info-box-info h2">{{ $banner->daysRemaining() }}</div>
											<div class="info-box-label h4">Days Remaining</div>
										@elseif ($banner->impressionsRemaining() > 0)
											<i class="icon icon_clock_alt"></i>
											<div class="info-box-info h2">{{ $banner->impressionsRemaining() }}</div>
											<div class="info-box-label h4">Impressions Remaining</div>
										@else
											<i class="icon icon_refresh"></i>
											<div class="info-box-info h2">0</div>
											<div class="info-box-label h4">Days/Impressions Left</div>
										@endif
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div id="banner-stats-chart" style="width:100%;height:300px;"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id="banner-stats-overview" style="height: 100px;"></div>
							</div>
						</div>

						<div class="row banner-info-container">
							<div class="col-md-12">
								<div class="section-header">
									<h4>Banner Info</h4>
								</div>
							</div>
							<div class="col-md-6">
								@if ($banner->caption)
									<div class="banner-caption"><strong>Banner Caption:</strong> {{ $banner->caption }}</div>
								@endif
								<div class="banner-link"><strong>Banner Link:</strong> {{ $banner->link }}</div>
								<div class="banner-date"><strong>Submitted on:</strong> {{ date('F', strtotime($banner->created_at)) }} {{ date('jS', strtotime($banner->created_at)) }}, {{ date('Y', strtotime($banner->created_at)) }}</div>
								<div class="banner-location"><strong>Location:</strong> {{ $banner->location }}</div>
							</div>
							<div class="col-md-6">
								<div class="banner-img-container pull-right">
					        		<a href="{{ $banner->link }}" class="">
					        			{!! $banner->script !!}
					            		{{-- <img src="{{ config('assets.bannerImageThumbsDirectory') . 'small/' . $banner->image }}"> --}}
					    			</a>
								</div>
							</div>
						</div>

					</div>	
				</div>
			</div>
		@endif

	</div>

@stop

@section('scripts')
	<script>

		var dataImpressions = [
		@foreach ($banner->stats as $stat)
			[{{ strtotime( date('Y', strtotime($stat->date)) . '-' . date('m', strtotime($stat->date)) . '-' . '01') * 1000 }}, {{ $stat->impressions }}],
		@endforeach
		];

		var dataClicks = [
		@foreach ($banner->stats as $stat)
			[{{ strtotime( date('Y', strtotime($stat->date)) . '-' . date('m', strtotime($stat->date)) . '-' . '01') * 1000 }}, {{ $stat->clicks }}],
		@endforeach
		];

		var options = {
			xaxis: { 
				mode: "time",
				tickLength: 0,
				timeformat: "%b %Y"
			},
			yaxis: {
				tickLength: 0,
			},
			grid: {
				hoverable: true,
				clickable: true
			},
			// lines: { show: true, fill: true },
		};

		var plot = $.plot($("#banner-stats-chart"),[
				{ data: dataImpressions, label: "Impressions" },
				{ data: dataClicks, label: "Clicks"}
			],
			options);
		
		var overview = $.plot("#banner-stats-overview", [dataImpressions], {
			series: {
				lines: {
					show: true,
					lineWidth: 1
				},
				shadowSize: 0
			},
			xaxis: {
				ticks: [],
				tickLength: 0,
				mode: "time"
			},
			yaxis: {
				ticks: [],
				tickLength: 0,
				min: 0,
				autoscaleMargin: 0.1
			},
			selection: {
				mode: "x"
			}
		});

		// Bind tooltips
		$("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #f7f7f7",
			padding: "10px",
			"background-color": "#fff",
			opacity: 1
		}).appendTo("body");
		$("#banner-stats-chart").bind("plothover", function (event, pos, item) {


			var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
			$("#hoverdata").text(str);
			
			if (item) {
				var x = item.datapoint[0],
					y = item.datapoint[1];

				$("#tooltip").html(y + " " + item.series.label)
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(200);
			} else {
				$("#tooltip").hide();
			}
			
		});

		// Bind selection chart
		$("#banner-stats-chart").bind("plotselected", function (event, ranges) {
			// do the zooming
			$.each(plot.getXAxes(), function(_, axis) {
				var opts = axis.options;
				opts.min = ranges.xaxis.from;
				opts.max = ranges.xaxis.to;
			});
			plot.setupGrid();
			plot.draw();
			plot.clearSelection();
			overview.setSelection(ranges, true);
		});
		$("#banner-stats-overview").bind("plotselected", function (event, ranges) {
			plot.setSelection(ranges);
		});


	</script>
@endsection