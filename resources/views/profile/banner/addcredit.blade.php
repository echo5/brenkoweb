@extends('layouts.app')
{{-- Web site Title --}}
@section('title') {!!  $user->name !!}'s Banners - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>Add Banner Credit</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@if ($banner)
			<div class="row banners-stats-container">
				<div class="col-md-12">
					<h3>{{ $banner->caption }}</h3>
					<h4>Banner ID: {{ $banner->id }}</h4>
					<br/>
				</div>
				<div class="col-md-12">
		            {!! Form::open(array('url' => URL::route('banner.addcredit'), 'method' => 'post')) !!}
	                    <div class="form-group  {{ $errors->has('plan_id') ? 'has-error' : '' }}">
	                        {!! Form::label('plan_id', 'Plan', array('class' => 'control-label')) !!}
	                        <div class="controls">
	                        	<div class="list-group">	
	                        		@foreach ($plans as $plan)
	                        			@if ($plan->public)
	                        				<div class="list-group-item plan">
	        	                				<div class="row">
	        	                					<div class="col-md-1">
	        											{!! Form::radio('plan_id', $plan->id, false, array('id' => $plan->id)) !!}
	        	                					</div>
	        	                					<div class="col-md-11">
	        											{!! Form::label($plan->id, $plan->name . ' - $' . $plan->cost, array('class' => 'list-group-item-heading h5')) !!}
	        											<div class="plan-description">
	        												{!! $plan->description !!}
	        											</div>
	        	                					</div>
	        	                				</div>
	                        				</div>
	        							@endif
	                        		@endforeach
	                        	</div>
	                            <span class="help-block">{{ $errors->first('plan_id', ':message') }}</span>
	                        </div>
	                    </div>
	                    {!! Form::hidden('banner_id', $banner->id, false, array('id' => $banner->id)) !!}
			            <div class="form-group">
		                    <button type="submit" class="btn btn-primary">
		                        Add Credit
		                    </button>
			            </div>
		            {!! Form::close() !!}
				</div>
			</div>
		@endif

	</div>

@stop

@section('scripts')
	<script>

		var dataImpressions = [
		@foreach ($banner->stats as $stat)
			[{{ strtotime( date('Y', strtotime($stat->date)) . '-' . date('m', strtotime($stat->date)) . '-' . '01') * 1000 }}, {{ $stat->impressions }}],
		@endforeach
		];

		var dataClicks = [
		@foreach ($banner->stats as $stat)
			[{{ strtotime( date('Y', strtotime($stat->date)) . '-' . date('m', strtotime($stat->date)) . '-' . '01') * 1000 }}, {{ $stat->clicks }}],
		@endforeach
		];

		var options = {
			xaxis: { 
				mode: "time",
				tickLength: 0,
				timeformat: "%b %Y"
			},
			yaxis: {
				tickLength: 0,
			},
			grid: {
				hoverable: true,
				clickable: true
			},
			// lines: { show: true, fill: true },
		};

		var plot = $.plot($("#banner-stats-chart"),[
				{ data: dataImpressions, label: "Impressions" },
				{ data: dataClicks, label: "Clicks"}
			],
			options);
		
		var overview = $.plot("#banner-stats-overview", [dataImpressions], {
			series: {
				lines: {
					show: true,
					lineWidth: 1
				},
				shadowSize: 0
			},
			xaxis: {
				ticks: [],
				tickLength: 0,
				mode: "time"
			},
			yaxis: {
				ticks: [],
				tickLength: 0,
				min: 0,
				autoscaleMargin: 0.1
			},
			selection: {
				mode: "x"
			}
		});

		// Bind tooltips
		$("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #f7f7f7",
			padding: "10px",
			"background-color": "#fff",
			opacity: 1
		}).appendTo("body");
		$("#banner-stats-chart").bind("plothover", function (event, pos, item) {


			var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
			$("#hoverdata").text(str);
			
			if (item) {
				var x = item.datapoint[0],
					y = item.datapoint[1];

				$("#tooltip").html(y + " " + item.series.label)
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(200);
			} else {
				$("#tooltip").hide();
			}
			
		});

		// Bind selection chart
		$("#banner-stats-chart").bind("plotselected", function (event, ranges) {
			// do the zooming
			$.each(plot.getXAxes(), function(_, axis) {
				var opts = axis.options;
				opts.min = ranges.xaxis.from;
				opts.max = ranges.xaxis.to;
			});
			plot.setupGrid();
			plot.draw();
			plot.clearSelection();
			overview.setSelection(ranges, true);
		});
		$("#banner-stats-overview").bind("plotselected", function (event, ranges) {
			plot.setSelection(ranges);
		});


	</script>
@endsection