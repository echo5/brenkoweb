<div class="banner-listing">
	<div class="row">
		<div class="col-md-3">
			<div class="banner-img-container">
        		<a href="{{ $banner->link }}" class="">
            		<img src="{{ config('assets.bannerImagesDirectory') . $banner->image }}">
    			</a>
			</div>
		</div>
		<div class="col-md-9">
			@if ($banner->caption)
				<div class="banner-caption"><strong>Banner Caption:</strong> {{ $banner->caption }}</div>
			@endif
			<div class="banner-link"><strong>Banner Link:</strong> {{ $banner->link }}</div>
			<div class="banner-date"><strong>Submitted on:</strong> {{ date('F', strtotime($banner->created_at)) }} {{ date('jS', strtotime($banner->created_at)) }}, {{ date('Y', strtotime($banner->created_at)) }}</div>
			<div class="banner-location"><strong>Location:</strong> {{ $banner->location }}</div>
			<a href="{{ URL::route('profile.banner.stats', ['banner_id' => $banner->id]) }}" class="btn btn-default">View Stats</a>
		</div>
	</div>
</div>