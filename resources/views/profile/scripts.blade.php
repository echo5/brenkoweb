@extends('layouts.app')
{{-- Web site Title --}}
@section('title') {!!  $user->name !!}'s Scripts - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>{{ $user->name }}'s Scripts</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-12">
		@include('partials.messages')
	</div>

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@if (count($user->scripts))
			<div class="row latest-scripts">
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Latest Scripts</div>
					</div>
					<div class="row">
					<?php $n = 1; ?>
						@foreach ($user->scripts as $script)
							@if ($script->status == 'approved')
								<div class="col-md-4 col-sm-6">
										@include('scripts.partials.grid-item')
								</div>
								@if ($n%3 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
								@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
								<?php $n++; ?>
							@endif
						@endforeach
					</div>		
				</div>
			</div>
		@endif

		@if (count($user->scripts))
			<div class="row pending-scripts">
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Pending Scripts</div>
					</div>
					<div class="row">
					<?php $n = 1; ?>
						@foreach ($user->scripts as $script)
							@if ($script->status == 'pending')
								<div class="col-md-4 col-sm-6">
										@include('scripts.partials.grid-item', ['pending' => true])
								</div>
								@if ($n%3 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
								@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
								<?php $n++; ?>
							@endif
						@endforeach	
					</div>		
				</div>
			</div>
		@endif



	</div>

@stop