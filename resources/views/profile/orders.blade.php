@extends('layouts.app')
{{-- Web site Title --}}
@section('title') Your Orders - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>Your Orders</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@if (count($orders))
			<div class="row latest-scripts">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Order History</div>
						<table class="table">
							<thead>
								<tr>
									<th>Order #</th>
									<th>Payment ID</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($orders as $order)
									<tr>
										<th scope="row"><a href="{{ URL::route('profile.order', ['id' => $order->id]) }}">{{ $order->id }}</a></th>
										<td>{{ $order->payment_id }}</td>
										<td>{{ $order->created_at }}</td>
									</tr>
								@endforeach
							</tbody>
					    </table>
					</div>
				</div>
			</div>
		@else
			<div class="row">
				<div class="alert alert-info">
					You haven't made any purchases yet.
				</div>
			</div>
		@endif

	</div>

@stop