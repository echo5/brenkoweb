@extends('layouts.app')
{{-- Web site Title --}}
@section('title') Order #{{ $order->id }} - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>Order #{{ $order->id }}</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		<div class="row ">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
	    				<div class="row">
	    			        <div class="panel-heading">
	    		        		<h3>Order #{{ $order->id }}</h3>
	    					</div>
	    				</div>
	    				<div class="row">
	    			        <div class="col-xs-12">
	    		        		<h4>Products</h4>
	    					</div>
	    				</div>
	    				<div class="row">
	    			        <div class="col-xs-12 table-responsive">
	    			          <table class="table table-striped">
	    			            <thead>
	    			            <tr>
	    			              <th>Type</th>
	    			              <th>Product</th>
	    			              <th>Product ID (plan or script ID)</th>
	    			              <th>Model ID</th>
	    			              <th>Subtotal</th>
	    			            </tr>
	    			            </thead>
	    			            <tbody>
	    			            @foreach ($orderItems as $item)
	    				            <tr>
	    				              <td>{{ $item->type }}</td>
	    				              <td>{{ $item->name }}</td>
	    				              <td>{{ $item->product_id }}</td>
	    				              <td>{{ $item->model_id }}</td>
	    				              <td>${{ $item->price }}</td>
	    				            </tr>
	    			            @endforeach
	    			            </tbody>
	    			          </table>
	    			        </div>
	    			        <!-- /.col -->
	    			    </div>

	    	    		<div class="row">
	    	    	        <div class="col-xs-12">
	    	            		<h4>Payment</h4>
	    	    			</div>
	    	    		</div>

	    				<div class="row">
	    			    	<div class="col-md-6">
	            				<div class="table-responsive">
	            		            <table class="table">
	            						<tbody>
	    		        		            	<tr>
	    		        		                	<th>Payment Method:</th>
	    		        		                	<td>{{ $order->payment_method }}</td>
	    		        		              	</tr>
	    		        		            	<tr>
	    		        		                	<th>Payer Name:</th>
	    		        		                	<td>{{ $order->payer_name }}</td>
	    		        		              	</tr>
	    		        		            	<tr>
	    		        		                	<th>Payer Email:</th>
	    		        		                	<td>{{ $order->payer_email }}</td>
	    		        		              	</tr>
	    		        		            	<tr>
	    		        		                	<th>Payer ID:</th>
	    		        		                	<td>{{ $order->payer_id }}</td>
	    		        		              	</tr>
	    		        		            	<tr>
	    		        		                	<th>Payment ID:</th>
	    		        		                	<td>{{ $order->payment_id }}</td>
	    		        		              	</tr>
	    		        		            	<tr>
	    		        		                	<th>Payment Status:</th>
	    		        		                	<td>{{ $order->payment_status }}</td>
	    		        		              	</tr>
	            		            	</tbody>
	        		            	</table>
	            	            </div>
	    		            </div>
	            	    	<div class="col-md-6">
	            				<div class="table-responsive">
	            		            <table class="table">
	            						<tbody>
	    		        		            	<tr>
	    		        		                	<th>Total:</th>
	    		        		                	<td>${{ $order->total() }}</td>
	    		        		              	</tr>
	            		            	</tbody>
	        		            	</table>
	            	            </div>
	                        </div>
	    	            </div>
    	            </div>
				</div>
			</div>
		</div>

	</div>

@stop