@extends('layouts.app')
{{-- Web site Title --}}
@section('title') {!!  $user->name !!}'s Reviews - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>{{ $user->name }}'s Reviews</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@if (count($user->scriptReviews))
			<div class="row latest-reviews">
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Latest Reviews</div>
					</div>
					@foreach ($user->scriptReviews as $review)
						{{-- @if ($review->approved) --}}
							<h5>Review of 
								@if ($review->script->approved)
									<a href="{{ URL::route('script.show', ['id' => $review->script->id]) }}">{{ $review->script->name }}</a>
								@else
									{{ $review->script->name }}
								@endif
							</h5>
							@include('scripts.partials.review')
						{{-- @endif --}}
					@endforeach			
				</div>
			</div>
		@else
			<div class="alert alert-info">No reviews have been left yet.</div>
		@endif




	</div>

@stop