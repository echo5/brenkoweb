@extends('layouts.app')
{{-- Web site Title --}}
@section('title') Edit Profile - @parent @stop

@section('page-header')

<div class="page-header page-header-directory invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>Edit Profile</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@include('partials.messages')

		<div class="section-header">
			<h2>Basic Info</h2>
		</div>

        {!! Form::open(array('url' => URL::route('profile.edit'), 'method' => 'post', 'files' => true)) !!}
	        <div class="form-group  {{ $errors->has('avatar_file') ? 'has-error' : '' }}">
	            {!! Form::label('avatar_file', 'Avatar', array('class' => 'control-label')) !!}
	            <div class="controls">
					<img src="{{ $user->getAvatar() }}" class="user-avatar-original">
	                {!! Form::file('avatar_file', null, array('class' => 'form-control')) !!}
	                <span class="help-block">{{ $errors->first('avatar_file', ':message') }}</span>
	            </div>
	        </div>
            <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                {!! Form::label('name', 'Full Name', array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::text('name', $user->name, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                </div>
            </div>
            <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                {!! Form::label('email', 'Email', array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::text('email', $user->email, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                </div>
            </div>
            <div class="form-group  {{ $errors->has('website') ? 'has-error' : '' }}">
                {!! Form::label('website', 'Website', array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::text('website', $user->website, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('website', ':message') }}</span>
                </div>
            </div>
            <div class="form-group  {{ $errors->has('paypal_email') ? 'has-error' : '' }}">
                {!! Form::label('paypal_email', 'PayPal Email', array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::text('paypal_email', $user->paypal_email, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('paypal_email', ':message') }}</span>
                </div>
            </div>
            <div class="row">
	            <div class="col-md-6">
	            	<div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
	            	    {!! Form::label('password', "Password", array('class' => 'control-label')) !!}
	            	    <div class="controls">
	            	        {!! Form::password('password', array('class' => 'form-control')) !!}
	            	        <span class="help-block">{{ $errors->first('password', ':message') }}</span>
	            	    </div>
	            	</div>
	            </div>
	            <div class="col-md-6">
	            	<div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
	            	    {!! Form::label('password_confirmation', "Confirm Password", array('class' => 'control-label')) !!}
	            	    <div class="controls">
	            	        {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
	            	        <span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
	            	    </div>
	            	</div>
	            </div>
	            <div class="col-md-12">
		            <div class="alert alert-info">Leave password field blank to keep the same.</div>
	            </div>
            </div>


            <div class="form-group">
                <div class="">
                    <button type="submit" class="btn btn-default">
                        Update
                    </button>
                </div>
            </div>
        {!! Form::close() !!}

	</div>

@stop