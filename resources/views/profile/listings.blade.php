@extends('layouts.app')
{{-- Web site Title --}}
@section('title') {!!  $user->name !!}'s Directory Listings - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>{{ $user->name }}'s Directory Listings</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-12">
		@include('partials.messages')
	</div>

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@if (count($user->listings))
			<div class="row latest-listings">
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Latest Directory Listings</div>
					</div>
					<div class="row">
						<?php $n = 1; ?>
						@foreach ($user->listings as $listing)
							@if ($listing->status == 'approved')
								<div class="col-md-4 col-sm-6">
									@include('listings.partials.grid-item')
								</div>
								@if ($n%3 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
								@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
								<?php $n++; ?>
							@endif
						@endforeach	
					</div>		
				</div>
			</div>
		@endif

		@if (count($user->listings))
			<div class="row pending-listings">
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Pending Directory Listings</div>
					</div>
					<div class="row">
						<?php $n = 1; ?>
						@foreach ($user->listings as $listing)
							@if ($listing->status == 'pending')
								<div class="col-md-4 col-sm-6">
									@include('listings.partials.grid-item')
								</div>
								@if ($n%3 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
								@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
								<?php $n++; ?>
							@endif
						@endforeach			
					</div>
				</div>
			</div>
		@endif

	</div>

@stop