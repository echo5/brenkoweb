@extends('layouts.app')
{{-- Web site Title --}}
@section('title') {!!  $user->name !!}'s Profile - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>{{ $user->name }}'s Profile</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		<div class="row">
			<div class="col-md-12">
				<div class="row">

					<div class="col-md-4">
						<div class="info-box info-box-top">
							<div class="row">
								<i class="icon icon_paperclip"></i>
								<div class="info-box-count h2">{{ count($user->posts) }}</div>
								<div class="info-box-label h4">Articles</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="info-box info-box-top">
							<div class="row">
								<i class="icon icon_tools"></i>
								<div class="info-box-count h2">{{ count($user->scripts) }}</div>
								<div class="info-box-label h4">Scripts</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="info-box info-box-top">
							<div class="row">
								<i class="icon icon_search-2"></i>
								<div class="info-box-count h2">{{ count($user->listings) }}</div>
								<div class="info-box-label h4">Listings</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="info-box info-box-top">
							<div class="row">
								<i class="icon icon_chat_alt"></i>
								<div class="info-box-count h2">{{ count($user->comments) }}</div>
								<div class="info-box-label h4">Comments</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="info-box info-box-top">
							<div class="row">
								<i class="icon icon_star_alt"></i>
								<div class="info-box-count h2">{{ count($user->reviews) }}</div>
								<div class="info-box-label h4">Reviews</div>
							</div>
						</div>
					</div>
				</div>
			</div>
{{-- 			<div class="col-md-12">
				@if (count($user->comments))
					<div class="section-header">
						<h3>Latest Comments</h3>
					</div>
					@foreach ($user->comments as $comment)
						@include('posts.partials.comment')
					@endforeach
				@endif
			</div> --}}
			<div class="col-md-12">
				@if (count($user->scripts))
					<div class="section-header">
						<h3>Latest Scripts</h3>
					</div>
					@foreach ($user->scripts->slice(0,3) as $script)
						@include('scripts.partials.list-item')
					@endforeach
				@endif
			</div>
			<div class="col-md-12">
				@if (count($user->posts))
					<div class="section-header">
						<h3>Latest Articles</h3>
					</div>
					@foreach ($user->posts->slice(0,3) as $item)
						@include('posts.partials.list-item')
					@endforeach
				@endif
			</div>
		</div>
		

	</div>

@stop