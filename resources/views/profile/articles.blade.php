@extends('layouts.app')
{{-- Web site Title --}}
@section('title') {!!  $user->name !!}'s Articles - @parent @stop

@section('page-header')

<div class="page-header page-header-profile page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>{{ $user->name }}'s Articles</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-12">
		@include('partials.messages')
	</div>

	<div class="col-md-3">
		@include('profile.partials.sidebar')
	</div>

	<div class="col-md-9">
		
		@if (count($user->posts))
			<div class="row latest-posts">
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Latest Articles</div>
					</div>
					@foreach ($user->posts as $post)
						@if ($post->status == 'approved' && $post->type == 'article')
							@include('posts.partials.list-item', array('item' => $post))
						@endif
					@endforeach			
				</div>
			</div>
		@endif

		@if (count($user->posts))
			<div class="row latest-posts">
				<div class="col-md-12">
					<div class="section-header">
						<div class="h3">Pending Articles</div>
					</div>
					@foreach ($user->posts as $post)
						@if ($post->status == 'pending' && $post->type == 'article')
							@include('posts.partials.list-item', array('item' => $post, 'pending' => true))
						@endif
					@endforeach			
				</div>
			</div>
		@endif



	</div>

@stop