<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta-description')"/>

    <title>@section('title'){{ Config::get('settings.siteName') }}</title>

    @show
	    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
	    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.js') }}"></script>
        <script src="{{ asset('js/codemirror/lib/codemirror.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/xml/xml.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/javascript/javascript.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/css/css.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
        <!-- PhpFiddle Widget -->
        <link type="text/css" href="http://phpfiddle.org/js/codemirror/addon/pf/css_set.css" rel="stylesheet">
        <script src="http://phpfiddle.org/js/codemirror/mode/pf/allmixed.min.js"></script>
        <script src="http://phpfiddle.org/js/codemirror/addon/pf/js_set.min.js"></script>
        <script src="http://phpfiddle.org/js/jquery.hotkeys-0.7.9.min.js"></script>

    @yield('styles')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{!! asset('favicon.ico')  !!} ">
</head>
<body id="codeparser">
    <?php $height = isset($_GET['height']) ? $_GET['height'] : '400'; ?>
	<?php $width = isset($_GET['width']) ? $_GET['width'] : '100'; ?>
	<script src="http://phpfiddle.org/deposit/widget_001.php?widget_width_percentage=<?php echo $width; ?>&widget_height_pixels=<?php echo $height; ?>&editor_width_percentage=50&editor_placeholder=Insert code here" type="text/javascript">
    </script>
    <div id="phpfiddle_widget_001">Test text</div>
</body>
</html>