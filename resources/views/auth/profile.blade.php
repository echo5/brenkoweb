@extends('layouts.app')
{{-- Web site Title --}}
{{-- @section('title') {!!  $category->name !!} - @parent @stop --}}

@section('page-header')

<div class="page-header page-header-directory page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>{{ $user->name }}'s Profile</h1>
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-3">
		<div class="user-info-box">
			<a href="{{ URL::route('auth.edit') }}" class="btn btn-primary edit-profile-link">Edit</a>
			<div class="user-info-avatar">
				<img src="{{ $user->getAvatar(200) }}" class="user-avatar">
			</div>
			<div class="row">	
				<div class="col-md-12">
					<div class="user-info h3">{{ $user->name }}</div>
				</div>
			</div>

			<div class="section-header">
				<h4>Basic Info</h4>
			</div>
			@if ($user->username)
				<div class="row">	
					<div class="col-md-12">
						<div class="user-info">{{ '@' . $user->username }}</div>
					</div>
				</div>
			@endif
			<div class="row">	
				<div class="col-md-12">
					<div class="user-info">{{ $user->email }}</div>
				</div>
			</div>
			@if ($user->website)
				<div class="row">	
					<div class="col-md-12">
						<div class="user-info"><a href="{{ $user->website }}" target="_blank">{{ $user->website }}</a></div>
					</div>
				</div>
			@endif
			<div class="row">
				<div class="col-md-12">
					<div class="user-info">Member since {{ date('F', strtotime($user->created_at)) }} {{ date('jS', strtotime($user->created_at)) }}, {{ date('Y', strtotime($user->created_at)) }}</div>
				</div>
			</div>
		</div>
		<div class="sidebar-item">
			<div class="sidebar-item-title h4">User Menu</div>
			<ul class="nav nav-pills nav-stacked nav-user">
				<li>
					<a href="#">
						<i class="icon icon_cogs"></i>
						Profile
					</a>
				</li>
				<li>
					<a href="#">
						<i class="icon icon_paperclip"></i>
						Articles
					</a>
				</li>
				<li>
					<a href="#">
						<i class="icon icon_tools"></i>
						Scripts
					</a>
				</li>
				<li>
					<a href="#">
						<i class="icon icon_search-2"></i>
						Listings
					</a>
				</li>
				<li>
					<a href="#">
						<i class="icon icon_datareport"></i>
						Banners
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="col-md-9">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-3">
						<div class="info-box">
							<div class="row">
								<div class="col-md-3">
									<i class="icon icon_paperclip"></i>
								</div>
								<div class="col-md-9">
									<div class="info-box-count h3">{{ count($user->posts) }}</div>
									<div class="info-box-label h5">Articles</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="info-box">
							<div class="row">
								<div class="col-md-3">
									<i class="icon icon_tools"></i>
								</div>
								<div class="col-md-9">
									<div class="info-box-count h3">{{ count($user->scripts) }}</div>
									<div class="info-box-label h5">Scripts</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="info-box">
							<div class="row">
								<div class="col-md-3">
									<i class="icon icon_search-2"></i>
								</div>
								<div class="col-md-9">
									<div class="info-box-count h3">{{ count($user->listings) }}</div>
									<div class="info-box-label h5">Listings</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="info-box">
							<div class="row">
								<div class="col-md-3">
									<i class="icon icon_chat_alt"></i>
								</div>
								<div class="col-md-9">
									<div class="info-box-count h3">{{ count($user->comments) }}</div>
									<div class="info-box-label h5">Comments</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="alert alert-info">No activity to show yet.</div>
			</div>
		</div>
		

	</div>

@stop