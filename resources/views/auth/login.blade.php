@extends('layouts.app')

{{-- Web site Title --}}
@section('title') Member Login - @parent @stop

@section('page-header')
@stop

{{-- Content --}}
@section('content')
	
	<div class="container-fluid">
	    <div class="row">
	    	<div class="col-md-4 col-md-push-4">
				@include('partials.messages')
			</div>
		</div>
	</div>

    <div class="container-fluid">
        <div class="row">
        	<div class="col-md-6 col-md-push-3 panel panel-default panel-login">
        	    <div class="panel-heading">
        	        <h1>Member Login</h1>
        	    </div>
        		<div class="panel-body">
		            {!! Form::open(array('url' => URL::route('auth.login'), 'method' => 'post', 'files'=> true)) !!}
		            {!! csrf_field() !!}
		            <div class="form-group  {{ $errors->has('username') ? 'has-error' : '' }}">
		                {!! Form::label('username', "Username or E-Mail", array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::text('username', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('username', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
		                {!! Form::label('password', "Password", array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::password('password', array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('password', ':message') }}</span>
		                </div>
		            </div>

					<div class="row">
		                <div class="col-md-6">
		                    <div class="checkbox">
		                        <label>
		                            <input type="checkbox" name="remember"> Remember Me
		                        </label>
		                    </div>
		                    <a href="{{ URL::route('password.email') }}">Forgot Your Password?</a>
		                </div>
		                <div class="col-md-6">
		                    <button type="submit" class="btn btn-default pull-right">
		                        Login
		                    </button>
		                </div>
	                </div>


			            <div class="or-separator">
			            	<span class="or-text">
				                Or
			            	</span>
			            </div>
						


		            <div class="row social-connect">
		                <div class="col-md-6">
		                    <a href="{{ URL::route('auth.social', ['provider' => 'facebook']) }}" class="btn btn-primary social-connect-facebook">
		                    	<i class="icon social_facebook"></i>
		                    	Facebook Connect
	                    	</a>
		                </div>
		                <div class="col-md-6">
		                    <a href="{{ URL::route('auth.social', ['provider' => 'google']) }}" class="btn btn-primary social-connect-google">
		                    	<i class="icon social_googleplus"></i>
		                    	Google+ Connect
	                    	</a>
		                </div>
		            </div>
		            {!! Form::close() !!}
        		</div>
        	</div>
        </div>
    </div>
@endsection