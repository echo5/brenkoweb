@extends('layouts.app')

{{-- Web site Title --}}
@section('title') Register - @parent @stop

{{-- Content --}}
@section('content')
    <div class="container-fluid">
        <div class="row">
	        <div class="col-md-4 col-md-push-4 panel panel-default panel-login">
	            <div class="panel-heading">
	                <h1>Register</h1>
	            </div>
	        	<div class="panel-body">
		            {!! Form::open(array('url' => URL::route('auth.register'), 'method' => 'post')) !!}
		            {{-- {!! csrf_field() !!} --}}
			            <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
			                {!! Form::label('name', 'Full Name', array('class' => 'control-label')) !!}
			                <div class="controls">
			                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
			                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
			                </div>
			            </div>
			            <div class="form-group  {{ $errors->has('username') ? 'has-error' : '' }}">
			                {!! Form::label('username', 'Username', array('class' => 'control-label')) !!}
			                <div class="controls">
			                    {!! Form::text('username', null, array('class' => 'form-control')) !!}
			                    <span class="help-block">{{ $errors->first('username', ':message') }}</span>
			                </div>
			            </div>
			            <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
			                {!! Form::label('email', 'Email', array('class' => 'control-label')) !!}
			                <div class="controls">
			                    {!! Form::text('email', null, array('class' => 'form-control')) !!}
			                    <span class="help-block">{{ $errors->first('email', ':message') }}</span>
			                </div>
			            </div>
			            <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
			                {!! Form::label('password', "Password", array('class' => 'control-label')) !!}
			                <div class="controls">
			                    {!! Form::password('password', array('class' => 'form-control')) !!}
			                    <span class="help-block">{{ $errors->first('password', ':message') }}</span>
			                </div>
			            </div>
			            <div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
			                {!! Form::label('password_confirmation', "Confirm Password", array('class' => 'control-label')) !!}
			                <div class="controls">
			                    {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
			                    <span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
			                </div>
			            </div>
			            <div class="form-group  {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
			                <div class="controls">
					            {!! Recaptcha::render() !!}
			                    <span class="help-block">{{ $errors->first('g-recaptcha-response', ':message') }}</span>
				            </div>
			            </div>
			            <div class="form-group">
			                <div class="col-md-6 col-md-offset-4">
			                    <button type="submit" class="btn btn-primary">
			                        Register
			                    </button>
			                </div>
			            </div>
		            {!! Form::close() !!}
	            </div>
            </div>
        </div>
    </div>
@endsection