@if ($newListings)
	<div class="sidebar-item">
		<div class="sidebar-item-title h4">New Listings</div>
		<ul class="nav nav-pills nav-stacked nav-categories">
			@foreach ($newListings as $listing)
				<li>
					<a href="{{ $listing->link }}">
						<i class="icon arrow_right"></i>
						{{ $listing->title }} <br/>
						{{ $listing->link }}
					</a>
				</li>
			@endforeach
		</ul>
	</div>
@endif