<div class="social-share pull-right">
	<i class="icon social_share"></i>
	<div class="social-share-options">
		<div class="social-share-option">
			<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::url()) }}" target="_blank">
				<i class="icon social_facebook"></i>
			</a>
		</div>
		<div class="social-share-option">
			<a href="https://twitter.com/home?status={{ urlencode(Request::url()) }}" target="_blank">
				<i class="icon social_twitter"></i>
			</a>
		</div>
		<div class="social-share-option">
			<a href="https://plus.google.com/share?url={{ urlencode(Request::url()) }}" target="_blank">
				<i class="icon social_googleplus"></i>
			</a>
		</div>
		<div class="social-share-option">
			<a href="https://pinterest.com/pin/create/button/?url={{ urlencode(Request::url()) }}&media=@if(isset($image)){{ urlencode(URL::to($image)) }}@endif&description=@if(isset($description)){{ urlencode($description) }}@endif" target="_blank">
				<i class="icon social_pinterest"></i>
			</a>
		</div>
		<div class="social-share-option">
			<a href="mailto:?&body={{ urlencode(Request::url()) }}&subject=@if(isset($description)){{ urlencode($description) }}@endif" target="_blank">
				<i class="icon icon_mail"></i>
			</a>
		</div>
		<div class="social-share-option">
			<a href="http://www.reddit.com/submit?url={{ urlencode(Request::url()) }}&title=@if(isset($description)){{ urlencode($description) }}@endif" target="_blank">
				<img src="/img/icons/reddit.png" alt="Reddit icon">
			</a>
		</div>
		<div class="social-share-option">
			<a href="https://www.tumblr.com/widgets/share/tool?canonicalUrl={{ urlencode(Request::url()) }}&title=@if(isset($description)){{ urlencode($description) }}@endif" target="_blank">
				<i class="icon social_tumblr"></i>
			</a>
		</div>
	</div>
</div>