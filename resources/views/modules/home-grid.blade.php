<div class="col-md-8 posts-grid container-xs" id="posts-grid">
    @if(count($articles)>0)
        <div class="row posts-grid-col-1" id="posts-grid-ajax-1">
        	<?php 
        	if (isset($is_home)) {
	        	$col_class = 'col-md-12'; $image_size = 'large';
        	}
        	else {
        		$col_class = 'col-sm-6'; $image_size = 'medium';
        	}
        	$n = 1;
        	?>
            @foreach ($articles as $item)
            	@include('posts.partials.grid-item')
                @if ($n%2 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
                <?php ($col_class == 'col-sm-6' ? $n++ :  $n = $n); ?>
                <?php $col_class = 'col-sm-6'; $image_size = 'medium'; ?>
            @endforeach
        </div>
    @endif
	
	@if (isset($is_home))
	    <div class="row">
	        <div class="col-md-12">
				@include('banners.bottom')
			</div>
		</div>
	@endif

</div>
<div class="col-md-4 posts-grid container-xs posts-grid-col-2" id="home-side_default">
    @if (count($tutorials) > 0)
        <div class="row posts-grid-col-2">
        	<?php $col_class = 'col-md-12 col-sm-6'; $image_size = 'medium'; ?>
            @foreach ($tutorials as $item)
	            @include('posts.partials.grid-item')
            @endforeach
        </div>
    @endif

    @if (isset($is_home))
	    <div class="row">
	        <div class="col-md-12">
				@include('banners.content')
	        </div>
	    </div>
    @endif

    @if (count($sponsoredListings) > 0)
        <div class="row">
	    	@foreach ($sponsoredListings as $listing)
            	<div class="col-md-12 col-sm-6">
					@include('listings.partials.grid-item')
				</div>
            @endforeach
        </div>
    @endif

    @if (count($scripts) > 0)
        <div class="row posts-grid-col-2" id="posts-grid-ajax-2">
            @foreach ($scripts as $script)
            	<div class="col-md-12 col-sm-6">                    
    	            @include('scripts.partials.grid-item')
	            </div>
            @endforeach
        </div>
    @endif
</div>