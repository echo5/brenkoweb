@if ($randomListings)
	<div class="sidebar-item">
		<div class="sidebar-item-title h4">Random Listings</div>
		<ul class="nav nav-pills nav-stacked nav-categories">
			@foreach ($randomListings as $listing)
				<li>
					<a href="{{ URL::route('listing.show', ['id' => $listing->id]) }}">
						<i class="icon arrow_right"></i>
						{{ $listing->title }}
					</a>
				</li>
			@endforeach
		</ul>
	</div>
@endif