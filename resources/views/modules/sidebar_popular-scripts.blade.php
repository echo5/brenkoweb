@if (count($popularScripts))
	<div class="sidebar-item">
		<div class="sidebar-item-title h4">Popular Scripts</div>
		<ul class="nav nav-pills nav-stacked nav-categories">
			@foreach ($popularScripts as $script)
				<li>
					<a href="{{ URL::route('script.show', ['id' => $script->id]) }}">
						<i class="icon arrow_right"></i>
						{{ $script->name }}
					</a>
				</li>
			@endforeach
		</ul>
	</div>
@endif