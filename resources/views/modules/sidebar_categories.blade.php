<div class="sidebar-item">
	<div class="sidebar-item-title h4">Article Categories</div>
	<ul class="nav nav-pills nav-stacked nav-categories">
		@foreach ($articleCategories as $category)
			<li>
				<a href="{{ $category->getUrl() }}">
					<i class="icon arrow_right"></i>
					{{ $category->name }}
				</a>
				@if (count($category->children))
					<ul class="children">
						@foreach ($category->children as $child)
							<li>
								<a href="{{ $child->getUrl($category->slug) }}">
									<i class="icon arrow_carrot-right"></i>
									{{ $child->name }}
								</a>
							</li>
						@endforeach
					</ul>
				@endif
			</li>
		@endforeach
	</ul>
</div>

<div class="sidebar-item sidebar-directory" id="sidebar-tutorials">
	<div class="sidebar-item-title h4">Tutorial Chapters</div>
	<ul class="nav nav-pills nav-stacked nav-categories">
		@foreach ($tutorialCategories as $category)
			<li>
            	<a href="{{ $category->getUrl() }}" class="parent @if (count($category->children)) has-children @endif">{{ $category->name }}@if (count($category->children)) <i class="icon icon_plus"></i> @endif</a>
				@if (count($category->children))
					<ul class="children">
						<li>
							@foreach ($category->children as $child)
								<a href="{{ $child->getUrl($category->slug) }}"><i class="icon arrow_carrot-right"></i>{{ $child->name }}</a>
							@endforeach
						</li>
					</ul>
				@endif
				<!--<a href="{{ $category->getUrl() }}"  class="parent @if (count($category->children)) has-children @endif">
					<i class="icon icon_plus"></i>
					{{ $category->name }}
				</a>
				@if (count($category->children))
					<ul class="children">
						@foreach ($category->children as $child)
							<li>
								<a href="{{ $child->getUrl($category->slug) }}">
									<i class="icon arrow_carrot-right"></i>
									{{ $child->name }}
								</a>
							</li>
						@endforeach
					</ul>
				@endif-->
			</li>
		@endforeach
	</ul>
</div>