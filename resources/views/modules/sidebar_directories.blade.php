<div class="sidebar-item sidebar-directory" id="sidebar-directory">
	<div class="sidebar-item-title h4">Directory</div>
	<ul class="nav nav-pills nav-stacked nav-categories">
		@foreach ($listingCategories as $category)
			<li>
				<a href="{{ $category->getUrl() }}" class="parent @if (count($category->children)) has-children @endif">{{ $category->name }} ( {{ $category->totalListings() }} )@if (count($category->children)) <i class="icon icon_plus"></i> @endif</a>
				@if (count($category->children))
					<ul class="children">
						<li>
							@foreach ($category->children as $child)
								<a href="{{ $child->getUrl($category->slug) }}"><i class="icon arrow_carrot-right"></i>{{ $child->name }} ({{ $child->totalListings() }})</a>
							@endforeach
						</li>
					</ul>
				@endif
			</li>
		@endforeach
	</ul>
</div>