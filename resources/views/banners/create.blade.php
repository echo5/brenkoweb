@extends('layouts.app')
{{-- Web site Title --}}
@section('title') Create a Banner - @parent @stop

@section('styles')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
@endsection

@section('body-class') banner-create @stop

@section('page-header')
	@include('partials.messages')
@stop

{{-- Content --}}
@section('content')

	<div class="col-md-3">
		@include('posts.partials.sidebar')
	</div>


    <div class="col-md-9">
        <div class="page-header">
            <h1>Create a Banner</h1>
        </div>
    	<div class="row">
    		<div class="col-md-12">
	            {!! Form::open(array('url' => URL::route('banner.store'), 'method' => 'post', 'files' => true)) !!}
		            <div class="form-group  {{ $errors->has('caption') ? 'has-error' : '' }}">
		                {!! Form::label('caption', 'Caption', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::text('caption', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('caption', ':message') }}</span>
		                </div>
		            </div>
                    <div class="form-group  {{ $errors->has('location') ? 'has-error' : '' }}">
                        {!! Form::label('location', 'Location', array('class' => 'control-label')) !!}
                        <div class="controls">
                    		<div class="row">
                        		@foreach ($locations as $location => $location_name)
	                        		<div class="col-sm-4">
                        				<div class="location">
        	                				<div class="row">
        	                					<div class="col-md-12">
        											<label for="{{ $location }}" class="h5 image-radio">
	        											{!! Form::radio('location', $location, false, array('id' => $location)) !!}
        												<img src="{{ asset('img/banners/location_'.$location.'.png') }}">
        											</label>
        	                					</div>
        	                				</div>
                        				</div>
                    				</div>
                        		@endforeach
                    		</div>
                            <span class="help-block">{{ $errors->first('location', ':message') }}</span>
                        </div>
                    </div>
                    <div>
                    	<p><span style="color:red">Note</span>: <strong>Your Ad</strong> will replace a default advertisement placed at that slot (i.e. Google AdSense). <strong>Your Ad</strong> will rotate with other Ads that may share that location till the set limit expires.</p>
                    </div>
		            <div class="form-group  {{ $errors->has('link') ? 'has-error' : '' }}">
		                {!! Form::label('link', 'Link', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::text('link', 'http://', array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('link', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('image') ? 'has-error' : '' }}">
		                {!! Form::label('image', 'Image', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::file('image', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('image', ':message') }}</span>
		                </div>
		            </div>
                    <div class="form-group  {{ $errors->has('plan_id') ? 'has-error' : '' }}">
                        {!! Form::label('plan_id', 'Plan', array('class' => 'control-label')) !!}
                        <div class="controls">
                        	<div class="list-group">	
                        		@foreach ($plans as $plan)
                        			@if ($plan->public)
                        				<div class="list-group-item plan">
        	                				<div class="row">
        	                					<div class="col-md-1">
        											{!! Form::radio('plan_id', $plan->id, false, array('id' => $plan->id)) !!}
        	                					</div>
        	                					<div class="col-md-11">
        											{!! Form::label($plan->id, $plan->name . ' - $' . $plan->cost, array('class' => 'list-group-item-heading h5')) !!}
        											<div class="plan-description">
        												{!! $plan->description !!}
        											</div>
        	                					</div>
        	                				</div>
                        				</div>
        							@endif
                        		@endforeach
                        	</div>
                            <span class="help-block">{{ $errors->first('plan_id', ':message') }}</span>
                        </div>
                    </div>
                    <div>
                    	<h4>Attention: Read the <strong><a style="text-decoration:underline" href="http://brenkoweb.com/terms-of-service#tos_product">Terms of Service</a></strong> before submitting.</h4>
                    </div>
		            <div class="form-group">
	                    <button type="submit" class="btn btn-primary">
	                        Submit For Review
	                    </button>
		            </div>
	            {!! Form::close() !!}
    		</div>
        </div>
    </div>
    
@endsection

@section('scripts')
	<script>
	    CKEDITOR.replace( 'description' );
	    CKEDITOR.replace( 'features' );
	    CKEDITOR.replace( 'requirements' );
	</script>
@endsection