@if ($banner)
	<div class="banner-block" id="banner-block1" data-banner-id="{{ $banner->id }}">
		<a href="{{ URL::route('banner', ['id' => $banner->id]) }}">
			@if ($banner->script)
				{!! $banner->script !!}
			@else
				<img src="{{ config('assets.bannerImagesDirectory') . $banner->image }}">
			@endif
	    </a>
	</div>
@else
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- BW Responsive Left -->
	<ins class="adsbygoogle"
	     style="display:block"
	     data-ad-client="ca-pub-9194867070756132"
	     data-ad-slot="8879666117"
	     data-ad-format="auto"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
@endif