<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img id="logo" src="{{ asset('img/logo.png') }}" alt="Brenko Web"></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right navbar-user">
                @if (Auth::guest())
                    <li class="{{ (Request::is('login') ? 'active' : '') }}"><a href="{{ Route('auth.login') }}"><i class="icon icon_login"></i> Login</a></li>
                    <li class="{{ (Request::is('register') ? 'active' : '') }}"><a href="{{ Route('auth.register') }}"><i class="icon icon_pencil-edit_alt"></i> Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        	<div class="user-avatar-container">
	                        	<img src="{{ Auth::user()->getAvatar(30) }}" class="user-avatar" alt="{{ 'logged user avatar' }}">
                        	</div>
                        	{{ Auth::user()->name }}
                        	<i class="icon arrow_carrot-down"></i>
                    	</a>
                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::check())
	                            <li>
	                                <a href="{{ URL::route('profile.show', ['id' => Auth::user()->id]) }}"><i class="icon icon_profile"></i> My Profile</a>
	                            </li>
                                @if(Auth::user()->level() >= 100)
                                    <li>
                                        <a href="{{ URL::to('admin') }}"><i class="icon icon_cogs"></i> Admin Dashboard</a>
                                    </li>
                                @endif
                                <li role="presentation" class="divider"></li>
                            @endif
                            <li>
                                <a href="{{ Route('auth.logout') }}"><i class="icon icon_logout"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="{{ (Request::is('/') ? 'active' : '') }}">
                    <a href="{{ URL::to('') }}">Home</a>
                </li>
                <li class="{{ (Request::is('articles') ? 'active' : '') }}">
                    <a href="{{ URL::to('articles') }}">Articles</a>
                </li>
                <li class="{{ (Request::is('tutorials') ? 'active' : '') }}">
                    <a href="{{ URL::to('tutorials') }}">Tutorials</a>
                </li>
                <li class="{{ (Request::is('marketplace') ? 'active' : '') }}">
                    <a href="{{ URL::to('marketplace') }}">Marketplace</a>
                </li>
                <li class="{{ (Request::is('directory') ? 'active' : '') }}">
                    <a href="{{ URL::to('directory') }}">Directory</a>
                </li>
            </ul>
        </div>
    </div>
</nav>