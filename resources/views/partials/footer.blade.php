<div id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<img id="footer-logo" src="{{ asset('img/logo_full.png') }}" alt="Brenko Web">
			</div>
			<div class="col-md-3 col-sm-6 col-sm-offset-3 col-md-offset-0">
				<h4>About</h4>
				<ul class="nav">
					<li><a href="/about"><span itemprop="author" itemscope itemtype="http://schema.org/Person">
<span itemprop="name">BrenkoWeb</span></span></a></li>
					<li><a href="/privacy-policy">Private Policy</a></li>
					<li><a href="/terms-of-service">Terms &amp; Conditions</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 col-sm-offset-3 col-md-offset-0">
				<h4>Contact</h4>
				<ul class="nav">
					<li><a href="{{ URL::route('article.create') }}">Submit Article</a></li>
					<li><a href="{{ URL::route('tutorial.create') }}">Submit Tutorial</a></li>
					<li><a href="{{ URL::route('listing.create') }}">Submit Website</a></li>
                    <li><a href="{{ URL::route('script.create') }}">Sell With Us</a></li>
					<li><a href="{{ URL::route('banner.create') }}">Advertise With Us</a></li>
					<li><a href="/contact">Contact Us</a></li>
					<!--<li><a href="#">Errors Report</a></li>-->
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 col-sm-offset-3 col-md-offset-0">
				<h4>Get Connected</h4>
				<ul class="nav">
					<!--<li><a href="https://www.facebook.com/BrenkoWebLLC" target="_blank">Facebook</a></li>-->
                    <li><a href="https://www.facebook.com/Brenkoweb-1140219112691499/" target="_blank">Facebook</a></li>
					<li><a href="https://twitter.com/brenkoweb" target="_blank">Twitter</a></li>
					<li><a href="https://plus.google.com/+BrenkowebLLC/posts" target="_blank" rel="Publisher">Google +</a></li>
				</ul>
			</div>                
    	</div>
	</div>
</div>
<div id="footer-copy">
    <!--SITELOCK-->
	<div style="float: right; z-index: 1000; width: 110px; height: 60px; margin-top: -10px; margin-right: 25px; padding: 0px"><a href="#" onclick="window.open('https://www.sitelock.com/verify.php?site=www.brenkoweb.com','SiteLock','width=600,height=600,left=160,top=170');" ><img class="img-responsive" alt="SiteLock" title="SiteLock" src="//shield.sitelock.com/shield/www.brenkoweb.com" /></a></div>
    
    <div class="container">
        <p class="text-muted credit"><span style="text-align: left; float: left">&copy; <?php echo intval(date("Y")); ?> Brenko Web</span> <span class="hidden-credit" style="text-align: right; float: right">Designed by <a href="http://echo5webdesign.com/" target="_blank">Echo 5 Web Design</a></span></p>        
    </div>
   
</div>