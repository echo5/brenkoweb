@if(Session::has('message'))
	<div class="row">
		<div class="col-md-12">
			<div class="system-message">
				<p class="alert {!! Session::get('alert-class', 'alert-info') !!}">{!! Session::get('message') !!}</p>
			</div>	
		</div>
	</div>
@endif