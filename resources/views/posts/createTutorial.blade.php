@extends('layouts.app')
{{-- Web site Title --}}
@section('title') Submit a Tutorial - @parent @stop

@section('body-class') tutorial-create @stop

@section('page-header')
	@include('partials.messages')
@stop

{{-- Content --}}
@section('content')

	<div class="col-md-3">
		@include('posts.partials.sidebar')
	</div>


    <div class="col-md-9">
        <div class="page-header">
            <h1>Create Tutorial</h1>
        </div>
    	<div class="row">
    		<div class="col-md-12">
	            {!! Form::open(array('url' => URL::route('tutorial.store'), 'method' => 'post', 'files' => true)) !!}
		            <div class="form-group  {{ $errors->has('title') ? 'has-error' : '' }}">
		                {!! Form::label('title', 'Title', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::text('title', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('title', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('meta_description') ? 'has-error' : '' }}">
		                {!! Form::label('meta_description', 'Description', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::textarea('meta_description', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('meta_description', ':message') }}</span>
		                </div>
		            </div>
                    <div>
                    	<p><span style="color:red">Note</span>: Use <strong>Description</strong> field for suggestions.</p>
                    </div>
		            <div class="form-group  {{ $errors->has('zip') ? 'has-error' : '' }}">
		                {!! Form::label('zip', 'Zip File', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::file('zip', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('zip', ':message') }}</span>
		                </div>
		            </div>
                    <div>
                    	<p><span style="color:red">Note</span>: Use <strong>Zip File</strong> field to upload compressed tutorials or samples.</p>
                    </div>
                    <div>	
                    	<h4>Attention: Read the <strong><a style="text-decoration:underline" href="http://brenkoweb.com/terms-of-service#tos_product">Terms of Service</a></strong> before submitting.</h4>
                    </div>
		            <div class="form-group">
	                    <button type="submit" class="btn btn-primary">
	                        Submit For Review
	                    </button>
		            </div>
	            {!! Form::close() !!}
    		</div>
        </div>
    </div>
    
@endsection