<div class="{{ $col_class }} post">
	<div class="row">
    	<div class="col-md-12">
			<div class="post-img-container">
        		<a href="{{ $item->getUrl() }}" class="">
        			{!! $item->getImage($image_size) !!}
    			</a>
                <div class="post-date">
                	<div class="post-date-day h2">{{ date('j', strtotime($item->created_at)) }}</div>
                	<div class="post-date-month h5">{{ date('M', strtotime($item->created_at)) }}</div>
            	</div>
			</div>
		</div>		
	</div>    
    
    <div itemprop="articleSection" class="row">
        <div itemprop="articleBody" class="col-md-8">
        	@if ($item->category)
        	<span class="post-category h5">
        		<a href="@if ($item->category->parent){{ $item->category->getUrl($item->category->parent->slug)}} @else{{ $item->category->getUrl() }}@endif">{!! ($item->category->parent) ? $item->category->parent->name . ' / ' : '' !!} {{ $item->category->name }}</a>
        	</span>
        	@endif
            <h4 class="post-title">
                <a href="{{ $item->getUrl() }}">{{ $item->title }}</a>
            </h4>
        </div>
        <div class="col-md-4">
                <div class="by-line">By <span class="user-name">{{ $item->user->name }}</span></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 post-content">
        	{!! strip_tags(str_limit($item->content, 150, '...')) !!}
        </div>
    </div>
    <div class="row">

    </div>
</div>