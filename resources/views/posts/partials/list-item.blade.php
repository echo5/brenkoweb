<div class="post">
	<div class="row">					
		<div class="col-md-3">
			<div class="post-img-container">
        		@if (!isset($pending))<a href="{{ $item->getUrl() }}" class="">@endif
        			{!! $item->getImage('small') !!}
    			@if (!isset($pending))</a>@endif
			</div>
		</div>
		<div class="col-md-9">
			<h3>@if (!isset($pending))<a href="{{ $item->getUrl() }}" class="">@endif{{ $item->title }}@if (!isset($pending))</a>@endif</h3>
			<div class="post-date h5">{{ date('F', strtotime($item->created_at)) }} {{ date('jS', strtotime($item->created_at)) }}, {{ date('Y', strtotime($item->created_at)) }}</div>
			<div class="post-content">{!! strip_tags(str_limit($item->content, 200, '...')) !!}</div>
		</div>
	</div>
</div>