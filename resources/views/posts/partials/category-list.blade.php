<div class="row categories-grid">
	<?php $n = 1; ?>
	@foreach ($categories as $category)
		<div class="col-md-4 col-sm-6">
			<ul class="category-list">
				<li class="category-parent">
					<i class="icon icon_documents_alt"></i>
					<h4><a href="{{ $category->getUrl() }}">{{ $category->name }}</a></h4>
					@if (count($category->children))
						<ul class="category-children">
							@foreach ($category->children as $child)
								<li>
									<h5><a href="{{ $child->getUrl($category->slug) }}">{{ $child->name }}</a></h5>
								</li>
							@endforeach
						</ul>
					@endif
				</li>
			</ul>
		</div>
		@if ($n%3 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
		@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
		<?php $n++; ?>
	@endforeach
</div>