<div id="comment-{{ $comment->id }}" class="comment">
	<div class="row">
		<div class="col-xs-2">
			<div class="comment-user-avatar">
				<img src="{{ $comment->user->getAvatar() }}">
			</div>
		</div>
		<div class="col-xs-10">
			<div class="comment-user h5">{{ $comment->user->name }}</div>
			<div class="comment-date h6">on {{ date('F', strtotime($comment->created_at)) }} {{ date('jS', strtotime($comment->created_at)) }}, {{ date('Y', strtotime($comment->created_at)) }}</div>
			<div class="clearfix"></div>
			<div class="comment-content">{{ $comment->content }}</div>
			@if ($comment->parent == null)
				<a href="#" class="comment-reply" data-comment-id="{{ $comment->id }}">
					<i class="icon arrow_back"></i> Reply
				</a>
			@endif
		</div>
	</div>
</div>