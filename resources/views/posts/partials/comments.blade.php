<div class="comments-section">
	<div class="section-header">
		<h3>Comments</h3>
	</div>
	<div class="row">
		<div class="col-md-8">
			@if (count($comments))
				<div class="comments">
					@foreach ($comments as $comment)
						@include('posts.partials.comment')
						@if (count($comment->children))
							<div class="row">
								<div class="col-xs-11 col-xs-push-1">
									<div class="comment-children" id="comment-children-{{ $comment->id }}">
										@foreach ($comment->children as $comment)
											@include('posts.partials.comment')
										@endforeach
									</div>
								</div>									
							</div>
						@endif
					@endforeach
				</div>
			@else
				<p>No comments have been made yet.</p>
			@endif	

			@if (Auth::check())
				<div class="row">
					<div id="comment-form-container">
						<div id="comment-form" class="comment-form">
						    {!! Form::open(array('url' => Request::url(), 'method' => 'post')) !!}
							    {!! Form::hidden('post_id', $post->id) !!}
							    {!! Form::hidden('parent_id', null) !!}
							    <div class="col-xs-2">
							    	<div class="comment-user-avatar">
							    		<img src="{{ Auth::user()->getAvatar() }}">
							    	</div>
							    </div>
							    <div class="col-xs-10">
							        <div class="form-group  {{ $errors->has('content') ? 'has-error' : '' }}">
							            {{-- {!! Form::label('content', 'Comment', array('class' => 'control-label')) !!} --}}
							            <div class="controls">
							                {!! Form::textarea('content', null, array('class' => 'form-control', 'placeholder' => 'Leave a Comment')) !!}
							                <span class="help-block">{{ $errors->first('content', ':message') }}</span>
							            </div>
							        </div>
							    </div>
						        <div class="col-md-12">
							        <div class="form-group">
						                <button type="submit" class="btn btn-primary pull-right">
						                    Post Comment
						                </button>
							        	<a href="#" class="comment-cancel h5 pull-right">Cancel Reply</a>
							        </div>        	
						        </div>
						    {!! Form::close() !!}
						</div>
					</div>
				</div>
			@else
				
				<p class="alert alert-info">Please login to leave a comment.  <a href="{{ URL::route('auth.login') }}">Login now</a></p>

			@endif

		</div>
		<div class="col-md-4">
			@include('banners.content')
		</div>
	</div>




</div>