<div class="post-paging">
	<div class="col-md-4">
		@if ($previous)
			<a href="{{ $previous->getUrl() }}" class="h4"><i class="icon arrow_carrot-2left"></i> {{ $previous->title }}</a>
		@endif
	</div>
	<div class="col-md-4 center">
		<a href="#" id="scroll-to-top" class="h4"><i class="icon arrow_carrot-2up"></i> Go Back To Top</a>
	</div>
	<div class="col-md-4 pull-right-md">
		@if ($next)
			<a href="{{ $next->getUrl() }}" class="h4">{{ $next->title }} <i class="icon arrow_carrot-2right"></i></a>
		@endif
	</div>
</div>