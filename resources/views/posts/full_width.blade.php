@extends('layouts.app')
@section('meta-description') {{  $post->meta_description }} @stop
@section('title') {!!  $post->title !!} - @parent @stop

@section('meta_author')
    <meta name="author" content="{!! $post->user->name !!}"/>
@stop

@section('page-header')
	<div class="page-header page-header-{{ $post->slug }} @if ($post->image) page-header-with-image invert-section @endif" @if ($post->image) style="background-image:url('{{ config('assets.postImageThumbsDirectory') . 'medium/' . $post->image }}')" @endif>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>{{ $post->title }}</h1>
				</div>
			</div>
	    </div>
	</div>
@stop

{{-- Content --}}
@section('content')
	<div class="col-md-12">
		{!! $post->content !!}
	</div>
@stop