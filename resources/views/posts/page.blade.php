@extends('layouts.app')
{{-- Web site Title --}}
@section('title') {!!  $post->title !!} - @parent @stop

@section('meta_author')
    <meta name="author" content="{!! $post->user->name !!}"/>
@stop


<div class="page-header page-header-{{ $post->slug }} @if ($post->image) page-header-with-image invert-section @endif" @if ($post->image) style="background-image:url('{{ config('assets.postImageThumbsDirectory') . 'medium/' . $post->image }}')" @endif>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>{{ $post->title}}</h1>
			</div>
		</div>
    </div>
</div>

{{-- Content --}}
@section('content')

	<div class="col-md-3">
		@include('posts.partials.sidebar')
	</div>

	<div class="col-md-9">
		{!! $post->content !!}
	</div>

@stop