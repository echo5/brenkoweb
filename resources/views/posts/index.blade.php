@extends('layouts.app')

@if ($currentCategory)
	@section('meta-description'){{ $currentCategory->meta_description }}@stop
@else
	@if ($post_type == 'tutorial')
		@section('meta-description')The BrenkoWeb provides an excellent choice of free online programming tutorials for the web programmers in various languages such as PHP, SQL, HTML, ASP, JavaScript, XML, etc... By visiting the website users can find various online interactive programs, scripts and languages that are designed to suite their needs.@stop
	@else    
		@section('meta-description')The BrenkoWeb provides an excellent choice of free online articles for web designers, developers, beginners as well as professionals. There are various design and development articles posted on the webpage. By visiting the website users can find online learning articles suitable to their needs.@stop
	@endif
@endif

@if ($post_type == 'tutorial')
	@section('meta-keywords')Language Learning Tutorials, Learn JavaScript Online Interactive, Online Programming Tutorials, Learn Basic HTML, Learn Basic CSS, Learning MySQL Programming, Learning PHP Programming, Learning JavaScript Programming, Learn PHP Online Free, JavaScript Learn Online, Learn JavaScript Online Free, Database MySQL Tutorial, JavaScript Coding Tutorial @stop
@else         
	@section('meta-keywords')Language Learning Articles, Online Programming Articles, Learning Web Design, Learn Web Development, Learn Web Design Online, Web development Articles, Learn Graphic Design, Learn Photoshop, Learn SEO and Marketing, Online Articles about Web Development, Onlne Artticles about Web Design, Advanced HTML and CSS, Free Articles for Web Developers @stop
@endif

@if ($currentCategory)
	@section('title'){{ $currentCategory->name }} - @parent @stop
@else 
   	@if ($post_type == 'tutorial') 
       	@section('title')Online Programming Tutorials - @parent @stop
    @else
       	@section('title')Language Learning Articles - @parent @stop
    @endif
@endif

@if ($currentCategory)
	@section('body-id')category-{{ $currentCategory->id }}@stop
@endif
@section('body-class') post-category post-category-type-{{ $post_type }} @stop

@section('content')

	<div class="col-md-9 pull-right-md">
		<div class="page-header">
			<h1>@if ($currentCategory) {{ $currentCategory->name}} @else {{ $title }} @endif</h1>
		</div>

		@if (count($categories))
			@if ($post_type == 'tutorial')
				@include('posts.partials.category-list')
			@else
				@include('posts.partials.category-grid')
			@endif
		@endif

		@if (count($posts))
			<?php $n = 1; ?>
			<div class="row latest-posts container-xs">
				<div class="col-md-12">
					@if ($post_type == 'tutorial' && $currentCategory)

					@else
						@if (!$currentCategory)
							<div class="section-header">
								<div class="h3">Latest {{ $title }}</div>
							</div>
						@endif
					@endif
					<div class="row">
						@foreach($posts as $post)
							<div class="col-md-12 col-sm-6">
								@include('posts.partials.list-item', ['item' => $post])
							</div>
							@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
							<?php $n++; ?>
						@endforeach		
					</div>	
				</div>
				<div class="col-md-12">
					{!! $posts->render() !!}
				</div>
			</div>
		@endif

	</div>

	<div class="col-md-3">
		@include('posts.partials.sidebar')
	</div>


@stop