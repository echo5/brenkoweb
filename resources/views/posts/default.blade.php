@extends('layouts.app')
@section('meta-description') {{  $post->meta_description }} @stop
@section('title'){!!  $post->title !!} - @parent @stop

@section('body-id')post-{{ $post->id }}@stop
@section('body-class') post-single post-type-{{ $post->type }} template-post-default @stop

{{-- Content --}}
@section('content')

	<div class="col-md-12">
		@include('partials.messages')
	</div>

	<div class="col-md-9 pull-right-md">
			<div class="row">
				<div class="visible-sm visible-xs col-md-12">
					<div class="post-img">
						{!! $post->getImage() !!}
					</div>
				</div>
				<div class="col-md-10">
					<div class="row">
						<div class="col-xs-6">
							<span class="post-category h5">
								<a href="@if ($post->category->parent){{ $post->category->getUrl($post->category->parent->slug)}} @else{{ $post->category->getUrl() }}@endif">{!! ($post->category->parent) ? $post->category->parent->name . ' / ' : '' !!} {{ $post->category->name }}</a>
							</span>
						</div>
						<div class="col-xs-6">
				            <div class="by-line pull-right">
			            		By <span itemprop="author" itemscope itemtype="http://schema.org/Person" class="user-name"><a itemprop="name" href="{{ URL::route('profile.show', ['id' => $post->user->id ]) }}">{{ $post->user->name }}</a></span>
		            		</div>
						</div>
					</div>
					<div class="page-header">
					    <a itemprop="url" href="{{ $post->getUrl() }}" class=""><h1>{{ $post->title }}</h1></a>
					</div>

				    <div itemscope itemtype="http://schema.org/Article" itemprop="articleBody" class="post-content">
				    	{!! $post->content !!}
			    	</div>

				</div>
				<div class="col-md-2 post-meta">
				    <div itemprop="datePublished" content="Please insert valid ISO 8601 date/time here. Examples: 2015-07-27 or 2015-07-27T15:30" class="post-date pull-right">
				    	<div class="post-date-day h2">{{ date('j', strtotime($post->created_at)) }}</div>
				    	<div class="post-date-month h5">{{ date('M', strtotime($post->created_at)) }}</div>
                        <div class="post-date-year h6" style="display:none;">{{ date('Y', strtotime($post->created_at)) }}</div>
					</div>
					@include('modules.social-share', ['description' => $post->title, 'image' => $post->getImage()])
				</div>
			</div>

			@include('posts.partials.comments')

			@include('posts.partials.paging')
	</div>

	<div class="col-md-3">
		<div class="visible-md visible-lg">
			<div itemprop="image" class="post-img">
				{!! $post->getImage() !!}
			</div>
		</div>
		@include('posts.partials.sidebar')
	</div>

@stop