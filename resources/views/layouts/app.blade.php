<!DOCTYPE html>
<!-- Microdata markup added by Google Structured Data Markup Helper. -->
<html itemscope itemtype="http://schema.org/WebPage" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta-description')"/>
    <meta name="keywords" content="@yield('meta-keywords')"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    
    <!-- BEGIN verifications  -->
    <meta name="google-site-verification" content="ai47URJfBpDzc2hvfdW7dUt0j17Titdtl2Mm6CxU-AM" />
	<meta name="alexaVerifyID" content="YXpy1trHw0EOWiGgqFzmpVhs1CU" />
	<meta name="msvalidate.01" content="564C69E85E4D3DB2BF064B6585D9929E" />
	<!-- END ver-->

    <title>@section('title'){{ Config::get('settings.siteName') }}</title>

    @show
	    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
	    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.js') }}"></script>
        <script src="{{ asset('js/codemirror/lib/codemirror.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/xml/xml.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/javascript/javascript.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/css/css.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/clike/clike.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/php/php.js') }}"></script>
        <script src="{{ asset('js/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
        <!-- PhpFiddle Widget -->
        <link type="text/css" href="http://phpfiddle.org/js/codemirror/addon/pf/css_set.css" rel="stylesheet">
        <script src="http://phpfiddle.org/js/codemirror/mode/pf/allmixed.min.js"></script>
        <script src="http://phpfiddle.org/js/codemirror/addon/pf/js_set.min.js"></script>
        <script src="http://phpfiddle.org/js/jquery.hotkeys-0.7.9.min.js"></script>

    @yield('styles')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{!! asset('favicon.ico')  !!} ">
</head>
<body id="@yield('body-id', 'body_default')" class="@yield('body-class', '')">
@include('partials.header')
<div itemprop="mainEntity" itemscope itemtype="http://schema.org/Article" id="container">
	@yield('page-header')
	<div id="main" class="container">
		<div class="row">
			@yield('content') 
		</div>
	</div>
	@yield('below-content')
</div>

@include('partials.footer')

<!-- Scripts -->
@yield('scripts')
<script src="{{ asset('js/scripts.js') }}"></script>

@if (App::environment('local'))
	<script type="text/javascript" src="http://localhost:48626/takana.js"></script>
	<script type="text/javascript">
	  takanaClient.run({
	    host: 'localhost:48626'
	  });
	</script>
@endif

<!-- BEGIN Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-16616082-4', 'auto');
  ga('send', 'pageview');

</script>
<!-- END G.A. -->

</body>
</html>