@extends('layouts.app')
@section('meta-description') {{ $post->description }} @stop
@section('title') {!!  $post->title !!} - @parent @stop

@section('meta_author')
    <meta name="author" content="{!! $post->user->name !!}"/>
@stop

@section('page-header')
@stop



{{-- Content --}}
@section('content')

	<div class="row">
		<div class="col-md-12">
			<h1>{{ $post->title }}</h1>			
		</div>
	</div>

	<div class="col-md-12">
		{!! $post->content !!}
	</div>
	<div class="col-md-8 col-md-push-2">
		@include('partials.messages')
	
		{!! Form::open(array('route' => 'contact.store', 'class' => 'form')) !!}

            <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                {!! Form::label('name', 'Your Name', array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                </div>
            </div>

            <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                {!! Form::label('email', 'Your Email', array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::text('email', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                </div>
            </div>

            <div class="form-group  {{ $errors->has('message') ? 'has-error' : '' }}">
                {!! Form::label('message', 'Your Message', array('class' => 'control-label')) !!}
                <div class="controls">
                    {!! Form::textarea('message', null, array('class' => 'form-control')) !!}
                    <span class="help-block">{{ $errors->first('message', ':message') }}</span>
                </div>
            </div>
            
            <div class="form-group  {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
                <div class="controls">
		            {!! Recaptcha::render() !!}
                    <span class="help-block">{{ $errors->first('g-recaptcha-response', ':message') }}</span>
	            </div>
            </div>

			<div class="form-group">
			    {!! Form::submit('Contact Us!', 
			      array('class'=>'btn btn-primary')) !!}
			</div>
			
		{!! Form::close() !!}
	</div>
@stop