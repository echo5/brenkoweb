@extends('layouts.app')
@section('meta-description') {{ $post->description }} @stop
@section('title') {!!  $post->title !!} - @parent @stop

{{-- Content --}}
@section('content')
	<div class="page-header page-header-{{ $post->slug }}">
		<div class="row">
			<div class="col-md-12">
				<h1>{{ $post->title }}</h1>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		{!! $post->content !!}
	</div>
@stop