@extends('layouts.app')
@section('meta-description') BrenkoWeb is a prominent website allowing users to learn languages such as PHP, MYSQL, JAVASCRIPT, HTML, CSS, and find out online programming articles and ideas @stop
@section('meta-keywords') PHP Learning Site, Programming Tutorials Online, Online Web Tutorials, Learning Coding Online, MYSQL Learning Online, Web Design Principles, Learn Web Programming, Learn To Create A Website, Learn web Design Online Free, JAVASCRIPT Learning Site, HTML Learning Site, CSS Learning Site, XML Learning Site @stop
@section('title')Programming Learning Site, Tutorials and Articles Online - @parent @stop

@section('body-class') home @stop

@section('page-header')

<div class="page-header page-header-home page-header-with-image invert-section">
	<div itemprop="mainEntityOfPage" class="container">
		<img itemprop="author" src="{{ asset('img/logo_icon_white.png') }}" alt="BrenkoWeb LLC">
        <h1 itemprop="name" style="margin-top: 30px;">Free online articles, tutorials and code editors,<br/> development ideas and more.</h1>
        <h2>We are dedicated to help web amateurs and professionals learn something new or improve their already existing knowledge about the many web related developing and programming tools and information.</h2>
        <a href="#">
			<div id="scroll-down" class="icon arrow_down scroll-down"></div>
        </a>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-9 pull-right-md">
		<?php $is_home = true; ?>
		<div id="home-grid">
			@include('modules.home-grid')
		</div>

		<div class="load-more" id="load-more">
		    <div class="loading-icon-container" id="loading-icon-container">
			    <i id="loading-icon" class="icon icon_loading rotating"></i>
		    </div>
		    <div class="alert alert-info" id="loading-message"></div>
			<a class="btn btn-default" id="btn-load-more" data-page="1">Load More</a>
		</div>

	</div>



	<div class="col-md-3">
		@include('posts.partials.sidebar')
	</div>

@endsection

@section('below-content')

<div class="submit-article-container invert-section">
	<div class="container">
		<div class="col-md-8 col-md-push-2">
			<h2>Want to write?</h2>
			<p>Our website is built of interesting information about the Web and Internet such as their history, existing protocols, etc., easy-to-use programming tutorials as the HTML tutorial, how-to section that helps understand how to start things up, let say choose a domain name or a web server (very handy for newcomers!), different online resources and many other aspects that together represent the Internet and the World Wide Web universe.</p>
			<a href="{{ URL::route('article.create') }}" class="btn btn-inverse">Submit an Article</a>	
		</div>
	</div>
</div>

@endsection
