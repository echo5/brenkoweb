@extends('layouts.app')
@section('meta-description') {{ $post->description }} @stop
@section('title') {!!  $post->title !!} - @parent @stop

{{-- Content --}}
@section('content')
	<div class="col-md-12">
		{!! $post->content !!}
	</div>
@stop