<div id="content" class="content-wrapper">
    <div class="content">
        <h1 class="h3">Dashboard</h1>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-pencil"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Articles</span>
                        <span class="info-box-number">{{ $total_articles }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-graduation-cap"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Tutorial pages</span>
                        <span class="info-box-number">{{ $total_tutorials }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-code"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Scripts</span>
                        <span class="info-box-number">{{ $total_scripts }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Members</span>
                        <span class="info-box-number">{{ $total_users }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Total Site Earnings</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="chart">
                                    <!-- Sales Chart Canvas -->
                                    <div id="sales-chart" style="width:100%;height:300px;"></div>
                                </div>
                                <!-- /.chart-responsive -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- ./box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header">${{ $order_data['total_amount'] }}</h5>
                                    <span class="description-text">TOTAL REVENUE</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header">${{ $order_data['script_total'] }}</h5>
                                    <span class="description-text">SCRIPTS TOTAL</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header">${{ $order_data['listing_total'] }}</h5>
                                    <span class="description-text">LISTINGS TOTAL</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block">
                                    <h5 class="description-header">${{ $order_data['banner_total'] }}</h5>
                                    <span class="description-text">BANNERS TOTAL</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-md-8">

                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Category Posts</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="chart-responsive">
                                    <div id="article-category-chart" height="155" width="329" style="width: 329px; height: 155px;"></div>
                                </div>
                                <!-- ./chart-responsive -->
                            </div>
                            <div class="col-md-6">
                                <div class="chart-responsive">
                                    <div id="tutorial-category-chart" height="155" width="329" style="width: 329px; height: 155px;"></div>
                                </div>
                                <!-- ./chart-responsive -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Orders</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th>Payment ID</th>
                                        <th>Item</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders->slice(0,15) as $order)
                                    <tr>
                                        <td><a href="/admin/page/admin.order?id={{ $order->id }}">{{ $order->id }}</a></td>
                                        <td>{{ $order->payment_id }}</td>
                                        <td>
                                            @foreach ($order->items as $item)
                                            {{ ucfirst($item->type) }}: {{ $item->name }}
                                            @endforeach
                                        </td>
                                        <td><span class="label label-success">{{ $order->payment_status }}</span></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="/admin/orders" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
            <div class="col-md-4">

                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pending Items</h3>
                        <div class="box-tools pull-right">
                            @if (($pending_articles + $pending_tutorials + $pending_scripts + $pending_listings) > 0)
                            <span class="label label-danger">{{ $pending_articles + $pending_tutorials + $pending_scripts + $pending_listings }} Pending Items</span>
                            @endif
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding pending-items">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="description-block margin-bottom">
                                    <a href="/admin/articles?status=pending">
                                    <i class="fa fa-pencil"></i>
                                    <h5 class="description-header">{{ $pending_articles }}</h5>
                                    <span class="description-text">Articles</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="description-block margin-bottom">
                                    <a href="/admin/tutorials?status=pending">
                                    <i class="fa fa-graduation-cap"></i>
                                    <h5 class="description-header">{{ $pending_tutorials }}</h5>
                                    <span class="description-text">Tutorials</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="description-block margin-bottom">
                                    <a href="/admin/scripts?status=pending">
                                    <i class="fa fa-code"></i>
                                    <h5 class="description-header">{{ $pending_scripts }}</h5>
                                    <span class="description-text">Scripts</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="description-block margin-bottom">
                                    <a href="/admin/directory_listings?status=pending">
                                    <i class="fa fa-sitemap"></i>
                                    <h5 class="description-header">{{ $pending_listings }}</h5>
                                    <span class="description-text">Listings</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!--/.box -->
                <!-- USERS LIST -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Top Script Authors</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <ul class="users-list clearfix">
                            @foreach ($topAuthors->slice(0,8) as $user)
                            <li>
                                <img src="{{ $user->getAvatar() }}" alt="{{ $user->getDisplayName() }}">
                                <a class="users-list-name" href="/admin/users/{{ $user->id }}">{{ $user->getDisplayName() }}</a>
                                <span class="users-list-date">${{ $user->totalSales() }}</span>
                            </li>
                            @endforeach
                        </ul>
                        <!-- /.users-list -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="/admin/users" class="uppercase">View All Users</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!--/.box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Top Earning Scripts</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>ID</th>
                                <th>Script</th>
                                <th>Total Sales</th>
                                <th>Author</th>
                            </tr>
                            @foreach ($topScripts->slice(0,10) as $script)
								<tr>
									<td><a href="/admin/scripts/{{ $script->id }}">{{ $script->id }}</a></td>
									<td>{{ $script->name }}</td>
									<td>${{ $script->totalSales() }}</td>
									<td>{{ $script->user->getDisplayName() }}</td>
								</tr>
                            @endforeach
                        </tbody></table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </div>
</div>

<script>
jQuery( document ).ready(function($) {
	@if (isset($order_data['scripts']))
		var dataScripts = [
			{{ implode(',', $order_data['scripts']) }}
		];
	@endif
	@if (isset($order_data['listings']))
	var dataListings = [
		{{ implode(',', $order_data['listings']) }}
	];
	@endif
	@if (isset($order_data['banners']))
	var dataBanners = [
		{{ implode(',', $order_data['banners']) }}
	];
	@endif
	var dataArticleCategories = [
		@foreach ($articleCategories as $category)
			{ data: {{ $category->postCount() }}, label: "{{ $category->name }}" },
		@endforeach
	]
	var dataTutorialCategories = [
		@foreach ($tutorialCategories as $category)
			{ data: {{ $category->postCount() }}, label: "{{ $category->name }}" },
		@endforeach
	]


	var options = {
		xaxis: { 
			mode: "time",
			tickLength: 0,
			timeformat: "%b %Y"
		},
		yaxis: {
			tickLength: 0,
		},
		grid: {
			hoverable: true,
			clickable: true,
			color: "#f7f7f7"
		},
		legend: {
		    color: "#3d3d3d"
		},
		// lines: { show: true, fill: true },
	};

	var salesPlot = $.plot($("#sales-chart"),[
			@if (isset($order_data['scripts']))
				{ data: dataScripts, label: "Scripts" },
			@endif
			@if (isset($order_data['listings']))
				{ data: dataListings, label: "Listings" },
			@endif
			@if (isset($order_data['banners']))
				{ data: dataBanners, label: "Banners" },
			@endif
		],
		options);

	var articleCategoriesPlot = $.plot('#article-category-chart', dataArticleCategories, {
	    series: {
	        pie: {
	            innerRadius: 0.5,
	            show: true
	        }
	    }
	});

	var tutorialCategoriesPlot = $.plot('#tutorial-category-chart', dataTutorialCategories, {
	    series: {
	        pie: {
	            innerRadius: 0.5,
	            show: true
	        }
	    }
	});

	// Bind tooltips
	$("<div id='tooltip'></div>").css({
		position: "absolute",
		display: "none",
		border: "1px solid #f7f7f7",
		padding: "10px",
		"background-color": "#3d3d3d",
		opacity: ".8",
		color: "#fff"
	}).appendTo("body");
	$("#sales-chart").bind("plothover", function (event, pos, item) {


		var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
		$("#hoverdata").text(str);
		
		if (item) {
			var x = item.datapoint[0],
				y = item.datapoint[1];

			$("#tooltip").html("$" + y)
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(200);
		} else {
			$("#tooltip").hide();
		}
		
	});
});
</script>