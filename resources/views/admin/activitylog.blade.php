<li class="dropdown messages-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	    <i class="fa fa-envelope-o"></i>
	    @if ($unreadMessages)
		    <span class="label label-success">{{ $unreadMessages }}</span>
	    @endif
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have {{ $unreadMessages }} messages</li>
        <li class="activity-list">
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                @foreach ($activityMessages as $activity)
		            <li class="activity {{ in_array($activity->id, (array) $readActivities) ? 'read' : 'unread' }}" data-activity-id="{{ $activity->id }}">
	                    <a href="{{ $activity->getUrl() }}">
	                    <h4>
	                    {{ $activity->description }}
	                    <small><i class="fa fa-clock-o"></i> {{ $activity->created_at }}</small>
	                    </h4>
	                    <p>{{ $activity->details }}</p>
	                    </a>
	                </li>
                @endforeach
            </ul>
        </li>
        <li class="footer"><a href="/admin/messages">See All Messages</a></li>
        <li class="footer mark-all-as-read"><a href="#">Mark all as read</a></li>
    </ul>
</li>
<!-- Notifications: style can be found in dropdown.less -->
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	    <i class="fa fa-bell-o"></i>
	    @if ($unreadNotifications)
		    <span class="label label-warning">{{ ($unreadNotifications) }}</span>
	    @endif
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have {{ $unreadNotifications }} notifications</li>
        <li class="activity-list">
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
            	@foreach ($activityNotifications as $activity)
	            	<li class="activity {{ in_array($activity->id, (array) $readActivities) ? 'read' : 'unread' }}" data-activity-id="{{ $activity->id }}">
	            	    <a href="{{ $activity->getUrl() }}">
	            	    <i class="fa fa-{!! $activity->getIcon() !!}"></i>  {{-- <i class="fa fa-users text-aqua"></i> --}} {{ $activity->description }} <br/>
	            	    <small>{{ $activity->details }}</small>
	            	    </a>
	            	</li>
            	@endforeach
            </ul>
        </li>
        <li class="footer mark-all-as-read"><a href="#">Mark all as read</a></li>
    </ul>
</li>
<!-- Tasks: style can be found in dropdown.less -->
<li class="dropdown tasks-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	    <i class="fa fa-flag-o"></i>
	    @if ($unreadPendingItems > 0)
		    <span class="label label-danger">{{ ($unreadPendingItems) }}</span>
	    @endif
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have {{ ($unreadPendingItems) }} new items waiting approval</li>
        <li class="activity-list">
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
            	@foreach ($activityPendingItems as $activity)
	            	<li class="activity {{ in_array($activity->id, (array) $readActivities) ? 'read' : 'unread' }}" data-activity-id="{{ $activity->id }}">
	            		@if ($activity->content_type == 'Tutorial')
							<a href="{{ $activity->details }}">
	            		@else
		            	    <a href="{{ $activity->getUrl() }}">
	            	    @endif
	            	    <i class="fa fa-{!! $activity->getContentIcon() !!}"></i>  {{-- <i class="fa fa-users text-aqua"></i> --}} {{ $activity->description }} <br/>
	            	    <small>{{ $activity->details }}</small>
	            	    </a>
	            	</li>
            	@endforeach
        	</ul>
        </li>
        <li class="footer mark-all-as-read"><a href="#">Mark all as read</a></li>
    </ul>
</li>