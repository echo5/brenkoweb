<div id="content" class="content-wrapper">
	<div class="content">
		<div class="box box-primary">
			<div class="box-body">
				<h2 class="page-header">
					<i class="fa fa-barcode"></i>
					Order #{{ $order->id }}					
					<small class="pull-right">Date: {{ $order->created_at }}</small>
				</h2>
				<div class="row">
			        <div class="col-xs-12">
		        		<h4>Purchaser</h4>
					</div>
				</div>
				<div class="row">
			        <div class="col-md-6">
				        <div class="order">
				        	<strong>ID:</strong> {{ $order->user->id }}<br>
				        	<strong>Name:</strong> {{ $order->user->name }}<br>
				        	<strong>Email:</strong> {{ $order->user->email }}<br>
				        </div>
					</div>
				</div>
				<br>
				<div class="row">
			        <div class="col-xs-12">
		        		<h4>Products</h4>
					</div>
				</div>
				<div class="row">
			        <div class="col-xs-12 table-responsive">
			          <table class="table table-striped">
			            <thead>
			            <tr>
			              <th>Type</th>
			              <th>Product</th>
			              <th>Product ID (plan or script ID)</th>
			              <th>Model ID</th>
			              <th>Subtotal</th>
			            </tr>
			            </thead>
			            <tbody>
			            @foreach ($order->items as $item)
				            <tr>
				              <td>{{ $item->type }}</td>
				              <td>{{ $item->name }}</td>
				              <td>{{ $item->product_id }}</td>
				              <td>{{ $item->model_id }}</td>
				              <td>${{ $item->price }}</td>
				            </tr>
			            @endforeach
			            </tbody>
			          </table>
			        </div>
			        <!-- /.col -->
			    </div>

	    		<div class="row">
	    	        <div class="col-xs-12">
	            		<h4>Payment</h4>
	    			</div>
	    		</div>

				<div class="row">
			    	<div class="col-md-6">
        				<div class="table-responsive">
        		            <table class="table">
        						<tbody>
		        		            	<tr>
		        		                	<th>Payment Method:</th>
		        		                	<td>{{ $order->payment_method }}</td>
		        		              	</tr>
		        		            	<tr>
		        		                	<th>Payer Name:</th>
		        		                	<td>{{ $order->payer_name }}</td>
		        		              	</tr>
		        		            	<tr>
		        		                	<th>Payer Email:</th>
		        		                	<td>{{ $order->payer_email }}</td>
		        		              	</tr>
		        		            	<tr>
		        		                	<th>Payer ID:</th>
		        		                	<td>{{ $order->payer_id }}</td>
		        		              	</tr>
		        		            	<tr>
		        		                	<th>Payment ID:</th>
		        		                	<td>{{ $order->payment_id }}</td>
		        		              	</tr>
		        		            	<tr>
		        		                	<th>Payment Status:</th>
		        		                	<td>{{ $order->payment_status }}</td>
		        		              	</tr>
        		            	</tbody>
    		            	</table>
        	            </div>
		            </div>
        	    	<div class="col-md-6">
        				<div class="table-responsive">
        		            <table class="table">
        						<tbody>
		        		            	<tr>
		        		                	<th>Total:</th>
		        		                	<td>${{ $order->total() }}</td>
		        		              	</tr>
        		            	</tbody>
    		            	</table>
        	            </div>
                    </div>
	            </div>

			</div>
		</div>
	</div>
</div>