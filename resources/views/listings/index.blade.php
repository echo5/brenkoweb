@extends('layouts.app')

@if ($currentCategory)
	@section('meta-description'){{ $currentCategory->meta_description }}@stop
@else
	@section('meta-description') The BrenkoWeb is a prominent directory website which created a business listing platform for webmasters and designers and also provides an online coding learning platform for programmers @stop
@endif

@section('meta-keywords') Directory Website, Learning Coding Online, Business Directory @stop

@section('title') @if ($currentCategory) {{ $currentCategory->name }} @else Directory - Directory Website, Learning Coding Online @endif - @parent @stop

@section('page-header')

<div class="page-header page-header-directory page-header-with-image invert-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2">
				<h1>@if ($currentCategory) {{ $currentCategory->name }} @else Directory @endif</h1>
				@if (!$currentCategory)
					<h2>BrenkoWeb is a directory for web designers, developers, and webmasters which can be used for building both simple and enterprise-level listings.  There are several options that can be used when adding a listing of your company, website or a project.  Some of them are: articles publishing, adding simple or complex link features, publishing pictures and videos, advertisements, etc.  BrenkoWeb is constantly being improved, both as an online web developing tools and tutorials, as well as a web developing directory.</h2>
				@endif
			</div>
		</div>
    </div>
</div>

@endsection

@section('content')

	<div class="col-md-9 pull-right-md">

		<div class="row">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12">
						@include('banners.bottom')
					</div>
				</div>
				<div class="row latest-listings container-xs">
					<div class="col-md-12">
						<div class="section-header">
							<div class="h3">@if ($currentCategory) {{ $currentCategory->name }} @else Latest Listings @endif</div>
						</div>		
						<div class="row">
							<?php $n = 1; ?>
							@if (count($sponsoredListings))
								@foreach($sponsoredListings as $listing)
									<div class="col-md-4 col-sm-6">
										@include('listings.partials.grid-item', ['class' => 'listing-sponsored'])
									</div>
									@if ($n%3 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
									@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
									<?php $n++; ?>
								@endforeach	
							@endif
							@if (count($currentListings))
								@foreach($currentListings as $listing)
									<div class="col-md-4 col-sm-6">
										@include('listings.partials.grid-item')
									</div>
									@if ($n%3 == 0) <div class="clearfix visible-md visible-lg"></div> @endif
									@if ($n%2 == 0) <div class="clearfix visible-sm"></div> @endif
									<?php $n++; ?>
								@endforeach		
								<div class="col-md-12">
									{!! $currentListings->render() !!}
								</div>
							@endif

						</div>	
					</div>
				</div>
			</div>
			<div class="col-md-4">
				@include('listings.partials.sidebar-right')
			</div>
		</div>
	</div>

	<div class="col-md-3">
		@include('listings.partials.sidebar')
	</div>


@stop