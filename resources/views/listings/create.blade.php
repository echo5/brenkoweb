@extends('layouts.app')
{{-- Web site Title --}}
@section('title') Add a Listing - @parent @stop

@section('styles')
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
@endsection

@section('body-class') listing-create @stop

@section('page-header')
	@include('partials.messages')
@stop

{{-- Content --}}
@section('content')

	<div class="col-md-3">
		@include('listings.partials.sidebar')
	</div>


    <div class="col-md-9">
        <div class="page-header">
            <h1>Add a Listing</h1>
        </div>
    	<div class="row">
    		<div class="col-md-12">
	            {!! Form::open(array('url' => URL::route('listing.store'), 'method' => 'post', 'files' => true)) !!}
		            <div class="form-group  {{ $errors->has('title') ? 'has-error' : '' }}">
		                {!! Form::label('title', 'Title', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::text('title', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('title', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('link') ? 'has-error' : '' }}">
		                {!! Form::label('link', 'Link', array('class' => 'control-label')) !!}              
                        <div class="controls">
		                    {!! Form::text('link', 'http://', array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('link', ':message') }}</span>
		                </div>
		            </div>
		            <?php 
		            $category_array = array();
		            foreach ($listingCategories as $category) {
	            		$category_array[$category->id] = $category->name;
	            		if (count($category->children)) {
            				foreach ($category->children as $child) {
            					$category_array[$child->id] = '-- ' . $child->name;
            				}
	            		}
		            }
		            ?>
		            <div class="form-group  {{ $errors->has('category_id') ? 'has-error' : '' }}">
		                {!! Form::label('category_id', 'Category', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::select('category_id', $category_array, null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('category_id', ':message') }}</span>
		                </div>
		            </div>
                    <div>
                    	<p><span style="color:red">Note</span>: Use <strong>Description</strong> field's <strong>first line</strong> for category suggestions.</p>
                    </div>
		            <div class="form-group  {{ $errors->has('image') ? 'has-error' : '' }}">
		                {!! Form::label('image', 'Image', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::file('image', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('image', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('description') ? 'has-error' : '' }}">
		                {!! Form::label('description', 'Description', array('class' => 'control-label')) !!}
		                <div class="controls">
		                    {!! Form::textarea('description', null, array('class' => 'form-control')) !!}
		                    <span class="help-block">{{ $errors->first('description', ':message') }}</span>
		                </div>
		            </div>
		            <div class="form-group  {{ $errors->has('plan_id') ? 'has-error' : '' }}">
		                {!! Form::label('plan_id', 'Plan', array('class' => 'control-label')) !!}
		                <div class="controls">
		                	<div class="list-group">	
		                		@foreach ($plans as $plan)
		                			@if ($plan->public)
		                				<div class="list-group-item plan">
			                				<div class="row">
			                					<div class="col-md-1">
													{!! Form::radio('plan_id', $plan->id) !!}
			                					</div>
			                					<div class="col-md-11">
													<div class="list-group-item-heading h5">{{ $plan->name }} - ${{ $plan->cost }}</div>
													<div class="plan-description">
														{!! $plan->description !!}
													</div>
			                					</div>
			                				</div>
		                				</div>
									@endif
		                		@endforeach
		                	</div>
		                    <span class="help-block">{{ $errors->first('plan_id', ':message') }}</span>
		                </div>
		            </div>
                    <div>
                    	<h4>Attention: Read the <strong><a style="text-decoration:underline" href="http://brenkoweb.com/terms-of-service#tos_product">Terms of Service</a></strong> before submitting.</h4>
                    </div>
		            <div class="form-group">
	                    <button type="submit" class="btn btn-primary">
	                        Submit For Review
	                    </button>
		            </div>
	            {!! Form::close() !!}
    		</div>
        </div>
    </div>
    
@endsection

@section('scripts')
	<script>
	    CKEDITOR.replace( 'description' );
	</script>
@endsection