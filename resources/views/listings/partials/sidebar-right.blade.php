<div class="sidebar-item sidebar-add-listing sidebar-no-title">
	<h4>Want to get listed?</h4>
	<a href="{{ URL::route('listing.create') }}" class="btn btn-default">Submit A Listing</a>
</div>

@include('modules.sidebar_directories')
@include('banners.content')