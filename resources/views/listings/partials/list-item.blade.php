<div class="listing">
	<div class="row">					
		<div class="col-md-3">
			<div class="listing-img-container">
        		<a href="{{ $listing->link }}" target="_blank">
            		<img src="{{ config('assets.listingImageThumbsDirectory') . $listing->image }}" alt="{{ $listing->title . ' thumb' }}>
    			</a>
			</div>
		</div>
		<div class="col-md-9">
			<h3 class="listing-title">
        		<a href="{{ $listing->link }}" target="_blank">
					<span class="at-symbol">@</span>
					{{ $listing->title }}
				</a>
			</h3>
			<div class="h4 listing-link">
        		<a href="{{ $listing->link }}" target="_blank">
					<i class="icon icon_link_alt"></i>
					{{ $listing->link }}
				</a>
			</div>
			<p class="listing-content">{!! str_limit($listing->description, 150, '...') !!}</p>
		</div>
	</div>
</div>