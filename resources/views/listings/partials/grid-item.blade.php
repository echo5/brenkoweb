<div class="listing @if (isset($class)) {{ $class }} @endif @if (!$listing->image) listing-no-image @endif">
	<div class="row">					
		<div class="col-md-12">
			<div class="listing-img-container">
				@if ($listing->image)
	        		<a href="{{ URL::route('listing.show', ['id' => $listing->id]) }}" target="_blank">
	            		<img src="{{ config('assets.listingImageThumbsDirectory') . $listing->image }}" alt="{{ $listing->title . ' thumb' }}">
	    			</a>
    			@else
	    			<a href="{{ URL::route('listing.show', ['id' => $listing->id]) }}" target="_blank">
	    				<div class="listing-icon-image"><i class="icon icon_folder-open_alt"></i></div>
    				</a>
    			@endif
    			<div class="listing-clicks">
    				<i class="icon icon_search"></i>
    				<span class="h5">{{ $listing->clicks }}</span>
    			</div>
			</div>
		</div>
		<div class="col-md-12">
			<h4 class="listing-title">
        		<a href="{{ URL::route('listing.show', ['id' => $listing->id]) }}" target="_blank">
					{{ $listing->title }}
				</a>
			</h4>
			<div class="h5 listing-category">
				{!! ($listing->category->parent) ? $listing->category->parent->name . ' / ' : '' !!}{{ $listing->category->name }}
			</div>
			<p class="listing-content">{{ strip_tags(str_limit($listing->description, 150, '...')) }}</p>
			<a href="{{ URL::route('listing.show', ['id' => $listing->id]) }}" target="_blank" class="btn btn-sm btn-accent">Visit Site</a>
		</div>
	</div>
</div>